<?php /* THIS IS TH UI KIT FOR SPLENDOR & STORY */ ?>

<html xmlns="http://www.w3.org/1999/xhtml" lang="en-US">
    <head>
        <title>Splendor &amp; Story - UI Kit</title>
        <link rel='stylesheet' id='style-css'  href='/wp-content/themes/cleanslate/assets/css/style.css?ver=1.0.0' type='text/css' media='all' />
        <link href="https://fonts.googleapis.com/css?family=Assistant|Fanwood+Text" rel="stylesheet">
    </head>
    
    <body class="wrapper">
        
        <style>
            body {
                background-color: #ccbeab;
            }

            .ui-kit-title {
                text-transform: uppercase;
                font-weight: 300;
                display: block;
                letter-spacing: .5rem;
                font-size: 14px;
                color: white;
                margin-bottom: 10px;
            }
            
            .margin-none {
                margin-bottom: 0;
            }
            
            .ui-swatch {
                margin-bottom: 3rem;
                padding-left: 2rem;
                padding-right: 2rem;
                font-size: 14px;
                text-transform: uppercase;
            }
            
            .ui-swatch p {
                padding-bottom: 0;
            }
            
            .underline {
                margin-bottom: 10px;
                padding-bottom: 10px;
                border-bottom: 1px solid #dddddd;
                color: #555555;
                font-weight: 500;
            }
        </style>
        
        <div class="pt__6 pb__6">
            <span class="ui-kit-title underline">Headings</span>
            <div class="pb__2">
                <span class="ui-kit-title margin-none">H1  |  60px  |  #fff  |  Fanwood Text</span>
                <h1>The spectacle before us was indeed sublime.</h1>
            </div>
            <div class="pb__2">
                <span class="ui-kit-title margin-none">H2  |  40px  |  #773208  |  Fanwood Text  </span>
                <h2>The spectacle before us was indeed sublime.</h2>
            </div>
            <div class="pb__2">
                <span class="ui-kit-title margin-none">H3  |  30px  |  #773208  |  Fanwood Text  </span>
                <h3>The spectacle before us was indeed sublime.</h3>
            </div>
            <div class="pb__2">
                <span class="ui-kit-title margin-none">H4  |  24px  |  #773208  |  Fanwood Text  </span>
                <h4>The spectacle before us was indeed sublime.</h4>
            </div>
            <div class="pb__2">
                <span class="ui-kit-title margin-none">H5  |  18px  |  #773208  |  Fanwood Text</span>
                <h5>The spectacle before us was indeed sublime.</h5>
            </div>
            <div class="pb__2">
                <span class="ui-kit-title margin-none">H6  |  16px  |  #773208  |  Fanwood Text</span>
                <h6>The spectacle before us was indeed sublime.</h6>
            </div>
        </div>
        
        <div class="pb__5">
            <span class="ui-kit-title underline">Content</span>
            <span class="ui-kit-title">Main Content Section  |  Assisstant |  Font-size: 16px  |  Lineheight: 26px  |  Color: #757575
            </span>
            <p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et justo duo dolores et ea rebum. </p>
            
            <span class="ui-kit-title">Large Content Section  |  Assisstant  |  18px   |   Lineheight: 28px   |   Color: #555
            </span>
            <div class="text-lg">
                <p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et justo duo dolores et ea rebum. </p>
            </div>  

            <span class="ui-kit-title">Small Content Section  |  Assisstant  |  14px   |   Lineheight: 24px   |   Color: #555
            </span>
            <div class="text-sm">
                <p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et justo duo dolores et ea rebum. </p>
            </div>
        </div>
        <?php /*
        <div class="pb__4">
            <span class="ui-kit-title underline">Swatches</span>
            <div class="grid">
                <span class="grid__item col-1-1 ui-kit-title">Primary Colors</span>
                <div class="grid__item col-1-1 m--col-1-3">
                    <div class="ui-swatch bg--navy pt__4 pb__1">
                        <p>Primary<br>
                        Navy Teal<br>
                        Hex #044d66</p>
                    </div>
                </div>
                <div class="grid__item col-1-1 m--col-1-3">
                    <div class="ui-swatch bg--blue pt__4 pb__1">
                        <p>Primary<br>
                        Light Blue<br>
                        Hex #009bde</p>
                    </div>
                </div>
                <div class="grid__item col-1-1 m--col-1-3">
                    <div class="ui-swatch bg--orange pt__4 pb__1">
                        <p>Primary<br>
                        Orange<br>
                        Hex #faa41a</p>
                    </div>
                </div>
                <span class="grid__item col-1-1 ui-kit-title">Secondary Colors</span>
                <div class="grid__item col-1-1 m--col-1-3">
                    <div class="ui-swatch bg--coral pt__4 pb__1">
                        <p>Secondary<br>
                        Coral<br>
                        Hex #ff6c36</p>
                    </div>
                </div>
                <div class="grid__item col-1-1 m--col-1-3">
                    <div class="ui-swatch bg--cool-green pt__4 pb__1">
                        <p>Secondary<br>
                        Cool Green<br>
                        Hex #00b288</p>
                    </div>
                </div>
                <div class="grid__item col-1-1 m--col-1-3">
                    <div class="ui-swatch bg--md-blue pt__4 pb__1">
                        <p>Secondary<br>
                        Md. Blue<br>
                        Hex #0076a9</p>
                    </div>
                </div>
                <span class="grid__item col-1-1 ui-kit-title">Tertiary Colors</span>
                <div class="grid__item col-1-1 m--col-1-3">
                    <div class="ui-swatch bg--rose pt__4 pb__1">
                        <p>tertiary<br>
                        Dusty Rose<br>
                        Hex #8a4b52</p>
                    </div>
                </div>
                <div class="grid__item col-1-1 m--col-1-3">
                    <div class="ui-swatch bg--yellow pt__4 pb__1">
                        <p>Tertiary<br>
                        Yellow<br>
                        Hex #d6c300</p>
                    </div>
                </div>
                <div class="grid__item col-1-1 m--col-1-3">
                    <div class="ui-swatch bg--green pt__4 pb__1">
                        <p>Tertiary<br>
                        Green<br>
                        Hex #bbd437</p>
                    </div>
                </div>
                <span class="grid__item col-1-1 ui-kit-title">Grays</span>
                <div class="grid__item col-1-1 m--col-1-3">
                    <div class="ui-swatch bg--b-gray-lt pt__4 pb__1">
                        <p>Light Gray Blue<br>
                         Hex #dadfe1</p>
                    </div>
                </div>
                <div class="grid__item col-1-1 m--col-1-3">
                    <div class="ui-swatch bg--lt-blue pt__4 pb__1">
                        <p>Light Blue<br>
                         Hex #a6bbc3</p>
                    </div>
                </div>
                <div class="grid__item col-1-1 m--col-1-3">
                    <div class="ui-swatch bg--gray-lt pt__4 pb__1">
                        <p>Light Gray<br>
                         Hex #a7a9ac</p>
                    </div>
                </div>
                <div class="grid__item col-1-1 m--col-1-3">
                    <div class="ui-swatch bg--b-gray-md pt__4 pb__1">
                        <p>Medium Gray BLue<br>
                         Hex #778692</p>
                    </div>
                </div>
                <div class="grid__item col-1-1 m--col-1-3">
                    <div class="ui-swatch bg--b-gray-nm pt__4 pb__1">
                        <p>Gray Blue<br>
                         Hex #435363</p>
                    </div>
                </div>
                <div class="grid__item col-1-1 m--col-1-3">
                    <div class="ui-swatch bg--b-gray-dk pt__4 pb__1">
                        <p>Dark Gray Blue<br>
                         Hex #263746</p>
                    </div>
                </div>
                <div class="grid__item col-1-1 m--col-1-3">
                    <div class="ui-swatch bg--gray-dk pt__4 pb__1">
                        <p>Dark Gray<br>
                        Hex #333</p>
                    </div>
                </div>
                <div class="grid__item col-1-1 m--col-1-3">
                    <div class="ui-swatch bg--gray-md pt__4 pb__1">
                        <p>Content (Md) Gray<br>
                         Hex #555555</p>
                    </div>
                </div>
                <div class="grid__item col-1-1 m--col-1-3">
                    <div class="ui-swatch bg--gray-nc pt__4 pb__1">
                        <p>Gray Lightest<br>
                         Hex #F7F7F7</p>
                    </div>
                </div>
            </div>
        </div>
        */ ?>
        <div class="pb__7"> 
            <span class="ui-kit-title underline">Buttons &amp; Inputs</span>
            <div class="grid">
                <div class="grid__item col-1-1 m--col-1-3">
                    <input type="text" placeholder="Default Input" />
                    <select>
                        <option>Select</option>
                        <option>Option</option>
                        <option>Option</option>
                        <option>Option</option>
                    </select>
                    <textarea placeholder="Textarea"></textarea>
                </div>
                <div class="grid__item col-1-1 m--col-1-3">
                    <div class="pt__2"><button class="btn btn--primary"><span>Primary</span></button></div>
                    <div class="pt__2"><button class="btn btn--white"><span>Outline</span></button></div>
                </div>
                <div class="grid__item col-1-1 m--col-1-3">
                    <p><a class="text-sm with-caret" href="#">Read More - 14pt</a><br>
                    <a class="with-caret" href="#">Read More - 16pt</a><br>
                    <a class="text-lg with-caret" href="#">Read More- 18pt</a></p>
                </div>
            </div>
        </div>
        <?php /*
        <div class="pb__4">
            <span class="ui-kit-title underline">Icons &amp; Illustrations</span>
            <div class="grid">
                <div class="grid__item col-1-1 m--col-1-4">
                    <p><i class="fa fa-chevron-right"></i> fa-chevron-right<br>
                    <i class="fa fa-chevron-up"></i> fa-chevron-up<br>
                    <i class="fa fa-chevron-down"></i> fa-chevron-down<br>
                    <i class="fa fa-chevron-left"></i> fa-chevron-left<br>
                    <i class="fa fa-check"></i> fa-check<br>
                    <i class="fa fa-arrow-right"></i> fa-arrow-right<br>
                    <i class="fa fa-angle-right"></i> fa-angle-right<br>
                    <i class="fa fa-angle-up"></i> fa-angle-up<br>
                    <i class="fa fa-angle-down"></i> fa-angle-down<br>
                    <i class="fa fa-minus"></i> fa-minus<br>
                    <i class="fa fa-plus"></i> fa-plus<br>
                    <i class="fa fa-remove"></i> fa-remove<br>
                    <i class="fa fa-bars"></i> fa-bars</p>
                </div>
                <div class="grid__item col-1-1 m--col-1-4">
                    <p><i class="fa fa-share"></i> fa-share<br>
                    <i class="fa fa-facebook"></i> fa-facebook<br>
                    <i class="fa fa-linkedin"></i> fa-linkedin<br>
                    <i class="fa fa-twitter"></i> fa-twitter<br>
                    <i class="fa fa-youtube"></i> fa-youtube</p>
                </div>
                <div class="grid__item col-1-1 m--col-1-4">
                    <p><i class="fa fa-file-pdf-o"></i> fa-file-pdf-o<br>
                    <i class="fa fa-file-excel-o"></i> fa-file-excel-o<br>
                    <i class="fa fa-file-text-o"></i> fa-file-text-o<br>
                    <i class="fa fa-file-powerpoint-o"></i> fa-file-powerpoint-o<br>
                    <i class="fa fa-file-word-o"></i> fa-file-word-o<br>
                    <i class="fa fa-phone"></i> fa-phone<br>
                    <i class="fa fa-copyright"></i> fa-copyright<br>
                    <i class="fa fa-map-marker"></i> fa-map-marker<br>
                    <i class="fa fa-external-link-square"></i> fa-external-link-square</p>
                </div>
                <div class="grid__item col-1-1 m--col-1-4">
                    <p><i class="fa fa-book"></i> fa-book    |   eBook<br>
                    <i class="fa fa-calendar"></i> fa-calendar    |    Events<br>   
                    <i class="fa fa-feed"></i> fa-feed<br>
                    <i class="fa fa-desktop"></i> fa-desktop    |   Webinars<br>
                    <i class="fa fa-youtube-play"></i> fa-youtube-play<br>
                    <i class="fa fa-newspaper-o"></i> fa-newspaper-o    |    News<br>
                    <i class="fa fa-pencil"></i> fa-pencil<br>
                    <i class="fa fa-play-circle-o"></i> fa-play-circle-o</p>
                </div>
            </div>
        </div>
        */ ?>
	</body>
</html>