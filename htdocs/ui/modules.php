<?php /* THIS IS TH UI KIT FOR MRI SOFTWARE */ ?>

<html xmlns="http://www.w3.org/1999/xhtml" lang="en-US">
    <head>
        <title>MRI Software - Modules</title>

        <link rel='stylesheet' id='style-css'  href='/wp-content/themes/cleanslate/assets/css/style.css?ver=1.0.0' type='text/css' media='all' />
        <link href="https://fonts.googleapis.com/css?family=Roboto:300,500|Work+Sans:300,400,700" rel="stylesheet">
        <script type='text/javascript' src='/wp-includes/js/jquery/jquery.js'></script>
    </head>
    
    <body>

        <?php 
        /*
         * START OF BASIC CONTENT
        */ 
        ?>
        <div class="flex flex--basic_content text-center">
            <div class="wrapper wrapper--narrow">
                <div class="grid">
                    <div class="grid__item col-1-1">
                        <div class="block__heading">Test</div>
                        <div class="block__content">
                            <p>We deliver innovative solutions that set your real estate company free to elevate its potential.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="flex flex--basic_content">
            <div class="wrapper wrapper--narrow">
                <div class="grid">
                    <div class="grid__item col-1-1">
                        <div class="block__heading">H2- Let your business run wide open.</div>
                        <div class="block__content">
                            <p>As a business leader in retail, you are responsible for directing and managing your company to ensure it remains stable, profitable and able to grow. It’s no different for us: We focus our efforts first and foremost on the success of our clients, but the intended result of those efforts is to build a durable, thriving organization that continually benefits our stakeholders.</p>

                            <p>Those stakeholders, however, include more than just our customers, owners, executives and employees. They also include their families all the other people who receive and share the benefits of what we produce.</p>

                            <p>From this broader perspective, our business is not just a generator of profit but also part of a larger community and an agent of growth. As such, we have a responsibility to support not only those for whom it provides the privileges and rewards of good work, but also those who deserve and aspire to have the same kinds of opportunities. We are especially motivated to help children in difficult circumstances who could never access those opportunities without basic foundational resources such as education and secure home environments.</p>
                        </div>

                            <button class="btn btn--primary"><span>Contact Us</span></button>
                            <button class="btn btn--ghost"><span>Contact Us</span></button>

                    </div>
                </div>
            </div>
        </div>
        <?php 
        /*
         * START OF MULTIPLE COLUMNS
        */ 
        ?>
        <div class="flex flex--multiple_columns">
            <div class="wrapper">
                <div class="block__heading">H2- Let your business run wide open.</div>
                <div class="block__sub-content">
                    <p>Those stakeholders, however, include more than just our customers, owners, executives and employees. They also include their families all the other people who receive and share the benefits of what we produce.</p>
                </div>
                <div class="grid">
                    <div class="grid__item col-1-1 m--col-1-2">
                        <div class="block__sub-heading">H2- Let your business run wide open.</div>
                        <div class="block__sub-content">
                            <p>As a business leader in retail, you are responsible for directing and managing your company to ensure it remains stable, profitable and able to grow. It’s no different for us: We focus our efforts first and foremost on the success of our clients, but the intended result of those efforts is to build a durable, thriving organization that continually benefits our stakeholders.</p>
                        </div>
                    </div>
                    <div class="grid__item col-1-1 m--col-1-2">
                        <div class="block__sub-heading">H2- Let your business run wide open.</div>
                        <div class="block__sub-content">
                            <p>As a business leader in retail, you are responsible for directing and managing your company to ensure it remains stable, profitable and able to grow. It’s no different for us: We focus our efforts first and foremost on the success of our clients, but the intended result of those efforts is to build a durable, thriving organization that continually benefits our stakeholders.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="flex flex--multiple_columns">
            <div class="wrapper">
                <div class="block__heading">H2- Let your business run wide open.</div>
                <div class="grid">
                    <div class="grid__item col-1-1 l--col-1-3 text-sm">
                        <div class="block__sub-heading">H2- Let your business run wide open.</div>
                        <div class="block__sub-content">
                            <p>As a business leader in retail, you are responsible for directing and managing your company to ensure it remains stable, profitable and able to grow. </p>
                        </div>
                    </div>
                    <div class="grid__item col-1-1 l--col-1-3 text-sm">
                        <div class="block__sub-heading">H2- Let your business run wide open.</div>
                        <div class="block__sub-content">
                            <p>As a business leader in retail, you are responsible for directing and managing your company to ensure it remains stable, profitable and able to grow. </p>
                        </div>
                    </div>
                    <div class="grid__item col-1-1 l--col-1-3 text-sm">
                        <div class="block__sub-heading">H2- Let your business run wide open.</div>
                        <div class="block__sub-content">
                            <p>As a business leader in retail, you are responsible for directing and managing your company to ensure it remains stable, profitable and able to grow. </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="flex flex--multiple_columns text-center">
            <div class="wrapper">
                <div class="block__heading">H2- Let your business run wide open.</div>
                <div class="grid">
                    <div class="grid__item col-1-1 m--col-1-2 l--col-1-4 text-sm">
                        <div class="block__sub-heading">H2- Let your business run wide open.</div>
                        <div class="block__sub-content">
                            <p>As a business leader in retail, you are responsible for directing and managing your company to ensure it remains stable, profitable and able to grow. </p>
                        </div>
                    </div>
                    <div class="grid__item col-1-1 m--col-1-2 l--col-1-4 text-sm">
                        <div class="block__sub-heading">H2- Let your business run wide open.</div>
                        <div class="block__sub-content">
                            <p>As a business leader in retail, you are responsible for directing and managing your company to ensure it remains stable, profitable and able to grow. </p>
                        </div>
                    </div>
                    <div class="grid__item col-1-1 m--col-1-2 l--col-1-4 text-sm">
                        <div class="block__sub-heading">H2- Let your business run wide open.</div>
                        <div class="block__sub-content">
                            <p>As a business leader in retail, you are responsible for directing and managing your company to ensure it remains stable, profitable and able to grow. </p>
                        </div>
                    </div>
                    <div class="grid__item col-1-1 m--col-1-2 l--col-1-4 text-sm">
                        <div class="block__sub-heading">H2- Let your business run wide open.</div>
                        <div class="block__sub-content">
                            <p>As a business leader in retail, you are responsible for directing and managing your company to ensure it remains stable, profitable and able to grow. </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php 
        /*
         * START OF Case Studies
        */ 
        ?>
        <div class="flex flex--case_studies bg--circle bg--circle--blue">
            <div class="grid grid--middle">
                <div class="grid__item col-1-1 l--col-3-12 pt__2 pb__2">
                    <div class="block__wrap">
                        <div class="block__heading">Client success is our key metric.</div>
                        <div class="block__content">
                            <p>We determine success by needs met and goals achieved.</p>
                            <a class="btn btn--secondary" href="#"><span>View All Case Studies</span></a>
                        </div>
                    </div>
                </div>
                <div class="grid__item col-1-1 l--col-9-12 pt__2 pb__2">
                    <div class="slider">
                        <div>
                            <div class="card card--case-study">
                                <div class="card__img"><span style="background-image:url(/mri-software/wp-content/themes/mri-software/assets/images/buildout/logo-test.png);"></span></div>
                                <h6>Derwent London</h6>
                                <p>Increases agility with MRI Investmen Model</p>
                                <a class="with-caret text-sm" href="#">View Case Sudy</a>
                            </div>
                        </div>
                        <div>
                            <div class="card card--case-study">
                                <div class="card__img"><span style="background-image:url(/mri-software/wp-content/themes/mri-software/assets/images/buildout/logo-test.png);"></span></div>
                                <h6>Case Study Name</h6>
                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
                                <a class="with-caret text-sm" href="#">View Case Sudy</a>
                            </div>
                        </div>
                        <div>
                            <div class="card card--case-study">
                                <div class="card__img"><span style="background-image:url(/mri-software/wp-content/themes/mri-software/assets/images/buildout/logo-test.png);"></span></div>
                                <h6>Long Case Study Name</h6>
                                <p>Increases agility with MRI Investmen Model</p>
                                <a class="with-caret text-sm" href="#">View Case Sudy</a>
                            </div>
                        </div>
                        <div>
                            <div class="card card--case-study">
                                <div class="card__img"><span style="background-image:url(/mri-software/wp-content/themes/mri-software/assets/images/buildout/logo-test.png);"></span></div>
                                <h6>Really Long Case Study Name</h6>
                                <p>Increases agility with MRI Investmen Model</p>
                                <a class="with-caret text-sm" href="#">View Case Sudy</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php 
        /*
         * START OF Case Studies
        */ 
        ?>
        <div class="flex flex--testimonials bg--circle bg--circle--gray">
            <div class="grid grid--middle">
                <div class="grid__item col-1-1 l--col-3-12 pt__2 pb__2">
                    <div class="block__wrap">
                        <div class="block__heading">Customer Testimonials</div>
                        <div class="block__content">
                            <p>Our moms think we’re pretty cool. So do our customers.</p>
                            <a class="btn btn--secondary" href="#"><span>View All Testimonials</span></a>
                        </div>
                    </div>
                </div>
                <div class="grid__item col-1-1 l--col-9-12 pt__2 pb__2">
                    <div class="slider">
                        <div>
                            <div class="card card--testimonial">
                                <p>“Commanding the largest battle station in the galaxy and tracking maintenance on reactors grows tiresome. MRI Workspeed helped us organize the garrison and communicate with Imperial engineers on each level.”</p>
                                <h6>Darth Vader</h6>
                                <p class="h7">Sith Lord, First Galactic Empire</p>
                                <a class="with-caret text-sm" href="#">View Full Testimonial</a>
                            </div>
                        </div>
                        <div>
                            <div class="card card--testimonial">
                                <p>“On Mars, there’s a limited amount of living space and even less breathable air. MRI software offered us a flexible solution to manage our assets and leasing of our living space. It truly helped us control the planet.”</p>
                                <h6>Vilos Cohaagen</h6>
                                <p class="h7">Chief Administrator, Mars</p>
                                <a class="with-caret text-sm" href="#">View Full Testimonial</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php 
        /*
         * START OF 50/50 CONTENT
        */ 
        ?>
        <div class="flex flex--left_right_columns">
            <div class="grid">
                <div class="grid__item col-1-1 l--col-1-2">
                    <div class="block__image" style="background-image: url('/ui/images/50-50-test-img.jpg');"></div>
                </div>
                <div class="grid__item col-1-1 l--col-1-2">
                    <div class="block__right-column">
                        <div class="block__sub-heading">Resident Management</div>
                        <div class="block__heading h2">Let your business run wide open.</div>
                        <div class="block__content">
                            <p>Real estate software shouldn’t hold you back. That’s why MRI Software liberates real estate owners, operators and investors with the most flexible and connected technology platform in the industry. We’re in the business of empowering your organization to grow its potential and succeed on its own terms.</p>
                        </div>
                        <button class="btn btn--primary"><span>Contact Us</span></button>
                        <button class="btn btn--ghost"><span>Contact Us</span></button>
                    </div>
                </div>
            </div>
        </div>
        <div class="flex flex--left_right_columns">
            <div class="grid">
                <div class="grid__item col-1-1 l--col-1-2 push--l--col-1-2">
                    <div class="block__image" style="background-image: url('/ui/images/50-50-test-img.jpg');"></div>
                </div>
                <div class="grid__item col-1-1 l--col-1-2 pull--l--col-1-2">
                    <div class="block__left-column">
                        <div class="block__heading h2">Kick limitations to the curb.</div>
                        <div class="block__content">
                            <p>Too often, your capabilities are limited by software. Holding you back just isn’t our style. We turn the tables with an open and connected ecosystem to give you options that single-stack providers just can’t match.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="flex flex--left_right_columns">
            <div class="grid">
                <div class="grid__item col-1-1 l--col-1-2">
                    <div class="block__image contain" style="background-image: url('/ui/images/monitor-img.jpg');"></div>
                </div>
                <div class="grid__item col-1-1 l--col-1-2">
                    <div class="block__right-column">
                        <div class="block__heading h2">Market Connect Benefits</div>
                        <div class="block__content">
                            <ul class="checkmark">
                                <li>Higher conversion rates</li>
                                <li>Improved online user experience</li>
                                <li>Streamlined leasing processes
                                    <ul>
                                        <li>Higher conversion rates</li>
                                        <li>Improved online user experience</li>
                                        <li>Streamlined leasing processes</li>
                                    </ul>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="flex flex--left_right_columns">
            <div class="grid">
                <div class="grid__item col-1-1 l--col-1-2 push--l--col-1-2">
                    <div class="block__image" style="background-image: url('/ui/images/50-50-test-img.jpg');"></div>
                </div>
                <div class="grid__item col-1-1 l--col-1-2 pull--l--col-1-2">
                    <div class="block__left-column">
                        <div class="block__heading h3">Our Solutions by Market</div>
                        <div class="block__content">
                            <p>We listened to you and built solutions that meet the everyday demands of your role.</p>
                            <ul>
                                <li><a href="#">Afffordable Housing</a></li>
                                <li><a href="#">Afffordable Housing</a></li>
                                <li><a href="#">Afffordable Housing</a></li>
                                <li><a href="#">Afffordable Housing</a></li>
                                <li><a href="#">Afffordable Housing</a></li>
                                <li><a href="#">Afffordable Housing</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php 
        /*
         * START OF IMAGE BREAK
        */ 
        ?>
        <div class="flex flex--image_break bg--navy" style="background-image: url('/ui/images/image-break-test-bg.jpg');">
            <div class="wrapper wrapper--narrow">
                <div class="grid">
                    <div class="grid__item col-1-1">
                        <div class="block__heading">We build solutions that work for you.</div>
                        <div class="block__content">
                            <p>MRI software has been setting real estate businesses up to win since 1971. Let’s just say customer success is in our DNA. We built an open ecosystem of solutions to fit your exact needs.</p>
                        </div>
                        <a href="#" class="btn btn--primary">Learn More</a>
                    </div>
                </div>
            </div>
        </div>
        <?php 
        /*
         * START OF RELATED CONTENT
        */ 
        ?>
        <div class="flex flex--related_content flex--has-bg" style="background-image:url(/mri-software/wp-content/themes/mri-software/assets/images/global/related-content-bg.jpg);">
            <div class="wrapper">
                <div class="block__heading text-center">H2- Picture yourself back at the helm.</div>
                <div class="block__sub-content text-center">
                    <p>When we aren’t busy solving the world’s real-estate management problems, we get a kick out of helping you learn. What can we say? We love this stuff.</p>
                </div>
                <div class="grid">
                    <div class="grid__item col-1-1 s--col-1-2 l--col-1-4 pt__4 vertical_center">
                        <div class="card card--post">
                            <div class="card__img"><img src="/ui/images/post-image.jpg"></div>
                            <span class="text-sm card__subtitle"><i class="fa fa-desktop"></i>Webinar | April 8-10, 2017</span>
                            <h5 class="card__title">In Case You Missed It: MIPIM 2017</h5>
                            <a class="text-sm with-caret" href="#">View the Webinar</a>
                        </div>
                    </div>
                    <div class="grid__item col-1-1 s--col-1-2 l--col-1-4 pt__4 vertical_center">
                        <div class="card card--post">
                            <div class="card__img"><img src="/ui/images/post-image.jpg"></div>
                            <span class="text-sm card__subtitle"><i class="fa fa-desktop"></i>Webinar | April 8-10, 2017</span>
                            <h5 class="card__title">5 Questions you Should Ask Now to Make your Buildings and Management Teams More Profitable in 2017</h5>
                            <a class="text-sm with-caret" href="#">View the Webinar</a>
                        </div>
                    </div>
                    <div class="grid__item col-1-1 s--col-1-2 l--col-1-4 pt__4 vertical_center">
                        <div class="card card--post">
                            <div class="card__img"><img src="/ui/images/post-image.jpg"></div>
                            <span class="text-sm card__subtitle"><i class="fa fa-desktop"></i>Webinar | April 8-10, 2017</span>
                            <h5 class="card__title">How Profitable are your Buildings Compared to your Peers?</h5>
                            <a class="text-sm with-caret" href="#">View the Webinar</a>
                        </div>
                    </div>
                    <div class="grid__item col-1-1 s--col-1-2 l--col-1-4 pt__4 vertical_center">
                        <div class="card card--post">
                            <div class="card__img"><img src="/ui/images/post-image.jpg"></div>
                            <span class="text-sm card__subtitle"><i class="fa fa-desktop"></i>Webinar | April 8-10, 2017</span>
                            <h5 class="card__title"><span class="card__cat">[Partner Webinar]</span> The Ultimate Guide to Project Bidding for CRE Owners and Managers</h5>
                            <a class="text-sm with-caret" href="#">View the Webinar</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php 
        /*
         * START OF CALLOUTS
        */ 
        ?>
        <div class="flex flex--callouts">
            <div class="callout callout--inline bg--navy">
                <div class="wrapper wrapper--narrow">
                    <div class="grid grid--middle">
                        <div class="grid__item col-1-1 m--col-3-5">
                            <div class="block__heading callout__heading">Let’s Set your Business Free.</div>
                            <div class="block__content"><p>Get Started.</p></div>
                        </div>
                        <div class="grid__item col-1-1 m--col-2-5">
                            <a href="#" class="btn btn--ghost--invert"><span>Learn More</span></a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="callout callout--inline callout--pattern callout--pattern--light bg--cool-green">
                <div class="wrapper wrapper--narrow">
                    <div class="grid grid--middle">
                        <div class="grid__item col-1-1 m--col-3-5">
                            <div class="block__heading callout__heading">Let’s Set your Business Free.</div>
                            <div class="block__content"><p>Get Started.</p></div>
                        </div>
                        <div class="grid__item col-1-1 m--col-2-5">
                            <a href="#" class="btn btn--ghost--invert"><span>Learn More</span></a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="callout callout--inline callout--pattern callout--pattern--dark bg--b-gray-lt">
                <div class="wrapper wrapper--narrow">
                    <div class="grid grid--middle">
                        <div class="grid__item col-1-1 m--col-3-5">
                            <div class="block__heading callout__heading">Let’s Set your Business Free.</div>
                            <div class="block__content"><p>Get Started.</p></div>
                        </div>
                        <div class="grid__item col-1-1 m--col-2-5">
                            <a href="#" class="btn btn--ghost"><span>Learn More</span></a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="grid grid--full">
                <div class="grid__item col-1-1 m--col-1-2">
                    <div class="callout callout--inline bg--cool-green">
                        <div class="block__heading callout__heading">Let’s Set your Business Free.</div>
                        <div class="block__content"><p>Get Started.</p></div>
                        <a href="#" class="btn btn--ghost--invert"><span>Learn More</span></a>
                    </div>
                </div>
                <div class="grid__item col-1-1 m--col-1-2">
                    <div class="callout callout--inline bg--md-blue">
                        <div class="block__heading callout__heading">Let’s Set your Business Free.</div>
                        <div class="block__content"><p>Get Started.</p></div>
                        <a href="#" class="btn btn--ghost--invert"><span>Learn More</span></a>
                    </div>
                </div>
            </div>
            <div class="grid grid--full">
                <div class="grid__item col-1-1 m--col-1-3">
                    <div class="callout callout--inline bg--coral">
                        <div class="block__heading callout__heading">Let’s Set your Business Free.</div>
                        <div class="block__content"><p>Get Started.</p></div>
                        <a href="#" class="btn btn--ghost--invert"><span>Learn More</span></a>
                    </div>
                </div>
                <div class="grid__item col-1-1 m--col-1-3">
                    <div class="callout callout--inline bg--orange">
                        <div class="block__heading callout__heading">Let’s Set your Business Free.</div>
                        <div class="block__content"><p>Get Started.</p></div>
                        <a href="#" class="btn btn--ghost--invert"><span>Learn More</span></a>
                    </div>
                </div>
                <div class="grid__item col-1-1 m--col-1-3">
                    <div class="callout callout--inline bg--blue">
                        <div class="block__heading callout__heading">Let’s Set your Business Free.</div>
                        <div class="block__content"><p>Get Started.</p></div>
                        <a href="#" class="btn btn--ghost--invert"><span>Learn More</span></a>
                    </div>
                </div>
            </div>
            <div class="grid grid--full">
                <div class="grid__item col-1-1 m--col-1-2 l--col-1-4">
                    <div class="callout callout--inline bg--b-gray-lt">
                        <div class="block__heading callout__heading">Let’s Set your Business Free.</div>
                        <div class="block__content"><p>Get Started.</p></div>
                        <a href="#" class="with-caret"><span>Learn More</span></a>
                    </div>
                </div>
                <div class="grid__item col-1-1 m--col-1-2 l--col-1-4">
                    <div class="callout callout--inline bg--b-gray-md">
                        <div class="block__heading callout__heading">Let’s Set your Business Free.</div>
                        <div class="block__content"><p>Get Started.</p></div>
                        <a href="#" class="with-caret"><span>Learn More</span></a>
                    </div>
                </div>
                <div class="grid__item col-1-1 m--col-1-2 l--col-1-4">
                    <div class="callout callout--inline bg--b-gray-nm">
                        <div class="block__heading callout__heading">Let’s Set your Business Free.</div>
                        <div class="block__content"><p>Get Started.</p></div>
                        <a href="#" class="with-caret"><span>Learn More</span></a>
                    </div>
                </div>
                <div class="grid__item col-1-1 m--col-1-2 l--col-1-4">
                    <div class="callout callout--inline bg--b-gray-dk">
                        <div class="block__heading callout__heading">Let’s Set your Business Free.</div>
                        <div class="block__content"><p>Get Started.</p></div>
                        <a href="#" class="with-caret"><span>Learn More</span></a>
                    </div>
                </div>
            </div>
        </div>

        <script type='text/javascript' src='/wp-content/themes/cleanslate/assets/js/min/plugins.min.js'></script>
        <script type='text/javascript' src='/wp-content/themes/cleanslate/assets/js/min/main.min.js'></script>
	</body>
</html>