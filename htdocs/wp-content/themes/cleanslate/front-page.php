<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package cleanslate
 */

get_header(); ?>

<div class="portfolio-block wrapper">
	<h2>Featured Projects</h2>
	<div class="project--card">
	<?php

		$projects = get_field('projects');

		if($projects):
			$post_count = count(get_field('projects'));
			
			$project_slider = array();
			foreach( $projects as $project ):
			   $project_slider[] = $project;
			endforeach;

		
			$i = 1;
			foreach( $project_slider as $slider):
				if( $i == 1 ){
					echo '<div class="slider slider-big">';
				}
			?>
				<div>
					<a href="<?php echo get_permalink($slider->ID); ?>"><img class="project-slider" src="<?php echo get_field('slider_image', $slider->ID); ?>"></a>
				</div>
			<?php
				if( $i == $post_count ){
					echo '</div>';
				}
				$i++;

			endforeach;

			echo '<div class="right-half">';
			$i = 1;
			foreach( $project_slider as $slider):
				if( $i == 1 ){
					echo '<div class="slider slider-content">';
				}
			?>
				<div>
					<a href="<?php echo get_permalink($slider->ID); ?>" class="h3 project--title"><?php echo get_the_title($slider->ID); ?></a>
					<p class="project--excerpt"><?php echo get_the_excerpt($slider->ID); ?><span><a href="<?php echo get_permalink($slider->ID); ?>"> {Read More}</a></span></p>
				</div>
			<?php
				if( $i == $post_count ){
					echo '</div>';
				}
				$i++;

			endforeach;

			$i = 1;
			foreach( $project_slider as $slider):
				if( $i == 1 ){
					echo '<div class="slider slider-nav">';
				}
			?>
				<div>
					<img class="project-slider" src="<?php echo get_field('slider_image', $slider->ID); ?>"/>
				</div>
			<?php
				if( $i == $post_count ){
					echo '</div>';
				}
				$i++;

			endforeach;
			echo '</div>';
		endif;
	?>
	</div>
</div>

<div class="about-block wrapper wrapper--sixty">
	<div class="grid grid--middle" style="background-image:url(<?php echo get_stylesheet_directory_uri(); ?>/assets/images/global/working.jpg);">
		<div class="grid__item xl--col-3-5 col-1-1 text-left about-block__content">
			<p><?php the_field('hp_about_content') ?></p>
			<a class="btn btn--white" href="<?php the_field('hp_about_button_url') ?>"><?php the_field('hp_about_button_text') ?></a>
		</div><!--
		--><div class="grid__item xl--col-2-5 col-1-1 text-right">
			<img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/global/sewing_closeup.jpg">
		</div>
	</div>
</div>

<?php
// The Query
$the_query = new WP_Query( array( 'post_type' => 'post', 'posts_per_page' => 2, 'post_status' => 'publish' ) );

// The Loop
if ( $the_query->have_posts() ) {
	echo '<div class="blog-block wrapper wrapper--seventy">';
	while ( $the_query->have_posts() ) {
		$the_query->the_post();
		?>
		<div class="blog-block__post">
			<div class="blog-block__title">
				<a href="<?php echo get_permalink(); ?>" class="h3"><?php echo get_the_title(); ?></a>
				<p><?php echo get_the_date(); ?>  | tags:  <?php the_tags(''); ?></p>
			</div>
			<div class="grid">
				<div class="grid__item col-1-2 s--col-1-4">
					<a href="<?php echo get_permalink(); ?>"><img src="<?php echo the_post_thumbnail_url() ?>"></a>
				</div><!--
				--><div class="grid__item col-1-2 s--col-3-4">
					<p class="blog-block__excerpt"><?php echo get_the_excerpt(); ?> <span><a href="<?php echo get_permalink(); ?>">{Read More}</a></span></p>
				</div>
			</div>
		</div>
		<?php
	}?>
		<div class="text-center">
			<a class="btn btn--primary" href="<?php get_home_url(); ?>/blog/"><?php _e('View all', 'cleanslate'); ?></a>
		</div>
	</div>
	<?php
	/* Restore original Post Data */
	wp_reset_postdata();
}
?>

<?php
get_footer();