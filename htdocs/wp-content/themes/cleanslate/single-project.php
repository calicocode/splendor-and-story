<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package cleanslate
 */

get_header(); ?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">
			
		<?php
		
		while ( have_posts() ) : the_post();

		?>

			<div class="project-content wrapper wrapper--sixty">

				<?php if( have_rows('detail_slider') ): ?>

				<div class="slider-project wrapper wrapper--seventy">

				<?php while( have_rows('detail_slider') ): the_row(); 

					// vars
					$image = get_sub_field('image');
					$caption = get_sub_field('caption');

					?>

					<div>
						<img class="project-slider" src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt'] ?>" >
						<p class="caption"><?php echo $caption; ?></p>
					</div>
					

				<?php endwhile; ?>

				</div>

			<?php endif; ?>

			<?php
				the_content( sprintf(
					/* translators: %s: Name of current post. */
					wp_kses( __( 'Continue reading %s <span class="meta-nav">&rarr;</span>', 'cleanslate' ), array( 'span' => array( 'class' => array() ) ) ),
					the_title( '<span class="screen-reader-text">"', '"</span>', false )
				) );
			?>

			</div>
			<div class="related_projects wrapper wrapper--sixty text-center">
				<h3>Related Projects</h3>
				<?php 

				$posts = get_field('related_projects');
				$i = 1;
				if( $posts ): ?>
				<div class="grid">
				<!--
				    <?php foreach( $posts as $post): // variable must be called $post (IMPORTANT) ?>
				        <?php setup_postdata($post); ?>
					    --><div class="grid__item m--col-1-4 col-1-2">
					        <a href="<?php echo get_permalink(); ?>"><img src="<?php echo the_post_thumbnail_url() ?>"></a>
					        <a href="<?php echo get_permalink(); ?>" class="h5"><?php echo get_the_title(); ?></a>
					    </div><!--
				    <?php $i++; endforeach; ?>
				    <?php wp_reset_postdata(); // IMPORTANT - reset the $post object so the rest of the page works correctly ?>
				-->
				</div>
				<?php endif; ?>
					<a class="btn btn--primary" href="<?php get_home_url(); ?>/portfolio/"><?php _e('View all', 'cleanslate'); ?></a>
			</div>

		<?php
			

			/*the_post_navigation();

			// If comments are open or we have at least one comment, load up the comment template.
			if ( comments_open() || get_comments_number() ) :
				comments_template();
			endif;*/

		endwhile; // End of the loop.
		?>
		
		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_footer();
