<?php
/**
 * Template Name: Portfolio Template
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package cleanslate
 */

get_header(); ?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">
			<?php
			/*while ( have_posts() ) : the_post();*/
			?>
			<div class="categories">
				<h2 class="text-center">Project Categories</h2>
				<div class="wrapper">
				<?php 
					$terms = get_terms('project_category', array(
					    'hide_empty' => 0,
					    'orderby'    => 'count',
					    'order' => 'DESC',
					) );

					if ( ! empty( $terms ) && ! is_wp_error( $terms ) ){ ?>

					<div class="slider slider-cat">
						<?php foreach($terms as $term){
							$term_link = get_term_link( $term );
							$thumbnail = get_field('project_category_image', 'term_'.$term->term_id);
	                        if($thumbnail) { ?>

	                        	<div class="text-center">
							        <a href="<?php echo $term_link; ?>"><img src="<?php echo $thumbnail ?>"></a>
							        <a href="<?php echo $term_link; ?>" class="h5"><?php echo $term->name; ?></a>
							    </div>
	                    <?php
	                        }
						} 
					} ?>
					</div>
				</div>
			</div>
				<?php
				$paged = ( get_query_var('paged') ) ? get_query_var('paged') : 1;
				$args = array(
					'post_type' => 'project',
					'posts_per_page' => 4,
					'paged' => $paged
			  	);
			  	$the_query = new WP_Query( $args );
			  	
			  	if ( $the_query->have_posts() ) {
				?>
			<div class="all_projects wrapper" id="all_projects">
				<h2 class="text-center">All Projects</h2>
				<div class="grid wrapper">
				<!--
				<?php
					while ( $the_query->have_posts() ) {
						$the_query->the_post();
				?>
					--><div class="grid__item l--col-1-4 s--col-1-2 col-1-1 text-center">
				        <a href="<?php echo get_permalink(); ?>"><img src="<?php echo the_post_thumbnail_url() ?>"></a>
				        <a href="<?php echo get_permalink(); ?>" class="h5"><?php echo get_the_title(); ?></a>
				    </div><!--
				<?php
					}
				?>
				-->
				</div>
				<?php
					if (function_exists(custom_pagination)) {
					custom_pagination($the_query->max_num_pages,"",$paged);
			    	}
				?>
					
				<?php
					wp_reset_postdata();
				}
				//get_template_part( 'template-parts/content', 'page' );
			?>
			</div>
		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_footer();
