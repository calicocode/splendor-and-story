<?php
/**
 * Template Name: Blog Template
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package cleanslate
 */

get_header(); ?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">

			<?php
			// The Query
			$paged = ( get_query_var('paged') ) ? get_query_var('paged') : 1;
			$args = array(
					'post_type' => 'post',
					'posts_per_page' => 4,
					'paged' => $paged,
					'post_status' => 'publish'
			  	);
			$the_query = new WP_Query( $args );

			// The Loop
			if ( $the_query->have_posts() ) {
				echo '<div class="blog-block wrapper wrapper--seventy">';
				while ( $the_query->have_posts() ) {
					$the_query->the_post();
					?>
					<div class="blog-block__post">
						<div class="blog-block__title">
							<a href="<?php echo get_permalink(); ?>" class="h3"><?php echo get_the_title(); ?></a>
							<p><?php echo get_the_date(); ?>  | tags:  <?php the_tags(''); ?></p>
						</div>
						<div class="grid">
							<div class="grid__item col-1-2 s--col-1-4">
								<a href="<?php echo get_permalink(); ?>"><img src="<?php echo the_post_thumbnail_url() ?>"></a>
							</div><!--
							--><div class="grid__item col-1-2 s--col-3-4">
								<p class="blog-block__excerpt"><?php echo get_the_excerpt(); ?> <span><a href="<?php echo get_permalink(); ?>">{Read More}</a></span></p>
							</div>
						</div>
					</div>
					<?php
				}?>

				<?php
					if (function_exists(custom_pagination)) {
						custom_pagination($the_query->max_num_pages,"",$paged);
			    	}
				?>

				</div>
				<?php
				/* Restore original Post Data */
				wp_reset_postdata();
			}
			?>

		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_footer();
