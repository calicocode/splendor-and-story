<?php
/**
 * Template Name: Commissions Template
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package cleanslate
 */

get_header(); ?>

	<div id="primary" class="content-area wrapper wrapper--sixty">
		<main id="main" class="site-main" role="main">
			<div class="general_info" id="general_info">
			<?php
			while ( have_posts() ) : the_post();

				get_template_part( 'template-parts/content', 'page' );

				// If comments are open or we have at least one comment, load up the comment template.
				if ( comments_open() || get_comments_number() ) :
					comments_template();
				endif;

			endwhile; // End of the loop.
			?>
			</div>

			<div class="faqs" id="faqs">
				<h3>FAQs</h3>
				<?php 
				if ( have_rows('QAs') ): ?>
				<ul>
				<?php
					while( have_rows('QAs') ): the_row();
						$question = get_sub_field('question');
						$answer = get_sub_field('answer');

						if( $question ):
				?>
					<li class="faqs__question">
						<a href="#" class="h5"><span><i class="fa fa-plus" aria-hidden="true"></i></span><?php echo $question ?></a>
					</li>
				<?php 	
						endif; 
						if( $answer ):
				?>
					<li class="faqs__answer">
						<p><?php echo $answer ?></p>
					</li>
				<?php 	
						endif; 
					endwhile;
				?>
				</ul>
				<?php endif; ?>
			</div>
		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_footer();