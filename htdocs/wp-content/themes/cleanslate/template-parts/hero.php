<?php 
	$classes = 'hero';
	$style = 'style="background-image:url('.get_stylesheet_directory_uri().'/assets/images/global/hero.jpg);"';
	$title = get_the_title();
	$has_subtitle = false;
	$has_btn = false;
	$has_search = false;
	$has_hr = false;

	if(has_post_thumbnail()) {
		$style = 'style="background-image:url('.get_the_post_thumbnail_url().');"';
	}

	if(is_front_page()) {
		$classes .= ' hero--home';
		$has_subtitle = true;
		$has_btn = true;
		$title = 'Splendor &amp; Story Costuming';
		$subtitle = 'Costuming blog of Shannon Chopson';
		$btn_url = get_home_url() . '/portfolio/';
		$btn_text = 'Portfolio';
	}

	elseif(is_page( $page = 'commissions' )) {
		$classes .= ' hero--page';
		$has_btn = true;
		$btn_url = 'https://www.etsy.com/?ref=lgo';
		$btn_text = 'Etsy Store';
	}
	
	elseif((is_page( array ($page = 'portfolio', 'blog') )) || (is_search()) || (is_404())) {
		$classes .= ' hero--page';
		if(is_search()) {
			$style = 'style="background-image:url('.get_stylesheet_directory_uri().'/assets/images/global/hero.jpg);"';
			$title = 'Search';
		}
		elseif(is_404()) {
			$title = '404: Page not Found';
		}
		$has_search = true;
	}

	elseif(is_tax()) {
		$classes .= ' hero--category';
		$tax_id = get_queried_object()->term_id;
		$image = get_field('project_category_hero', 'term_'.$tax_id);

		if($image) {
			$style = 'style="background-image:url('.$image.');"';
		}
		$title = single_cat_title("", false);
	}

	elseif(is_singular(  array( 'project', 'post' ) )) {
		$classes .= ' hero--project';
		$has_hr = true;
		$has_subtitle = true;
		$subtitle = get_the_date();
	}

	else {
		$classes .= ' hero--page';
	}
?>


<div class="<?php echo $classes; ?>" <?php echo $style; ?>> 
	<div class="hero__content text-center">
		<h1><?php esc_html_e( $title ); ?></h1>

		<?php if($has_hr): ?>
		<hr>
		<?php endif; ?>
		
		<?php if($has_subtitle): ?>
		<h3 class="subtitle"><?php esc_html_e( $subtitle ); ?></h3>
		<?php endif; ?>
		
		<?php if($has_btn): ?>
		<a class="btn btn--white" href="<?php echo $btn_url; ?>"><?php esc_html_e( $btn_text ); ?></a>
		<?php endif; ?>

		<?php if($has_search): ?>
			<?php get_search_form(); ?>
		<?php endif; ?>
	</div>
</div>