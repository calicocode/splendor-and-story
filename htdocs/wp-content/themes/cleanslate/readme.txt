=== cleanslate ===

Contributors: Red Clay Interactive

Requires at least: 4.0
Tested up to: 4.7
Stable tag: 1.0.0
License: GNU General Public License v2 or later
License URI: LICENSE

A starter theme called Clean Slate, based on Underscores.

== Description ==

This is a boilerplate template based off of the Underscores WordPress theme. This theme is meant to be heavily modified, so do not use it as a Parent Theme.

== Installation ==

1. In your admin panel, go to Appearance > Themes and click the Add New button.
2. Click Upload and Choose File, then select the theme's .zip file. Click Install Now.
3. Click Activate to use your new theme right away.

== Frequently Asked Questions ==

= Does this theme support any plugins? =

Yes.  Clean Slate includes support for Infinite Scroll in Jetpack, and many other plugins available for WordPress.  The sky's the limit!

== Changelog ==

= 1.0 - January 31 2017 =
* Initial release

== Credits ==

* Based on Underscores http://underscores.me/, (C) 2012-2016 Automattic, Inc., [GPLv2 or later](https://www.gnu.org/licenses/gpl-2.0.html)
* normalize.css http://necolas.github.io/normalize.css/, (C) 2012-2016 Nicolas Gallagher and Jonathan Neal, [MIT](http://opensource.org/licenses/MIT)
