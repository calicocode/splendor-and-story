(function($){

	/* Get accurate Viewport sizing (no jQuery) */
    var viewportwidth;
    var viewportheight;

    function getWindowSize(){
        if (typeof window.innerWidth != 'undefined'){
            viewportwidth = window.innerWidth,
            viewportheight = window.innerHeight
        }else if (typeof document.documentElement != 'undefined' && typeof document.documentElement.clientWidth != 'undefined' && document.documentElement.clientWidth != 0){
            viewportwidth = document.documentElement.clientWidth,
            viewportheight = document.documentElement.clientHeight
        }else{
            viewportwidth = document.getElementsByTagName('body')[0].clientWidth,
            viewportheight = document.getElementsByTagName('body')[0].clientHeight
        }
    }

    
    function initSlider() {
        $('.slider-big').slick({
          slidesToShow: 1,
          slidesToScroll: 1,
          arrows: false,
          fade: true,
          draggable: false,
          asNavFor: '.slider'
        });

        $('.slider-content').slick({
          slidesToShow: 1,
          slidesToScroll: 1,
          arrows: false,
          fade: true,
          draggable: false,
          asNavFor: '.slider',
          responsive: [
              {
                breakpoint: 1024,
                settings: {
                  adaptiveHeight: true,
                }
              }
            ]
        });

        $('.slider-nav').slick({
            centerMode: true,
            centerPadding: '0',
            slidesToShow: 3,
            slidesToScroll: 1,
            swipeToSlide: true,
            focusOnSelect: true,
            arrows: true,
            infinite: true,
            speed: 500,
            fade: false,
            cssEase: 'linear',
            asNavFor: '.slider'
        });

        $('.slider-cat').slick({
            centerMode: true,
            centerPadding: '0',
            slidesToShow: 5,
            slidesToScroll: 5, // Make this the same as slidesToShow
            swipeToSlide: true,
            focusOnSelect: true,
            arrows: true,
            dots: true,
            infinite: true,
            speed: 500,
            fade: false,
            cssEase: 'linear',
            responsive: [
              {
                breakpoint: 1024,
                settings: {
                  slidesToShow: 3,
                  slidesToScroll: 2,
                }
              },
              {
                breakpoint: 768,
                settings: {
                  slidesToShow: 2,
                  slidesToScroll: 2
                }
              },
              {
                breakpoint: 600,
                settings: {
                  slidesToShow: 1,
                  slidesToScroll: 1
                }
              }
            ]
        });

        $('.slider-project').slick({
          slidesToShow: 1,
          slidesToScroll: 1,
          arrows: true,
          infinite: true,
          fade: true,
          draggable: true,
          adaptiveHeight: true,
        });
    }
    
    function initMasonry() {
        
        $('.YOUR_CONTAINER').masonry({
          columnWidth: '.YOUR_MASONRY_ELEMENT_CONTAINER',
          gutter: 30,
          itemSelector: '.YOUR_MASONRY_ELEMENT'
        });
        
    }

    function paginationScroll() {
        
        var _href = $('a.page-numbers').attr('href');
        $('a.page-numbers').attr('href', _href + '#all_projects');

    }

    function faqs() {

      var speed = "500";
      
      $('li.faqs__question').click( function () {
        event.preventDefault();
        $(this).next().slideToggle(speed).siblings('li.faqs__answer').slideUp();
        
        var i = $(this).find('i');
        // remove Rotate class from all icons except the active
        $('i').not(i).removeClass('rotate');
        i.toggleClass('rotate');
      });

    }


    /*
     *
     * Equal Height jQuery Snippet.  Using this for footer and grid accordion
     *
     */
    var equalheight = function (container){

        var currentTallest = 0,
            currentRowStart = 0,
            rowDivs = [],
            $el,
            topPosition = 0,
            currentDiv;
    
        $(container).each(function () {

            $el = $(this);
            $el.height('auto');
            topPosition = $el.position().top;

            if (currentRowStart < topPosition - 1 || currentRowStart > topPosition + 1) {
                for (currentDiv = 0; currentDiv < rowDivs.length; currentDiv++) {
                    rowDivs[currentDiv].height(currentTallest);
                }
                rowDivs = [];
                currentRowStart = topPosition;
                currentTallest = $el.height();
                rowDivs.push($el);
            }
            else {
                rowDivs.push($el);
                currentTallest = Math.max(currentTallest, $el.height());
            }
        });
        
        for (currentDiv = 0; currentDiv < rowDivs.length; currentDiv++) {
            rowDivs[currentDiv].css('height', currentTallest);
        }
    };

	/* Document Ready */
	$(document).ready(function() {
		getWindowSize();
        equalheight('.vertical_center');

        initSlider();
        initMasonry();

        if ($('#all_projects').length) {
          paginationScroll();
        }

        if ($('#faqs').length) {
          faqs();
        }


        //Sidr
        $('#mobile-trigger').sidr({
            name: 'sidr',
            side: 'right',
            renaming: 'false',
            source : function() {
                return '<div id="mobile-menu"><span class="sidr__close">&times;</span><div class="sidr__inner">' + $('#site-navigation').html() + '</div></div>';
            }
        });
        $('.sidr__close').click(function() {
            $.sidr('close','sidr');
        });



        // Double-click dropdown functionality for mobile (sidr) sub menus
        $(".sidr__inner > ul > li.menu-item-has-children").each(function() {
            var count = 0;
            var $this_li = $(this);
            $this_li.click(function() {
              count++;
              if ((count % 2) != 0) {
                event.preventDefault();
                //Toggle the child but don't include them in the hide selector using .not()
                $('li > ul').not($this_li.children("ul").toggle()).hide();
                if ($('.sub-menu').hasClass('active')) {
                    $('.sub-menu').removeClass('active');
                }
                else {
                    $('.sub-menu').addClass('active');
                }
              }
              else {
                $.sidr('close','sidr');
              }
            });
            
        });

	});

	$(window).on("resize", function() {
		getWindowSize();
        equalheight('.vertical_center');
	});

  $(window).on('load', function() { // makes sure the whole site is loaded 
    $('#status').fadeOut(); // will first fade out the loading animation 
    $('#preloader').delay(350).fadeOut('slow'); // will fade out the white DIV that covers the website. 
    $('body').delay(350).css({'overflow':'visible'});
  })

  /*function init() {
  var imgDefer = document.getElementsByTagName('img');
  for (var i=0; i<imgDefer.length; i++) {
    if(imgDefer[i].getAttribute('data-src')) {
      imgDefer[i].setAttribute('src',imgDefer[i].getAttribute('data-src'));
      } 
    } 
  }
  window.onload = init;*/

})(jQuery);