/*------------------------------------*\
    $CSSWIZARDRY-GRIDS (https://github.com/csswizardry/csswizardry-grids)

    MODIFIED EDITION
\*------------------------------------*/
/**
 * CONTENTS
 * INTRODUCTION.........How the grid system works.
 * VARIABLES............Your settings.
 * MIXINS...............Library mixins.
 * GRID SETUP...........Build the grid structure.
 * HELPERS..............Create helper show/hide classes around our breakpoints.
 * WIDTHS...............Build our responsive widths around our breakpoints.
 * PUSH.................Push classes.
 * PULL.................Pull classes.
 */





/*------------------------------------*\
    $INTRODUCTION
\*------------------------------------*/
/**
 * csswizardry grids provides you with widths to suit a number of breakpoints
 * designed around devices of a size you specify. Out of the box, csswizardry
 * grids caters to the following types of device:
 *
 * palm     --  palm-based devices, like phones and small tablets
 * lap      --  lap-based devices, like iPads or laptops
 * portable --  all of the above
 * desk     --  stationary devices, like desktop computers
 * regular  --  any/all types of device
 *
 * These namespaces are then used in the library to give you the ability to
 * manipulate your layouts based around them, for example:
 *
   <div class="grid__item  col-1-1  lap--col-1-2  desk--col-1-3">
 *
 * This would give you a grid item which is 100% width unless it is on a lap
 * device, at which point it become 50% wide, or it is on a desktop device, at
 * which point it becomes 33.333% width.
 *
 * csswizardry grids also has push and pull classes which allow you to nudge
 * grid items left and right by a defined amount. These follow the same naming
 * convention as above, but are prepended by either `push--` or `pull--`, for
 * example:
 *
   `class="grid__item  col-1-2  push--col-1-2"`
 *
 * This would give you a grid item which is 50% width and pushed over to the
 * right by 50%.
 *
 * All classes in csswizardry grids follow this patten, so you should fairly
 * quickly be able to piece together any combinations you can imagine, for
 * example:
 *
   `class="grid__item  col-1-1  lap--col-1-2  desk--col-1-3  push--desk--col-1-3"`
 *
   `class="grid__item  col-1-4  palm--col-1-2  push--palm--col-1-2"`
 *
   `class="grid__item  palm--col-1-3  desk--5-12"`
 */





/*------------------------------------*\
    $VARIABLES
\*------------------------------------*/
/**
 * If you want numbers instead of names (example : 'one-whole' becomes '1-1', 'third-quarter' becomes '3-4' etc...)
 */
$use-numbers-instead-names: true !default;

/**
 * When the first char of a class is a digit, you need to convert it to his unicode
 * http://stackoverflow.com/a/21229901
 * http://www.w3.org/TR/css3-syntax/#escaping
 *
 * But Sass doesn't support it yet...
 * https://github.com/sass/sass/issues/255
 *
 * So while waiting for this is possible, this is the code.
 */
$grid-item-prefix: 'col-';

$one:       if($use-numbers-instead-names, #{$grid-item-prefix}1, "one");
$two:       if($use-numbers-instead-names, #{$grid-item-prefix}2, "two");
$three:     if($use-numbers-instead-names, #{$grid-item-prefix}3, "three");
$four:      if($use-numbers-instead-names, #{$grid-item-prefix}4, "four");
$five:      if($use-numbers-instead-names, #{$grid-item-prefix}5, "five");
$six:       if($use-numbers-instead-names, #{$grid-item-prefix}6, "six");
$seven:     if($use-numbers-instead-names, #{$grid-item-prefix}7, "seven");
$eight:     if($use-numbers-instead-names, #{$grid-item-prefix}8, "eight");
$nine:      if($use-numbers-instead-names, #{$grid-item-prefix}9, "nine");
$ten:       if($use-numbers-instead-names, #{$grid-item-prefix}10, "ten");
$eleven:    if($use-numbers-instead-names, #{$grid-item-prefix}11, "eleven");

$whole:     if($use-numbers-instead-names, "1", "whole");
$half:      if($use-numbers-instead-names, "2", "half");
$third:     if($use-numbers-instead-names, "3", "third");
$quarter:   if($use-numbers-instead-names, "4", "quarter");
$fifth:     if($use-numbers-instead-names, "5", "fifth");
$sixth:     if($use-numbers-instead-names, "6", "sixth");
$seventh:   if($use-numbers-instead-names, "7", "seventh");
$eighth:    if($use-numbers-instead-names, "8", "eighth");
$ninth:     if($use-numbers-instead-names, "9", "ninth");
$tenth:     if($use-numbers-instead-names, "10", "tenth");
$eleventh:  if($use-numbers-instead-names, "11", "eleventh");
$twelfth:   if($use-numbers-instead-names, "12", "twelfth");


/**
 * If you are building a non-responsive site but would still like to use
 * csswizardry-grids, set this to ‘false’:
 */
$responsive:            true !default;


/**
 * Is this build mobile first? Setting to ‘true’ means that all grids will be
 * 100% width if you do not apply a more specific class to them.
 */
$mobile-first:          true !default;


/**
 * Set the spacing between your grid items.
 */
$gutter:                $base-grid-gutter !default; // 30px


/**
 * Would you like Sass’ silent classes, or regular CSS classes?
 */
$use-silent-classes:    false !default;


/**
 * Would you like push and pull classes enabled?
 */
$push:                  true !default;
$pull:                  false !default;


/**
 * Using `inline-block` means that the grid items need their whitespace removing
 * in order for them to work correctly. Set the following to true if you are
 * going to achieve this by manually removing/commenting out any whitespace in
 * your HTML yourself.
 *
 * Setting this to false invokes a hack which cannot always be guaranteed,
 * please see the following for more detail:
 *
 * github.com/csswizardry/csswizardry-grids/commit/744d4b23c9d2b77d605b5991e54a397df72e0688
 * github.com/csswizardry/inuit.css/issues/170#issuecomment-14859371
 */
$use-markup-fix:        true !default;


/**
 * Define your breakpoints. The first value is the prefix that shall be used for
 * your classes (e.g. `.palm--one-half`), the second value is the media query
 * that the breakpoint fires at.
 *
 *      phone-portrait = 20em   = 320px
 *     phone-landscape = 30em   = 480px
 *   tablet-S-portrait = 37.5em = 600px
 *   tablet-M-portrait = 48em   = 768px
 *   tablet-L-portrait = 60em   = 960px
 *   desktop-landscape = 64em   = 1024px
 * desktop-L-landscape = 80em   = 1280px
 */
$breakpoints: (
    'xs' '(min-width: 30em)',
    's' '(min-width: 37.5em)',
    'm' '(min-width: 48em)',
    'l' '(min-width: 60em)',
    'xl' '(min-width: 64em)',
    'xxl' '(min-width: 80em)',
    'xxxl' '(min-width: 92em)',
) !default;


/**
 * Define which namespaced breakpoints you would like to generate for each of
 * widths, push and pull. This is handy if you only need pull on, say, desk, or
 * you only need a new width breakpoint at mobile sizes. It allows you to only
 * compile as much CSS as you need. All are turned on by default, but you can
 * add and remove breakpoints at will.
 *
 * Push and pull shall only be used if `$push` and/or `$pull` and `$responsive`
 * have been set to ‘true’.
 */
$breakpoint-has-widths: ('xs', 's', 'm', 'l', 'xl', 'xxl', 'xxxl') !default;
$breakpoint-has-push:   ('xs', 's', 'm', 'l', 'xl', 'xxl', 'xxxl') !default;
$breakpoint-has-pull:   ('xs', 's', 'm', 'l', 'xl', 'xxl', 'xxxl') !default;


/**
 * You do not need to edit anything from this line onward; csswizardry-grids is
 * good to go. Happy griddin’!
 */
$class-type: if($use-silent-classes, unquote("%"), unquote("."));





/*------------------------------------*\
    $MIXINS
\*------------------------------------*/
/**
 * These mixins are for the library to use only, you should not need to modify
 * them at all.
 *
 * Enclose a block of code with a media query as named in `$breakpoints`.
 */
@mixin grid-media-query($media-query) {
    $breakpoint-found: false;

    @each $breakpoint in $breakpoints {
        $name: nth($breakpoint, 1);
        $declaration: nth($breakpoint, 2);

        @if $media-query == $name and $declaration {
            $breakpoint-found: true;

            @media only screen and #{$declaration} {
                @content;
            }
        }
    }

    @if not $breakpoint-found {
        @warn "Breakpoint ‘#{$media-query}’ does not exist";
    }
}


/**
 * Drop relative positioning into silent classes which can’t take advantage of
 * the `[class*="push--"]` and `[class*="pull--"]` selectors.
 */
@mixin silent-relative {
    @if $use-silent-classes {
        position:relative;
    }
}





/*------------------------------------*\
    $GRID SETUP
\*------------------------------------*/
/**
 * 1. Allow the grid system to be used on lists.
 * 2. Remove any margins and paddings that might affect the grid system.
 * 3. Apply a negative `margin-left` to negate the columns’ gutters.
 */
#{$class-type}grid {
    list-style:none;                /* [1] */
    margin:0;                       /* [2] */
    padding:0;                      /* [2] */
    margin-left:-$gutter;           /* [3] */
    @if not $use-markup-fix {
        letter-spacing:-0.32em;
    }
}

@if not $use-markup-fix {
    /* Opera hack */
    .opera:-o-prefocus,
    #{$class-type}grid {
        word-spacing:-0.43em;
    }
}


/**
 * 1. Cause columns to stack side-by-side.
 * 2. Space columns apart.
 * 3. Align columns to the tops of each other.
 * 4. Full-width unless told to behave otherwise.
 * 5. Required to combine fluid widths and fixed gutters.
 */
#{$class-type}grid__item {
    display:inline-block;           /* [1] */
    padding-left:$gutter;           /* [2] */
    vertical-align:top;             /* [3] */
    @if $mobile-first {
        width:100%;                 /* [4] */
    }
    -webkit-box-sizing:border-box;  /* [5] */
       -moz-box-sizing:border-box;  /* [5] */
            box-sizing:border-box;  /* [5] */
    @if not $use-markup-fix {
        letter-spacing:normal;
        word-spacing:normal;
    }
}


/**
 * Reversed grids allow you to structure your source in the opposite order to
 * how your rendered layout will appear. Extends `.grid`.
 */
#{$class-type}grid--rev {
    direction:rtl;
    text-align:left;

    > #{$class-type}grid__item {
        direction:ltr;
        text-align:left;
    }
}


/**
 * Gutterless grids have all the properties of regular grids, minus any spacing.
 * Extends `.grid`.
 */
#{$class-type}grid--full {
    margin-left:0;

    > #{$class-type}grid__item {
        padding-left:0;
    }
}


/**
 * Align the entire grid to the right. Extends `.grid`.
 */
#{$class-type}grid--right {
    text-align:right;

    > #{$class-type}grid__item {
        text-align:left;
    }
}

/**
 * Align the entire grid to the left. Extends `.grid`.
 */
#{$class-type}grid--left {
    text-align:left;

    > #{$class-type}grid__item {
        text-align:left;
    }
}


/**
 * Centered grids align grid items centrally without needing to use push or pull
 * classes. Extends `.grid`.
 */
#{$class-type}grid--center {
    text-align:center;

    > #{$class-type}grid__item {
        text-align:left;
    }
}


/**
 * Align grid cells vertically (`.grid--middle` or `.grid--bottom`). Extends
 * `.grid`.
 */
#{$class-type}grid--middle {

    > #{$class-type}grid__item {
        vertical-align:middle;
    }
}

#{$class-type}grid--bottom {

    > #{$class-type}grid__item {
        vertical-align:bottom;
    }
}


/**
 * Create grids with narrower gutters. Extends `.grid`.
 */
#{$class-type}grid--narrow {
    margin-left:-($gutter / 2);

    > #{$class-type}grid__item {
        padding-left:$gutter / 2;
    }
}


/**
 * Create grids with wider gutters. Extends `.grid`.
 */
#{$class-type}grid--wide {
    margin-left:-($gutter * 2);

    > #{$class-type}grid__item {
        padding-left:$gutter * 2;
    }
}





/*------------------------------------*\
    $WIDTHS
\*------------------------------------*/
/**
 * Create our width classes, prefixed by the specified namespace.
 */
@mixin device-type($namespace:"") {
    $prefix: $class-type + $namespace;

    /**
     * Whole
     */
    #{$prefix}#{$one}-#{$whole}         { width:100%; }


    /**
     * Halves
     */
    #{$prefix}#{$one}-#{$half}          { width:50%; }


    /**
     * Thirds
     */
    #{$prefix}#{$one}-#{$third}         { width:33.333%; }
    #{$prefix}#{$two}-#{$third}        { width:66.666%; }


    /**
     * Quarters
     */
    #{$prefix}#{$one}-#{$quarter}       { width:25%; }
    #{$prefix}#{$two}-#{$quarter}      { @extend #{$prefix}#{$one}-#{$half}; }
    #{$prefix}#{$three}-#{$quarter}    { width:75%; }


    /**
     * Fifths
     */
    #{$prefix}#{$one}-#{$fifth}         { width:20%; }
    #{$prefix}#{$two}-#{$fifth}        { width:40%; }
    #{$prefix}#{$three}-#{$fifth}      { width:60%; }
    #{$prefix}#{$four}-#{$fifth}       { width:80%; }


    /**
     * Sixths
     */
    #{$prefix}#{$one}-#{$sixth}         { width:16.666%; }
    #{$prefix}#{$two}-#{$sixth}        { @extend #{$prefix}#{$one}-#{$third}; }
    #{$prefix}#{$three}-#{$sixth}      { @extend #{$prefix}#{$one}-#{$half}; }
    #{$prefix}#{$four}-#{$sixth}       { @extend #{$prefix}#{$two}-#{$third}; }
    #{$prefix}#{$five}-#{$sixth}       { width:83.333%; }


    /**
     * Sevenths
     */
    #{$prefix}#{$one}-#{$seventh}       { width:14.2857%; }
    #{$prefix}#{$two}-#{$seventh}      { width:28.5714%; }
    #{$prefix}#{$three}-#{$seventh}    { width:42.8571%; }
    #{$prefix}#{$four}-#{$seventh}     { width:57.1428%; }
    #{$prefix}#{$five}-#{$seventh}     { width:71.4285%; }
    #{$prefix}#{$six}-#{$seventh}      { width:85.7142%; }


    /**
     * Eighths
     */
    #{$prefix}#{$one}-#{$eighth}        { width:12.5%; }
    #{$prefix}#{$two}-#{$eighth}       { @extend #{$prefix}#{$one}-#{$quarter}; }
    #{$prefix}#{$three}-#{$eighth}     { width:37.5%; }
    #{$prefix}#{$four}-#{$eighth}      { @extend #{$prefix}#{$one}-#{$half}; }
    #{$prefix}#{$five}-#{$eighth}      { width:62.5%; }
    #{$prefix}#{$six}-#{$eighth}       { @extend #{$prefix}#{$three}-#{$quarter}; }
    #{$prefix}#{$seven}-#{$eighth}     { width:87.5%; }


    /**
     * Ninths
     */
    #{$prefix}#{$one}-#{$ninth}         { width:11.111%; }
    #{$prefix}#{$two}-#{$ninth}        { width:22.222%; }
    #{$prefix}#{$three}-#{$ninth}      { @extend #{$prefix}#{$one}-#{$third}; }
    #{$prefix}#{$four}-#{$ninth}       { width:44.444%; }
    #{$prefix}#{$five}-#{$ninth}       { width:55.555%; }
    #{$prefix}#{$six}-#{$ninth}        { @extend #{$prefix}#{$two}-#{$third}; }
    #{$prefix}#{$seven}-#{$ninth}      { width:77.777%; }
    #{$prefix}#{$eight}-#{$ninth}      { width:88.888%; }


    /**
     * Tenths
     */
    #{$prefix}#{$one}-#{$tenth}         { width:10%; }
    #{$prefix}#{$two}-#{$tenth}        { @extend #{$prefix}#{$one}-#{$fifth}; }
    #{$prefix}#{$three}-#{$tenth}      { width:30%; }
    #{$prefix}#{$four}-#{$tenth}       { @extend #{$prefix}#{$two}-#{$fifth}; }
    #{$prefix}#{$five}-#{$tenth}       { @extend #{$prefix}#{$one}-#{$half}; }
    #{$prefix}#{$six}-#{$tenth}        { @extend #{$prefix}#{$three}-#{$fifth}; }
    #{$prefix}#{$seven}-#{$tenth}      { width:70%; }
    #{$prefix}#{$eight}-#{$tenth}      { @extend #{$prefix}#{$four}-#{$fifth}; }
    #{$prefix}#{$nine}-#{$tenth}       { width:90%; }


    /**
     * Elevenths
     */
    #{$prefix}#{$one}-#{$eleventh}       { width:9.0909%; }
    #{$prefix}#{$two}-#{$eleventh}      { width:18.1818%; }
    #{$prefix}#{$three}-#{$eleventh}    { width:27.2727%; }
    #{$prefix}#{$four}-#{$eleventh}     { width:36.3636%; }
    #{$prefix}#{$five}-#{$eleventh}     { width:45.4545%; }
    #{$prefix}#{$six}-#{$eleventh}      { width:54.5454%; }
    #{$prefix}#{$seven}-#{$eleventh}    { width:63.6363%; }
    #{$prefix}#{$eight}-#{$eleventh}    { width:72.7272%; }
    #{$prefix}#{$nine}-#{$eleventh}     { width:81.8181%; }
    #{$prefix}#{$ten}-#{$eleventh}      { width:90.9090%; }


    /**
     * Twelfths
     */
    #{$prefix}#{$one}-#{$twelfth}       { width:8.333%; }
    #{$prefix}#{$two}-#{$twelfth}      { width:16.666%; }
    #{$prefix}#{$three}-#{$twelfth}    { @extend #{$prefix}#{$one}-#{$quarter}; }
    #{$prefix}#{$four}-#{$twelfth}     { @extend #{$prefix}#{$one}-#{$third}; }
    #{$prefix}#{$five}-#{$twelfth}     { width:41.666% }
    #{$prefix}#{$six}-#{$twelfth}      { @extend #{$prefix}#{$one}-#{$half}; }
    #{$prefix}#{$seven}-#{$twelfth}    { width:58.333%; }
    #{$prefix}#{$eight}-#{$twelfth}    { @extend #{$prefix}#{$two}-#{$third}; }
    #{$prefix}#{$nine}-#{$twelfth}     { @extend #{$prefix}#{$three}-#{$quarter}; }
    #{$prefix}#{$ten}-#{$twelfth}      { @extend #{$prefix}#{$five}-#{$sixth}; }
    #{$prefix}#{$eleven}-#{$twelfth}   { width:91.666%; }
}





/*------------------------------------*\
  $HELPERS
\*------------------------------------*/
/**
 * Create helper show/hide classes, prefixed by the specified namespace.
 */
@mixin device-helper($namespace:"") {
    #{$class-type}#{$namespace}show     { display:inline-block; }
    #{$class-type}#{$namespace}hide     { display:none; }
}


/**
 * Our regular, non-responsive width classes.
 */
@include device-type;
@include device-helper;


/**
 * Our responsive classes, if we have enabled them.
 */
@if $responsive {
    @each $name in $breakpoint-has-widths {
        @include grid-media-query($name) {
            @include device-type('#{$name}--');
        }

        @include grid-media-query($name) {
            @include device-helper('#{$name}--');
        }
    }
}





/*------------------------------------*\
    $PUSH
\*------------------------------------*/
/**
 * Push classes, to move grid items over to the right by certain amounts.
 */
@mixin push-setup($namespace: "") {
    $prefix: $class-type + "push--" + $namespace;

    /**
     * Whole
     */
    #{$prefix}#{$one}-#{$whole}           { left:100%; @include silent-relative; }


    /**
     * Halves
     */
    #{$prefix}#{$one}-#{$half}            { left:50%; @include silent-relative; }


    /**
     * Thirds
     */
    #{$prefix}#{$one}-#{$third}           { left:33.333%; @include silent-relative; }
    #{$prefix}#{$two}-#{$third}          { left:66.666%; @include silent-relative; }


    /**
     * Quarters
     */
    #{$prefix}#{$one}-#{$quarter}         { left:25%; @include silent-relative; }
    #{$prefix}#{$two}-#{$quarter}        { @extend #{$prefix}#{$one}-#{$half}; }
    #{$prefix}#{$three}-#{$quarter}      { left:75%; @include silent-relative; }


    /**
     * Fifths
     */
    #{$prefix}#{$one}-#{$fifth}           { left:20%; @include silent-relative; }
    #{$prefix}#{$two}-#{$fifth}          { left:40%; @include silent-relative; }
    #{$prefix}#{$three}-#{$fifth}        { left:60%; @include silent-relative; }
    #{$prefix}#{$four}-#{$fifth}         { left:80%; @include silent-relative; }


    /**
     * Sixths
     */
    #{$prefix}#{$one}-#{$sixth}           { left:16.666%; @include silent-relative; }
    #{$prefix}#{$two}-#{$sixth}          { @extend #{$prefix}#{$one}-#{$third}; }
    #{$prefix}#{$three}-#{$sixth}        { @extend #{$prefix}#{$one}-#{$half}; }
    #{$prefix}#{$four}-#{$sixth}         { @extend #{$prefix}#{$two}-#{$third}; }
    #{$prefix}#{$five}-#{$sixth}         { left:83.333%; @include silent-relative; }


    /**
     * Sevenths
     */
    #{$prefix}#{$one}-#{$seventh}         { left:14.2857%; @include silent-relative(); }
    #{$prefix}#{$two}-#{$seventh}        { left:28.5714%; @include silent-relative(); }
    #{$prefix}#{$three}-#{$seventh}      { left:42.8571%; @include silent-relative(); }
    #{$prefix}#{$four}-#{$seventh}       { left:57.1428%; @include silent-relative(); }
    #{$prefix}#{$five}-#{$seventh}       { left:71.4285%; @include silent-relative(); }
    #{$prefix}#{$six}-#{$seventh}        { left:85.7142%; @include silent-relative(); }


    /**
     * Eighths
     */
    #{$prefix}#{$one}-#{$eighth}          { left:12.5%; @include silent-relative; }
    #{$prefix}#{$two}-#{$eighth}         { @extend #{$prefix}#{$one}-#{$quarter}; }
    #{$prefix}#{$three}-#{$eighth}       { left:37.5%; @include silent-relative; }
    #{$prefix}#{$four}-#{$eighth}        { @extend #{$prefix}#{$one}-#{$half}; }
    #{$prefix}#{$five}-#{$eighth}        { left:62.5%; @include silent-relative; }
    #{$prefix}#{$six}-#{$eighth}         { @extend #{$prefix}#{$three}-#{$quarter}; }
    #{$prefix}#{$seven}-#{$eighth}       { left:87.5%; @include silent-relative; }


    /**
     * Ninths
     */
    #{$prefix}#{$one}-#{$ninth}           { left:11.111%; @include silent-relative(); }
    #{$prefix}#{$two}-#{$ninth}          { left:22.222%; @include silent-relative(); }
    #{$prefix}#{$three}-#{$ninth}        { @extend #{$prefix}#{$one}-#{$third}; }
    #{$prefix}#{$four}-#{$ninth}         { left:44.444%; @include silent-relative(); }
    #{$prefix}#{$five}-#{$ninth}         { left:55.555%; @include silent-relative(); }
    #{$prefix}#{$six}-#{$ninth}          { @extend #{$prefix}#{$two}-#{$third}; }
    #{$prefix}#{$seven}-#{$ninth}        { left:77.777%; @include silent-relative(); }
    #{$prefix}#{$eight}-#{$ninth}        { left:88.888%; @include silent-relative(); }


    /**
     * Tenths
     */
    #{$prefix}#{$one}-#{$tenth}           { left:10%; @include silent-relative; }
    #{$prefix}#{$two}-#{$tenth}          { @extend #{$prefix}#{$one}-#{$fifth}; }
    #{$prefix}#{$three}-#{$tenth}        { left:30%; @include silent-relative; }
    #{$prefix}#{$four}-#{$tenth}         { @extend #{$prefix}#{$two}-#{$fifth}; }
    #{$prefix}#{$five}-#{$tenth}         { @extend #{$prefix}#{$one}-#{$half}; }
    #{$prefix}#{$six}-#{$tenth}          { @extend #{$prefix}#{$three}-#{$fifth}; }
    #{$prefix}#{$seven}-#{$tenth}        { left:70%; @include silent-relative; }
    #{$prefix}#{$eight}-#{$tenth}        { @extend #{$prefix}#{$four}-#{$fifth}; }
    #{$prefix}#{$nine}-#{$tenth}         { left:90%; @include silent-relative; }


    /**
     * Elevenths
     */
    #{$prefix}#{$one}-#{$eleventh}        { left:9.0909%; @include silent-relative(); }
    #{$prefix}#{$two}-#{$eleventh}       { left:18.1818%; @include silent-relative(); }
    #{$prefix}#{$three}-#{$eleventh}     { left:27.2727%; @include silent-relative(); }
    #{$prefix}#{$four}-#{$eleventh}      { left:36.3636%; @include silent-relative(); }
    #{$prefix}#{$five}-#{$eleventh}      { left:45.4545%; @include silent-relative(); }
    #{$prefix}#{$six}-#{$eleventh}       { left:54.5454%; @include silent-relative(); }
    #{$prefix}#{$seven}-#{$eleventh}     { left:63.6363%; @include silent-relative(); }
    #{$prefix}#{$eight}-#{$eleventh}     { left:72.7272%; @include silent-relative(); }
    #{$prefix}#{$nine}-#{$eleventh}      { left:81.8181%; @include silent-relative(); }
    #{$prefix}#{$ten}-#{$eleventh}       { left:90.9090%; @include silent-relative(); }


    /**
     * Twelfths
     */
    #{$prefix}#{$one}-#{$twelfth}         { left:8.333%; @include silent-relative; }
    #{$prefix}#{$two}-#{$twelfth}        { @extend #{$prefix}#{$one}-#{$sixth}; }
    #{$prefix}#{$three}-#{$twelfth}      { @extend #{$prefix}#{$one}-#{$quarter}; }
    #{$prefix}#{$four}-#{$twelfth}       { @extend #{$prefix}#{$one}-#{$third}; }
    #{$prefix}#{$five}-#{$twelfth}       { left:41.666%; @include silent-relative; }
    #{$prefix}#{$six}-#{$twelfth}        { @extend #{$prefix}#{$one}-#{$half}; }
    #{$prefix}#{$seven}-#{$twelfth}      { left:58.333%; @include silent-relative; }
    #{$prefix}#{$eight}-#{$twelfth}      { @extend #{$prefix}#{$two}-#{$third}; }
    #{$prefix}#{$nine}-#{$twelfth}       { @extend #{$prefix}#{$three}-#{$quarter}; }
    #{$prefix}#{$ten}-#{$twelfth}        { @extend #{$prefix}#{$five}-#{$sixth}; }
    #{$prefix}#{$eleven}-#{$twelfth}     { left:91.666%; @include silent-relative; }
}

@if $push {

    /**
     * Not a particularly great selector, but the DRYest way to do things.
     */
    [class*="push--"] { position:relative; }

    @include push-setup;

    @if $responsive {
        @each $name in $breakpoint-has-push {
            @include grid-media-query($name) {
                @include push-setup('#{$name}--');
            }
        }
    }

}





/*------------------------------------*\
    $PULL
\*------------------------------------*/
/**
 * Pull classes, to move grid items back to the left by certain amounts.
 */
@mixin pull-setup($namespace: "") {
    $prefix: $class-type + "pull--" + $namespace;

    /**
     * Whole
     */
    #{$prefix}#{$one}-#{$whole}           { right:100%; @include silent-relative; }


    /**
     * Halves
     */
    #{$prefix}#{$one}-#{$half}            { right:50%; @include silent-relative; }


    /**
     * Thirds
     */
    #{$prefix}#{$one}-#{$third}           { right:33.333%; @include silent-relative; }
    #{$prefix}#{$two}-#{$third}          { right:66.666%; @include silent-relative; }


    /**
     * Quarters
     */
    #{$prefix}#{$one}-#{$quarter}         { right:25%; @include silent-relative; }
    #{$prefix}#{$two}-#{$quarter}        { @extend #{$prefix}#{$one}-#{$half}; }
    #{$prefix}#{$three}-#{$quarter}      { right:75%; @include silent-relative; }


    /**
     * Fifths
     */
    #{$prefix}#{$one}-#{$fifth}           { right:20%; @include silent-relative; }
    #{$prefix}#{$two}-#{$fifth}          { right:40%; @include silent-relative; }
    #{$prefix}#{$three}-#{$fifth}        { right:60%; @include silent-relative; }
    #{$prefix}#{$four}-#{$fifth}         { right:80%; @include silent-relative; }


    /**
     * Sixths
     */
    #{$prefix}#{$one}-#{$sixth}           { right:16.666%; @include silent-relative; }
    #{$prefix}#{$two}-#{$sixth}          { @extend #{$prefix}#{$one}-#{$third}; }
    #{$prefix}#{$three}-#{$sixth}        { @extend #{$prefix}#{$one}-#{$half}; }
    #{$prefix}#{$four}-#{$sixth}         { @extend #{$prefix}#{$two}-#{$third}; }
    #{$prefix}#{$five}-#{$sixth}         { right:83.333%; @include silent-relative; }


    /**
     * Sevenths
     */
    #{$prefix}#{$one}-#{$seventh}         { left:14.2857%; @include silent-relative(); }
    #{$prefix}#{$two}-#{$seventh}        { left:28.5714%; @include silent-relative(); }
    #{$prefix}#{$three}-#{$seventh}      { left:42.8571%; @include silent-relative(); }
    #{$prefix}#{$four}-#{$seventh}       { left:57.1428%; @include silent-relative(); }
    #{$prefix}#{$five}-#{$seventh}       { left:71.4285%; @include silent-relative(); }
    #{$prefix}#{$six}-#{$seventh}        { left:85.7142%; @include silent-relative(); }


    /**
     * Eighths
     */
    #{$prefix}#{$one}-#{$eighth}          { right:12.5%; @include silent-relative; }
    #{$prefix}#{$two}-#{$eighth}         { @extend #{$prefix}#{$one}-#{$quarter}; }
    #{$prefix}#{$three}-#{$eighth}       { right:37.5%; @include silent-relative; }
    #{$prefix}#{$four}-#{$eighth}        { @extend #{$prefix}#{$one}-#{$half}; }
    #{$prefix}#{$five}-#{$eighth}        { right:62.5%; @include silent-relative; }
    #{$prefix}#{$six}-#{$eighth}         { @extend #{$prefix}#{$three}-#{$quarter}; }
    #{$prefix}#{$seven}-#{$eighth}       { right:87.5%; @include silent-relative; }


    /**
     * Ninths
     */
    #{$prefix}#{$one}-#{$ninth}           { right:11.111%; @include silent-relative(); }
    #{$prefix}#{$two}-#{$ninth}          { right:22.222%; @include silent-relative(); }
    #{$prefix}#{$three}-#{$ninth}        { @extend #{$prefix}#{$one}-#{$third}; }
    #{$prefix}#{$four}-#{$ninth}         { right:44.444%; @include silent-relative(); }
    #{$prefix}#{$five}-#{$ninth}         { right:55.555%; @include silent-relative(); }
    #{$prefix}#{$six}-#{$ninth}          { @extend #{$prefix}#{$two}-#{$third}; }
    #{$prefix}#{$seven}-#{$ninth}        { right:77.777%; @include silent-relative(); }
    #{$prefix}#{$eight}-#{$ninth}        { right:88.888%; @include silent-relative(); }


    /**
     * Tenths
     */
    #{$prefix}#{$one}-#{$tenth}           { right:10%; @include silent-relative; }
    #{$prefix}#{$two}-#{$tenth}          { @extend #{$prefix}#{$one}-#{$fifth}; }
    #{$prefix}#{$three}-#{$tenth}        { right:30%; @include silent-relative; }
    #{$prefix}#{$four}-#{$tenth}         { @extend #{$prefix}#{$two}-#{$fifth}; }
    #{$prefix}#{$five}-#{$tenth}         { @extend #{$prefix}#{$one}-#{$half}; }
    #{$prefix}#{$six}-#{$tenth}          { @extend #{$prefix}#{$three}-#{$fifth}; }
    #{$prefix}#{$seven}-#{$tenth}        { right:70%; @include silent-relative; }
    #{$prefix}#{$eight}-#{$tenth}        { @extend #{$prefix}#{$four}-#{$fifth}; }
    #{$prefix}#{$nine}-#{$tenth}         { right:90%; @include silent-relative; }


    /**
     * Elevenths
     */
    #{$prefix}#{$one}-#{$eleventh}        { right:9.0909%; @include silent-relative(); }
    #{$prefix}#{$two}-#{$eleventh}       { right:18.1818%; @include silent-relative(); }
    #{$prefix}#{$three}-#{$eleventh}     { right:27.2727%; @include silent-relative(); }
    #{$prefix}#{$four}-#{$eleventh}      { right:36.3636%; @include silent-relative(); }
    #{$prefix}#{$five}-#{$eleventh}      { right:45.4545%; @include silent-relative(); }
    #{$prefix}#{$six}-#{$eleventh}       { right:54.5454%; @include silent-relative(); }
    #{$prefix}#{$seven}-#{$eleventh}     { right:63.6363%; @include silent-relative(); }
    #{$prefix}#{$eight}-#{$eleventh}     { right:72.7272%; @include silent-relative(); }
    #{$prefix}#{$nine}-#{$eleventh}      { right:81.8181%; @include silent-relative(); }
    #{$prefix}#{$ten}-#{$eleventh}       { right:90.9090%; @include silent-relative(); }


    /**
     * Twelfths
     */
    #{$prefix}#{$one}-#{$twelfth}         { right:8.333%; @include silent-relative; }
    #{$prefix}#{$two}-#{$twelfth}        { @extend #{$prefix}#{$one}-#{$sixth}; }
    #{$prefix}#{$three}-#{$twelfth}      { @extend #{$prefix}#{$one}-#{$quarter}; }
    #{$prefix}#{$four}-#{$twelfth}       { @extend #{$prefix}#{$one}-#{$third}; }
    #{$prefix}#{$five}-#{$twelfth}       { right:41.666%; @include silent-relative; }
    #{$prefix}#{$six}-#{$twelfth}        { @extend #{$prefix}#{$one}-#{$half}; }
    #{$prefix}#{$seven}-#{$twelfth}      { right:58.333%; @include silent-relative; }
    #{$prefix}#{$eight}-#{$twelfth}      { @extend #{$prefix}#{$two}-#{$third}; }
    #{$prefix}#{$nine}-#{$twelfth}       { @extend #{$prefix}#{$three}-#{$quarter}; }
    #{$prefix}#{$ten}-#{$twelfth}        { @extend #{$prefix}#{$five}-#{$sixth}; }
    #{$prefix}#{$eleven}-#{$twelfth}     { right:91.666%; @include silent-relative; }
}

@if $pull {

    /**
     * Not a particularly great selector, but the DRYest way to do things.
     */
    [class*="pull--"] { position:relative; }

    @include pull-setup;

    @if $responsive {
        @each $name in $breakpoint-has-pull {
            @include grid-media-query($name) {
                @include pull-setup('#{$name}--');
            }
        }
    }

}

/* Global grid reset - setting font to 0 lets grid blocks bump next to each other */
.grid{
    font-size: 0;

    &__item{
        @include font-rem($base-font-size);
    }
}