<?php

class Splendor_Advanced_Custom_Fields{

	function __construct(){
        add_action('acf/init',array($this,'hp_featured_projects'));
        add_action('acf/init',array($this,'hp_about_block'));
        add_action('acf/init',array($this,'commissions_faq'));
        add_action('acf/init',array($this,'global_options'));
        add_action('acf/init',array($this,'project_categories'));
	}

	function hp_featured_projects(){

		acf_add_local_field_group(array (
			'key' => 'hp_featured_projects',
			'title' => 'Featured Projects',
			'fields' => array (
				array (
					'key' => 'hp_projects',
					'label' => 'Relationship',
					'name' => 'projects',
					'type' => 'relationship',
					'instructions' => '',
					'required' => 0,
					'conditional_logic' => 0,
					'wrapper' => array (
						'width' => '',
						'class' => '',
						'id' => '',
					),
					'default_value' => '',

					/* (mixed) Specify an array of post types to filter the available choices. Defaults to '' */
					'post_type' => 'project',

					/* (mixed) Specify an array of taxonomies to filter the available choices. Defaults to '' */
					'taxonomy' => '',

					/* (array) Specify the available filters used to search for posts. Choices of 'search' (Search input), 'post_type' (Post type select) and 'taxonomy' (Taxonomy select) */
					'filters' => array('search'),

					/* (array) Specify the visual elements for each post. Choices of 'featured_image' (Featured image icon) */
					'elements' => array('featured_image'),

					/* (int) Specify the minimum posts required to be selected. Defaults to 0 */
					'min' => 0,

					/* (int) Specify the maximum posts allowed to be selected. Defaults to 0 */
					'max' => 0,

					/* (string) Specify the type of value returned by get_field(). Defaults to 'object'. Choices of 'object' (Post object) or 'id' (Post ID) */
					'return_format' => 'object',
				),

			),
			'location' => array (
				array (
					array (
						'param' => 'page_type',
						'operator' => '==',
						'value' => 'front_page',
					),
				),
			),
			'menu_order' => 10,
			'position' => 'normal',
			'style' => 'default',
			'label_placement' => 'top',
			'instruction_placement' => 'label',
			'hide_on_screen' => '',
			'active' => 1,
			'description' => '',
		));
	}

	function hp_about_block(){

		acf_add_local_field_group(array (
			'key' => 'hp_about_block',
			'title' => 'About Block',
			'fields' => array (
				array (
					'key' => 'hp_about_content',
					'label' => 'Textarea',
					'name' => 'about_textarea',
					'type' => 'textarea',
					'instructions' => '',
					'required' => 1,
					'conditional_logic' => 0,
					'wrapper' => array (
						'width' => '',
						'class' => '',
						'id' => '',
					),
					'default_value' => '',

					/* (string) Appears within the input. Defaults to '' */
					'placeholder' => '',

					/* (string) Restricts the character limit. Defaults to '' */
					'maxlength' => '',

					/* (int) Restricts the number of rows and height. Defaults to '' */
					'rows' => '',

					/* (new_lines) Decides how to render new lines. Detauls to 'wpautop'. Choices of 'wpautop' (Automatically add paragraphs), 'br' (Automatically add <br>) or '' (No Formatting) */
					'new_lines' => '',

					/* (bool) Makes the input readonly. Defaults to 0 */
					'readonly' => 0,

					/* (bool) Makes the input disabled. Defaults to 0 */
					'disabled' => 0,
				),

				/**
				 * URL field
				 */
				array (
					'key' => 'hp_about_button_url',
					'label' => 'Button URL',
					'name' => 'hp_about_button_url',
					'type' => 'url',
					'instructions' => '',
					'required' => 1,
					'conditional_logic' => 0,
					'wrapper' => array (
						'width' => '',
						'class' => '',
						'id' => '',
					),
					'default_value' => '',

					/* (string) Appears within the input. Defaults to '' */
					'placeholder' => '',
				),

				/**
				 * Text field
				 */
				array (
					/**
					 * Common field settings
					 */

					/* (string) Unique identifier for the field. Must begin with 'field_' */
					'key' => 'hp_about_button_text',

					/* (string) Visible when editing the field value */
					'label' => 'Button Text',

					/* (string) Used to save and load data. Single word, no spaces. Underscores and dashes allowed */
					'name' => 'hp_about_button_text',

					/* (string) Type of field (text, textarea, image, etc) */
					'type' => 'text',

					/* (string) Instructions for authors. Shown when submitting data */
					'instructions' => '',

					/* (int) Whether or not the field value is required. Defaults to 0 */
					'required' => 1,

					/* (mixed) Conditionally hide or show this field based on other field's values. Best to use the ACF UI and export to understand the array structure. Defaults to 0 */
					'conditional_logic' => 0,
					/* Sample conditional Logic
					'conditional_logic' => array(
						array(
							array(
								'field'=>'field_key_here',
								'operator'=>'==',
								'value'=>'some_value',
							),

							// "And" condition
							array(
								'field'=>'some_other_key_here',
								'operator'=>'==',
								'value'=>'some_value',
							),
						),
						// "Or" condition
						array(
							array(
								'field'=>'field_key_here',
								'operator'=>'==',
								'value'=>'some_other_value',
							),
						),
					),
					 */

					/* (array) An array of attributes given to the field element */
					'wrapper' => array (
						'width' => '',
						'class' => '',
						'id' => '',
					),

					/* (mixed) A default value used by ACF if no value has yet been saved */
					'default_value' => '',

					/**
					 * Text Field specific settings
					 */

					/* (string) Appears within the input. Defaults to '' */
					'placeholder' => '',

					/* (string) Appears before the input. Defaults to '' */
					'prepend' => '',

					/* (string) Appears after the input. Defaults to '' */
					'append' => '',

					/* (string) Restricts the character limit. Defaults to '' */
					'maxlength' => '',

					/* (bool) Makes the input readonly. Defaults to 0 */
					'readonly' => 0,

					/* (bool) Makes the input disabled. Defaults to 0 */
					'disabled' => 0,
				),

			),
			'location' => array (
				array (
					array (
						'param' => 'page_type',
						'operator' => '==',
						'value' => 'front_page',
					),
				),
			),
			'menu_order' => 10,
			'position' => 'normal',
			'style' => 'default',
			'label_placement' => 'top',
			'instruction_placement' => 'label',
			'hide_on_screen' => '',
			'active' => 1,
			'description' => '',
		));
	}

	function commissions_faq(){

		$stem = strtolower(get_class($this));
		acf_add_local_field_group(array (
			'key' => $stem,
			'title' => 'FAQs',
			'fields' => array (
				/**
				 * Repeater field
				 */
				array (
					'key' => $stem.'_QAs',
					'label' => 'Q&As',
					'name' => 'QAs',
					'type' => 'repeater',
					'instructions' => '',
					'required' => 1,
					'conditional_logic' => 0,
					'wrapper' => array (
						'width' => '',
						'class' => '',
						'id' => '',
					),
					'default_value' => '',

					/* (array) Fields to nest within repeater, uses same settings as a top level field */
					'sub_fields' => array (

						/**
						 * Text field
						 *
						 * If copy/pasting, there is a simpler text configuration below this one
						 */
						array (
							/**
							 * Common field settings
							 */

							/* (string) Unique identifier for the field. Must begin with 'field_' */
							'key' => $stem.'_question',

							/* (string) Visible when editing the field value */
							'label' => 'Question',

							/* (string) Used to save and load data. Single word, no spaces. Underscores and dashes allowed */
							'name' => 'question',

							/* (string) Type of field (text, textarea, image, etc) */
							'type' => 'text',

							/* (string) Instructions for authors. Shown when submitting data */
							'instructions' => '',

							/* (int) Whether or not the field value is required. Defaults to 0 */
							'required' => 0,

							/* (mixed) Conditionally hide or show this field based on other field's values. Best to use the ACF UI and export to understand the array structure. Defaults to 0 */
							'conditional_logic' => 0,
							/* Sample conditional Logic
							'conditional_logic' => array(
								array(
									array(
										'field'=>'field_key_here',
										'operator'=>'==',
										'value'=>'some_value',
									),

									// "And" condition
									array(
										'field'=>'some_other_key_here',
										'operator'=>'==',
										'value'=>'some_value',
									),
								),
								// "Or" condition
								array(
									array(
										'field'=>'field_key_here',
										'operator'=>'==',
										'value'=>'some_other_value',
									),
								),
							),
							 */

							/* (array) An array of attributes given to the field element */
							'wrapper' => array (
								'width' => '',
								'class' => '',
								'id' => '',
							),

							/* (mixed) A default value used by ACF if no value has yet been saved */
							'default_value' => '',

							/**
							 * Text Field specific settings
							 */

							/* (string) Appears within the input. Defaults to '' */
							'placeholder' => '',

							/* (string) Appears before the input. Defaults to '' */
							'prepend' => '',

							/* (string) Appears after the input. Defaults to '' */
							'append' => '',

							/* (string) Restricts the character limit. Defaults to '' */
							'maxlength' => '',

							/* (bool) Makes the input readonly. Defaults to 0 */
							'readonly' => 0,

							/* (bool) Makes the input disabled. Defaults to 0 */
							'disabled' => 0,
						),

						/**
						 * Textarea field
						 */
						array (
							'key' => $stem.'_answer',
							'label' => 'Answer',
							'name' => 'answer',
							'type' => 'textarea',
							'instructions' => '',
							'required' => 1,
							'conditional_logic' => 0,
							'wrapper' => array (
								'width' => '',
								'class' => '',
								'id' => '',
							),
							'default_value' => '',

							/* (string) Appears within the input. Defaults to '' */
							'placeholder' => '',

							/* (string) Restricts the character limit. Defaults to '' */
							'maxlength' => '',

							/* (int) Restricts the number of rows and height. Defaults to '' */
							'rows' => '',

							/* (new_lines) Decides how to render new lines. Detauls to 'wpautop'. Choices of 'wpautop' (Automatically add paragraphs), 'br' (Automatically add <br>) or '' (No Formatting) */
							'new_lines' => '',

							/* (bool) Makes the input readonly. Defaults to 0 */
							'readonly' => 0,

							/* (bool) Makes the input disabled. Defaults to 0 */
							'disabled' => 0,
						),
					),

					/* (int) minimum number of blocks */
					'min' => 1,

					/* (int) maximum number of blocks */
					'max' => 0,

					/* (layout) Each group of inputs will be displayed in this way. table - Table, block - Block, row - Row */
					'layout' => 'row',

					/* (string) Test displayed on button to add new group of inputs.  Defaults to Add Row */
					'button_label' => '',

					/* (selection) Select a sub field to show when row is collapsed would be the key of a sub field */
					'collapsed' => '',
				),

			),
			'location' => array (
				array (
					array (
						'param' => 'page_template',
						'operator' => '==',
						'value' => 'templates/commissions.php',
					),
				),
			),
			'menu_order' => 10,
			'position' => 'normal',
			'style' => 'default',
			'label_placement' => 'top',
			'instruction_placement' => 'label',
			'hide_on_screen' => '',
			'active' => 1,
			'description' => '',
		));
	}

	function global_options(){

		acf_add_local_field_group(array (
			'key' => 'commissions_block',
			'title' => 'Commissions Block',
			'fields' => array (
				array (
					'key' => 'commissions_content',
					'label' => 'Textarea',
					'name' => 'commissions_textarea',
					'type' => 'textarea',
					'instructions' => '',
					'required' => 1,
					'conditional_logic' => 0,
					'wrapper' => array (
						'width' => '',
						'class' => '',
						'id' => '',
					),
					'default_value' => '',

					/* (string) Appears within the input. Defaults to '' */
					'placeholder' => '',

					/* (string) Restricts the character limit. Defaults to '' */
					'maxlength' => '',

					/* (int) Restricts the number of rows and height. Defaults to '' */
					'rows' => '',

					/* (new_lines) Decides how to render new lines. Detauls to 'wpautop'. Choices of 'wpautop' (Automatically add paragraphs), 'br' (Automatically add <br>) or '' (No Formatting) */
					'new_lines' => '',

					/* (bool) Makes the input readonly. Defaults to 0 */
					'readonly' => 0,

					/* (bool) Makes the input disabled. Defaults to 0 */
					'disabled' => 0,
				),

				/**
				 * URL field
				 */
				array (
					'key' => 'commissions_button_url',
					'label' => 'Button URL',
					'name' => 'commissions_button_url',
					'type' => 'url',
					'instructions' => '',
					'required' => 1,
					'conditional_logic' => 0,
					'wrapper' => array (
						'width' => '',
						'class' => '',
						'id' => '',
					),
					'default_value' => '',

					/* (string) Appears within the input. Defaults to '' */
					'placeholder' => '',
				),

				/**
				 * Text field
				 */
				array (
					/**
					 * Common field settings
					 */

					/* (string) Unique identifier for the field. Must begin with 'field_' */
					'key' => 'commissions_button_text',

					/* (string) Visible when editing the field value */
					'label' => 'Button Text',

					/* (string) Used to save and load data. Single word, no spaces. Underscores and dashes allowed */
					'name' => 'commissions_button_text',

					/* (string) Type of field (text, textarea, image, etc) */
					'type' => 'text',

					/* (string) Instructions for authors. Shown when submitting data */
					'instructions' => '',

					/* (int) Whether or not the field value is required. Defaults to 0 */
					'required' => 1,

					/* (mixed) Conditionally hide or show this field based on other field's values. Best to use the ACF UI and export to understand the array structure. Defaults to 0 */
					'conditional_logic' => 0,
					/* Sample conditional Logic
					'conditional_logic' => array(
						array(
							array(
								'field'=>'field_key_here',
								'operator'=>'==',
								'value'=>'some_value',
							),

							// "And" condition
							array(
								'field'=>'some_other_key_here',
								'operator'=>'==',
								'value'=>'some_value',
							),
						),
						// "Or" condition
						array(
							array(
								'field'=>'field_key_here',
								'operator'=>'==',
								'value'=>'some_other_value',
							),
						),
					),
					 */

					/* (array) An array of attributes given to the field element */
					'wrapper' => array (
						'width' => '',
						'class' => '',
						'id' => '',
					),

					/* (mixed) A default value used by ACF if no value has yet been saved */
					'default_value' => '',

					/**
					 * Text Field specific settings
					 */

					/* (string) Appears within the input. Defaults to '' */
					'placeholder' => '',

					/* (string) Appears before the input. Defaults to '' */
					'prepend' => '',

					/* (string) Appears after the input. Defaults to '' */
					'append' => '',

					/* (string) Restricts the character limit. Defaults to '' */
					'maxlength' => '',

					/* (bool) Makes the input readonly. Defaults to 0 */
					'readonly' => 0,

					/* (bool) Makes the input disabled. Defaults to 0 */
					'disabled' => 0,
				),

			),
			'location' => array (
				array (
					array (
						'param' => 'options_page',
						'operator' => '==',
						'value' => 'acf-options-global',
					),
				),
			),
			'menu_order' => 0,
			'position' => 'normal',
			'style' => 'default',
			'label_placement' => 'top',
			'instruction_placement' => 'label',
			'hide_on_screen' => '',
			'active' => 1,
			'description' => '',
		));

		acf_add_local_field_group(array (
			'key' => 'about_block',
			'title' => 'About Block',
			'fields' => array (
				array (
					'key' => 'about_content',
					'label' => 'Textarea',
					'name' => 'about_textarea',
					'type' => 'textarea',
					'instructions' => '',
					'required' => 1,
					'conditional_logic' => 0,
					'wrapper' => array (
						'width' => '',
						'class' => '',
						'id' => '',
					),
					'default_value' => '',

					/* (string) Appears within the input. Defaults to '' */
					'placeholder' => '',

					/* (string) Restricts the character limit. Defaults to '' */
					'maxlength' => '',

					/* (int) Restricts the number of rows and height. Defaults to '' */
					'rows' => '',

					/* (new_lines) Decides how to render new lines. Detauls to 'wpautop'. Choices of 'wpautop' (Automatically add paragraphs), 'br' (Automatically add <br>) or '' (No Formatting) */
					'new_lines' => '',

					/* (bool) Makes the input readonly. Defaults to 0 */
					'readonly' => 0,

					/* (bool) Makes the input disabled. Defaults to 0 */
					'disabled' => 0,
				),

				/**
				 * URL field
				 */
				array (
					'key' => 'about_button_url',
					'label' => 'Button URL',
					'name' => 'about_button_url',
					'type' => 'url',
					'instructions' => '',
					'required' => 1,
					'conditional_logic' => 0,
					'wrapper' => array (
						'width' => '',
						'class' => '',
						'id' => '',
					),
					'default_value' => '',

					/* (string) Appears within the input. Defaults to '' */
					'placeholder' => '',
				),

				/**
				 * Text field
				 */
				array (
					/**
					 * Common field settings
					 */

					/* (string) Unique identifier for the field. Must begin with 'field_' */
					'key' => 'about_button_text',

					/* (string) Visible when editing the field value */
					'label' => 'Button Text',

					/* (string) Used to save and load data. Single word, no spaces. Underscores and dashes allowed */
					'name' => 'about_button_text',

					/* (string) Type of field (text, textarea, image, etc) */
					'type' => 'text',

					/* (string) Instructions for authors. Shown when submitting data */
					'instructions' => '',

					/* (int) Whether or not the field value is required. Defaults to 0 */
					'required' => 1,

					/* (mixed) Conditionally hide or show this field based on other field's values. Best to use the ACF UI and export to understand the array structure. Defaults to 0 */
					'conditional_logic' => 0,
					/* Sample conditional Logic
					'conditional_logic' => array(
						array(
							array(
								'field'=>'field_key_here',
								'operator'=>'==',
								'value'=>'some_value',
							),

							// "And" condition
							array(
								'field'=>'some_other_key_here',
								'operator'=>'==',
								'value'=>'some_value',
							),
						),
						// "Or" condition
						array(
							array(
								'field'=>'field_key_here',
								'operator'=>'==',
								'value'=>'some_other_value',
							),
						),
					),
					 */

					/* (array) An array of attributes given to the field element */
					'wrapper' => array (
						'width' => '',
						'class' => '',
						'id' => '',
					),

					/* (mixed) A default value used by ACF if no value has yet been saved */
					'default_value' => '',

					/**
					 * Text Field specific settings
					 */

					/* (string) Appears within the input. Defaults to '' */
					'placeholder' => '',

					/* (string) Appears before the input. Defaults to '' */
					'prepend' => '',

					/* (string) Appears after the input. Defaults to '' */
					'append' => '',

					/* (string) Restricts the character limit. Defaults to '' */
					'maxlength' => '',

					/* (bool) Makes the input readonly. Defaults to 0 */
					'readonly' => 0,

					/* (bool) Makes the input disabled. Defaults to 0 */
					'disabled' => 0,
				),

			),
			'location' => array (
				array (
					array (
						'param' => 'options_page',
						'operator' => '==',
						'value' => 'acf-options-global',
					),
				),
			),
			'menu_order' => 0,
			'position' => 'normal',
			'style' => 'default',
			'label_placement' => 'top',
			'instruction_placement' => 'label',
			'hide_on_screen' => '',
			'active' => 1,
			'description' => '',
		));
	}

	function project_categories(){
		$stem = 'project_category';
		acf_add_local_field_group(array (
			'key' => $stem,
			'title' => 'Project Category',
			'fields' => array (
				array (
					'key' => $stem.'_image',
					'label' => 'Category Thumbnail',
					'name' => $stem.'_image',
					'type' => 'image',
					'instructions' => '',
					'required' => 0,
					'conditional_logic' => 0,
					'wrapper' => array (
						'width' => '',
						'class' => '',
						'id' => '',
					),
					/* (string) Specify the type of value returned by get_field(). Defaults to 'array'.
                    Choices of 'array' (Image Array), 'url' (Image URL) or 'id' (Image ID) */
                    'return_format' => 'url',

                    /* (string) Specify the image size shown when editing. Defaults to 'thumbnail'. */
                    'preview_size' => 'thumbnail',

                    /* (string) Restrict the image library. Defaults to 'all'.
                    Choices of 'all' (All Images) or 'uploadedTo' (Uploaded to post) */
                    'library' => 'all',

                    /* (int) Specify the minimum width in px required when uploading. Defaults to 0 */
                    'min_width' => 0,

                    /* (int) Specify the minimum height in px required when uploading. Defaults to 0 */
                    'min_height' => 0,

                    /* (int) Specify the minimum filesize in MB required when uploading. Defaults to 0 
                    The unit may also be included. eg. '256KB' */
                    'min_size' => 0,

                    /* (int) Specify the maximum width in px allowed when uploading. Defaults to 0 */
                    'max_width' => 0,

                    /* (int) Specify the maximum height in px allowed when uploading. Defaults to 0 */
                    'max_height' => 0,

                    /* (int) Specify the maximum filesize in MB in px allowed when uploading. Defaults to 0
                    The unit may also be included. eg. '256KB' */
                    'max_size' => 0,

                    /* (string) Comma separated list of file type extensions allowed when uploading. Defaults to '' */
                    'mime_types' => '',
				),
				array (
					'key' => $stem.'_hero',
					'label' => 'Category Hero',
					'name' => $stem.'_hero',
					'type' => 'image',
					'instructions' => '',
					'required' => 0,
					'conditional_logic' => 0,
					'wrapper' => array (
						'width' => '',
						'class' => '',
						'id' => '',
					),
					/* (string) Specify the type of value returned by get_field(). Defaults to 'array'.
                    Choices of 'array' (Image Array), 'url' (Image URL) or 'id' (Image ID) */
                    'return_format' => 'url',

                    /* (string) Specify the image size shown when editing. Defaults to 'thumbnail'. */
                    'preview_size' => 'thumbnail',

                    /* (string) Restrict the image library. Defaults to 'all'.
                    Choices of 'all' (All Images) or 'uploadedTo' (Uploaded to post) */
                    'library' => 'all',

                    /* (int) Specify the minimum width in px required when uploading. Defaults to 0 */
                    'min_width' => 0,

                    /* (int) Specify the minimum height in px required when uploading. Defaults to 0 */
                    'min_height' => 0,

                    /* (int) Specify the minimum filesize in MB required when uploading. Defaults to 0 
                    The unit may also be included. eg. '256KB' */
                    'min_size' => 0,

                    /* (int) Specify the maximum width in px allowed when uploading. Defaults to 0 */
                    'max_width' => 0,

                    /* (int) Specify the maximum height in px allowed when uploading. Defaults to 0 */
                    'max_height' => 0,

                    /* (int) Specify the maximum filesize in MB in px allowed when uploading. Defaults to 0
                    The unit may also be included. eg. '256KB' */
                    'max_size' => 0,

                    /* (string) Comma separated list of file type extensions allowed when uploading. Defaults to '' */
                    'mime_types' => '',
				),
			),
			'location' => array (
				array (
					array (
						'param' => 'taxonomy',
						'operator' => '==',
						'value' => 'project_category',
					),
				),
			),
			'menu_order' => 10,
			'position' => 'normal',
			'style' => 'default',
			'label_placement' => 'top',
			'instruction_placement' => 'label',
			'hide_on_screen' => '',
			'active' => 1,
			'description' => '',
		));
	}
}

new Splendor_Advanced_Custom_Fields();