<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package cleanslate
 */

?>

    <?php 
        if(is_page_template( 'templates/commissions.php' )):
            // to change image for about block, add an image variable to this list and use below
            $title = 'About';
            $content = get_field('about_content','options');
            $btn_url = get_field('about_button_url','options');
            $btn_text = get_field('about_button_text','options');
        else:
            $title = 'Commissions';
            $content = get_field('commissions_content','options');
            $btn_url = get_field('commissions_button_url','options');
            $btn_text = get_field('commissions_button_text','options');
        endif; 
    ?>

        <div class="footer-block text-center" style="background-image:url(<?php echo get_stylesheet_directory_uri(); ?>/assets/images/global/fabric_rolls.jpg);">
            <div class="footer-block__content wrapper wrapper--sixty">
                <h2><?php echo $title; ?></h2>
                <p><?php echo $content; ?></p>
                <a class="btn btn--white" href="<?php echo $btn_url; ?>"><?php echo $btn_text; ?></a>
            </div>
        </div>

	</div><!-- #content -->

	<footer id="colophon" class="site-footer" role="contentinfo">
        <div class="grid grid--middle">
            <div class="grid__item m--col-3-7 col-1-1 text-center">
                <div class="site-footer__info">
                    &copy; <?php echo date('Y'); ?> Splendor &amp; Story Costuming
                </div><!-- .site-info -->
            </div><!-- 
            --><div class="grid__item m--col-4-7 col-1-1 text-center">
        		<div class="site-footer__navigation">
        			<?php wp_nav_menu( array( 'theme_location' => 'menu-2', 'container' => '', 'container_id' => '' ) ); ?>
        		</div>
            </div>
        </div>
	</footer><!-- #colophon -->
</div><!-- #page -->

<?php wp_footer(); ?>

</body>
</html>
