<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package cleanslate
 */

?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="http://gmpg.org/xfn/11">

	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<div id="page" class="site position-rel">
	<a class="skip-link screen-reader-text" href="#content"><?php esc_html_e( 'Skip to content', 'cleanslate' ); ?></a>
	<header class="site-header">
		<div class="grid grid--middle grid--full">
			<div class="grid__item xl--col-1-2 col-10-12 text-left">
				<div class="site-id">
					<a href="<?php echo esc_url(home_url()); ?>"><img class="logo" src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/global/Logo.png"></a>
					<a href="<?php echo esc_url(home_url()); ?>" class="h3"><?php _e('Splendor &amp; Story Costuming', 'cleanslate'); ?></a>
				</div>
			</div><!--
			--><div class="grid__item xl--col-1-2 col-2-12 text-right">
			<a id="mobile-trigger" href="#mobile-menu"><span class="fa fa-bars" aria-hidden="true"></span></a>
				<nav id="site-navigation" class="main-navigation" role="navigation">
				<?php wp_nav_menu( array( 'theme_location' => 'menu-1', 'container' => '', 'container_id' => '' ) ); ?>
				</nav>
			</div>
		</div>
	</header>

	<div id="content" class="site-content">
		<div id="preloader" class="text-center">
			<div id="status" class="h1 text-center">Loading...</div>
		</div>

	<?php get_template_part('template-parts/hero'); ?>