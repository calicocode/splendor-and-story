<?php
/**
 * The template for displaying archive pages
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package cleanslate
 */

get_header(); ?>

	<div id="primary" class="content-area wrapper">
		<main id="main" class="site-main" role="main">
		<?php 
			$tax_id = get_queried_object()->term_id;
			$description = term_description( $term_id, $taxonomy );

			if($description): ?>
			<div class="category__description wrapper wrapper--seventy text-center">
			<?php
				echo $description;
			?>
			</div>
			<?php
			endif;
		?>

		<?php
		if ( have_posts() ) : ?>
			<div class="grid grid--center category__archive">
				<!--
			<?php
			/* Start the Loop */
			while ( have_posts() ) : the_post(); ?>
				
				    --><div class="grid__item l--col-1-4 col-1-2">
				        <a href="<?php echo get_permalink(); ?>"><img src="<?php echo the_post_thumbnail_url() ?>"></a>
				        <a href="<?php echo get_permalink(); ?>" class="h4"><?php echo get_the_title(); ?></a>
				    </div><!--

			<?php endwhile; ?>
			-->
				</div>

			<?php the_posts_navigation();

		else :

			get_template_part( 'template-parts/content', 'none' );

		endif; ?>

		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_footer();
