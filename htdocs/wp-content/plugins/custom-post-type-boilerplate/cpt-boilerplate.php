<?php

/**
 * The plugin bootstrap file
 *
 * This file is read by WordPress to generate the plugin information in the plugin
 * Dashboard. This file also includes all of the dependencies used by the plugin,
 * registers the activation and deactivation functions, and defines a function
 * that starts the plugin.
 *
 * @link              http://www.redclayinteractive.com
 * @since             1.0.1
 * @package           CPT_Boilerplate
 *
 * @wordpress-plugin
 * Plugin Name:       CPT_Boilerplate Custom Post Types
 * Plugin URI:        http://www.redclayinteractive.com/cpt-boilerplate-uri/
 * Description:       This is a short description of what the plugin does. It's displayed in the WordPress dashboard.
 * Version:           1.0.0
 * Author:            Red Clay Interactive
 * Author URI:        http://www.redclayinteractive.com/
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       cpt-boilerplate
 * Domain Path:       /languages
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

define('CPT_BOILERPLATE_STEM','cpt-boilerplate');
define('CPT_BOILERPLATE_VERSION','1.0.1');

/**
 * The code that runs during plugin activation.
 * This action is documented in includes/class-cpt-boilerplate-activator.php
 */
function activate_cpt_boilerplate() {
	require plugin_dir_path( __FILE__ ) . 'includes/class-cpt-boilerplate-activator.php';
	CPT_Boilerplate_Activator::activate();
}

/**
 * The code that runs during plugin deactivation.
 * This action is documented in includes/class-cpt-boilerplate-deactivator.php
 */
function deactivate_cpt_boilerplate() {
	require plugin_dir_path( __FILE__ ) . 'includes/class-cpt-boilerplate-deactivator.php';
	CPT_Boilerplate_Deactivator::deactivate();
}

register_activation_hook( __FILE__, 'activate_cpt_boilerplate' );
register_deactivation_hook( __FILE__, 'deactivate_cpt_boilerplate' );

/**
 * The core plugin class that is used to define internationalization,
 * dashboard-specific hooks, and public-facing site hooks.
 */
require plugin_dir_path( __FILE__ ) . 'includes/class-cpt-boilerplate.php';

/**
 * Begins execution of the plugin.
 *
 * Since everything within the plugin is registered via hooks,
 * then kicking off the plugin from this point in the file does
 * not affect the page life cycle.
 *
 * @since    1.0.0
 */
function run_cpt_boilerplate() {

	$plugin = new CPT_Boilerplate();
	$plugin->run();

}
run_cpt_boilerplate();
