<?php

class CPT_Boilerplate_Messages{
	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $plugin_name    The ID of this plugin.
	 */
	private $plugin_name;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * Format of each message string
	 * wp format: <div class="%type$s"><p>%content$s</p></div>
	 * bootstrap format: <div class="alert alert-%type$s" role="alert"><p>%content$s</p></div>
	 * jqueryui: <div class="%type$s ui-corner-all"><p>%content$s</p></div>
	 * @var string
	 */
	private $format = '<div class="%type$s"><p>%content$s</p></div>';

	private $_messages = array(
		''=>array(
			/**
			 * wp: updated, error, update-nag
			 * bootstrap: success, info, warning, danger
			 * jqueryui: ui-state-highlight, ui-state-error
			 */
			'type'=>'',
			'content'=>'',//message content
		),
		'sample'=>array(
			'type'=>'updated',
			'content'=>'This is a sample message with a variable: %var$s.',
		),
		1=>array(
			'type'=>'updated',
			'content'=>'Settings Saved.',
		),
	);

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @var      string    $plugin_name       The name of this plugin.
	 * @var      string    $version    The version of this plugin.
	 */
	public function __construct( $plugin_name, $version ) {

		$this->plugin_name = $plugin_name;
		$this->version = $version;

	}

	/**
	 * load and display messages from query string
	 * @return sting messaging
	 */
	public function display(){
		$messages = isset($_GET['message'])? $_GET['message']:array();
		$message_vars = isset($_GET['message-variables'])? $_GET['message-variables']:array();
		if(!is_array($messages)){
			$messages = array($messages);
		}

		$messages = array_flip($messages);
		$messages = array_intersect_key($this->_messages,$messages);
		foreach($messages as $message){
			$message['content'] = $this->vksprintf($message['content'],$message_vars);
			echo $this->vksprintf($this->format,$message);
		}
	}

	/**
	 * vsprintf functionality with a keyed array
	 * @param  string $str  message format
	 * @param  array $args keyed array for replacements
	 * @return string       resulting message
	 */
	function vksprintf($str, $args) {
		if (is_object($args)) {
			$args = get_object_vars($args);
		}
		$map = array_flip(array_keys($args));
		$new_str = preg_replace_callback('/(^|[^%])%([a-zA-Z0-9_-]+)\$/',
			function($m) use ($map) { return $m[1].'%'.($map[$m[2]] + 1).'$'; },
			$str
		);
		return vsprintf($new_str, $args);
	}
}