<div class="wrap">
	<h2>Sample Display Page</h2>
	<?php
		//pull in any messages for user
		do_action($plugin_name.'/display_messages');
	?>
	<p>The purpose of this page is to demonstrate a display mode with form that submits to a handler.  It makes use of a nonce check as well as redirect after processing with user message.</p>
	<form method="post">
		<input type="submit" value="Submit" class="button button-primary">

		<?php
			//security nonce - must match the corresponding pre header output action check
			wp_nonce_field( 'display',$plugin_name.'-nonce');
		?>
	</form>
</div>