<?php

/**
 * Provide a dashboard view for the plugin
 *
 * This file is used to markup the public-facing aspects of the plugin.
 *
 * @link       http://www.redclayinteractive.com
 * @since      1.0.0
 *
 * @package    CPT_Boilerplate
 * @subpackage CPT_Boilerplate/admin/partials
 */
?>

<!-- This file should primarily consist of HTML with a little bit of PHP. -->
<div class="wrap">
	<?php screen_icon(); ?>
	<form method="post" action="options.php?<?php echo $plugin_name; ?>-optgroup=<?php echo $options_group['key']; ?>">
		<?php
			settings_fields( $settings_group );
			do_settings_sections( $settings_page );
			submit_button();
		?>
	</form>
</div>
