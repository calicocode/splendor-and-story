<?php

/**
 * The dashboard-specific functionality of the plugin.
 *
 * @link       http://www.redclayinteractive.com
 * @since      1.0.0
 *
 * @package    CPT_Boilerplate
 * @subpackage CPT_Boilerplate/admin
 */

/**
 * The dashboard-specific functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the dashboard-specific stylesheet and JavaScript.
 *
 * @package    CPT_Boilerplate
 * @subpackage CPT_Boilerplate/admin
 * @author     Yancey Vickers <yvickers@redclayinteractive.com>
 */
class CPT_Boilerplate_Admin {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $plugin_name    The ID of this plugin.
	 */
	private $plugin_name;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * Display title of plugin
	 * @var string
	 */
	private $title = 'CPT Boilerplate';

	/**
	 * individual options for settings page
	 * @var array
	 */
	protected $_options = array(
		'default'=>array(
			'key'=>'default',
			'title'=>'General',
			'description'=>'',
			'fields'=>array(
				/**
				 * key: used for storage meta_key
				 * label: label on settings page
				 * callback: function to use for display on settings page
				 * value: current value, set here for default, load options will pull from database
				 */
				'example_setting'=>array(
					'key'=>'example_setting',
					'label'=>'Text Example',
					'callback'=>'_default_input',
					'value'=>'default value',
					'description'=>'You can put in some description text here.',
				),

				'textarea_setting'=>array(
					'key'=>'textarea_setting',
					'label'=>'Textarea Example',
					'callback'=>'_default_input',
					'output_function'=>'_textarea_input',
					'value'=>'',
				),

				'select'=>array(
					'key'=>'select',
					'label'=>'Simple Select',
					'callback'=>'_default_input',
					'output_function'=>'_select',
					'description'=>'',
					'value'=>'',
					'options'=>array(
						array('label'=>'-- Select --','value'=>''),
						array('label'=>'Yes','value'=>'yes'),
						array('label'=>'No','value'=>'no'),
					),
				),
			),
		),
	);

	protected $_options_group = 'default';

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @var      string    $plugin_name       The name of this plugin.
	 * @var      string    $version    The version of this plugin.
	 */
	public function __construct( $plugin_name, $version ) {

		$this->plugin_name = $plugin_name;
		$this->version = $version;

		$this->_set_options_group();
	}

	/**
	 * Add Entries to settings menu
	 */
	public function add_pages(){
		//sample to add page into options menu
		//add_options_page('Settings',$this->title,'manage_options',$this->plugin_name.'-settings',array($this,'settings_page'));

		//sample to add top level page
		//add_menu_page( $this->title, $this->title, 'manage_options', $this->plugin_name, array($this,'main_switch') );

		//top level menu for settings and each setting group
		add_menu_page( $this->title, $this->title, 'manage_options', $this->plugin_name.'-settings', array($this,'settings_page') );
		if(count($this->_options) > 1){
			foreach($this->_options as $k=>$group){
				add_submenu_page( $this->plugin_name.'-settings', $group['title'], $group['title'], 'manage_options', $this->plugin_name.'-settings&'.$this->plugin_name.'-optgroup='.$k, array($this,'settings_page') );
			}
		}
		//sample submenu page setup
		//add_submenu_page( $this->plugin_name, 'Sub Menu Page', 'Sub Menu Page', 'capability_required', $this->plugin_name, array($this,'method') );
	}

	/**
	 * sets up our pages - this runs before output is sent to browser
	 */
	public function init_page(){
		//this allows the saving of default settings (since the page parameter isn't passed)
		if(isset($_POST['option_page']) && $_POST['option_page'] == $this->plugin_name.'-settings-group'){
			$this->_settings_page();
			return;
		}

		if(!isset($_GET['page'])){
			return;
		}

		switch($_GET['page']){
			case $this->plugin_name:
				$this->_main_switch();
			break;
			case $this->plugin_name.'-settings':
				$this->_settings_page();
			break;
			default:
				return;
			break;
		}
	}

	/**
	 * routes mode to functionality before headers presented
	 * this lets us handle any forms and redirect browser if necessary
	 */
	function _main_switch(){
		$mode = $this->_request_var('mode');
		if($mode == ''){
			$mode = 'display';
		}

		switch($mode){
			case 'display':
			default:
				$this->_display();
			break;
		}
	}

	/**
	 * routes our display output
	 */
	public function main_switch(){
		$mode = $this->_request_var('mode');
		if($mode == ''){
			$mode = 'display';
		}

		switch($mode){
			case 'display':
			default:
				$this->display();
			break;
		}
	}

	/**
	 * this is where we would handle any forms for the display function
	 * use this as an example of how to test for a wordpress nonce
	 */
	function _display(){
		$nonce = $this->plugin_name.'-nonce';
		$nonce_action = 'display';

		if(!isset($_POST[$nonce])){
			return;
		}

		if(!check_admin_referer($nonce_action,$nonce)){
			return;
		}

		/**
		 * handle anything with the form here
		 */

		//redirect back to a display mode with a message for user
		$query = array(
			'mode'=>'display',
			'message'=>'sample',
			'message-variables'=>array(
				'var'=>'foo',
			),
		);
		$this->_redirect($query);
	}

	/**
	 * sample display output page
	 * demonstrates simple form and nonce check along with messages
	 * @return string html
	 */
	function display(){
		/**
		 * load any information we need from database, set variables to use in templates
		 */
		$plugin_name = $this->plugin_name;
		include(plugin_dir_path( dirname( __FILE__ ) ).'admin/partials/display.php');
	}

	/**
	 * determine which options group to set up based on query string
	 */
	function _set_options_group(){
		$this->_options_group = isset($_GET[$this->plugin_name.'-optgroup'])? $_GET[$this->plugin_name.'-optgroup']:$this->_options_group;
		//ensure that the string passed in the variable actually exists
		if(!isset($this->_options[$this->_options_group])){
			$this->_options_group = 'default';
		}
		//apply some defaults just in case
		$this->_options[$this->_options_group] + array('title'=>$this->title,'fields'=>array(),'key'=>'default','description'=>'',);
	}

	/**
	 * register settings with wordpress
	 */
	function _settings_page(){
		//get our settings values from database
		$this->_load_options();

		/**
		 * handle general settings save
		 */
		if(count($_POST) > 0 && check_admin_referer($this->plugin_name.'-settings-group-options')){
			/**
			 * If you want to manually handle the settings page, do so here
			 * recommend you submit to something other than options.php (include page parameter)
			 */
			//$this->_save_settings();
		}

		foreach($this->_options[$this->_options_group]['fields'] as $k => &$v) {
			register_setting(
				$this->plugin_name.'-settings-group',
				$this->plugin_name.'_'.$k
			);
		}

		add_settings_section(
			$this->plugin_name.'-settings-section',
			$this->_options[$this->_options_group]['title'].' Settings',
			array(&$this, 'render_section'),
			$this->plugin_name.'-settings'
		);

		foreach($this->_options[$this->_options_group]['fields'] as $k => &$v) {
			$v['label_for'] = $this->plugin_name.'_'.$k;
			add_settings_field(
				$this->plugin_name.'-'.$k,
				$v['label'],
				array(&$this, $v['callback']),
				$this->plugin_name.'-settings',
				$this->plugin_name.'-settings-section',
				$v
			);
		}

		/**
		 * this hooks into the wp settings errors api
		 * http://wordpress.stackexchange.com/questions/15354/passing-error-warning-messages-from-a-meta-box-to-admin-notices
		 */
		add_action('admin_notices',array($this,'_settings_errors'));
	}

	/**
	 * output WP settings error transient contents
	 */
	function _settings_errors(){
		settings_errors();
	}

	/**
	 * Custom settings page handler, this will mimic the regular wordpress settings save
	 * use the switch statement to perform any manipulations before saving in options table
	 */
	function _save_settings(){
		foreach($this->_options[$this->_options_group]['fields'] as $k => $v) {
			switch($k){
				default:
					update_option($this->plugin_name.'_'.$k,$_POST[$this->plugin_name.'_'.$k]);
				break;
			}
		}

		$query = array(
			'page'=>$this->plugin_name.'-settings',
			'message'=>1,
		);
		$this->_redirect($query);
	}

	/**
	 * load options from wp-options table and place value in class variables
	 * use the switch statement to perform any manipulations before setting option value for class
	 */
	function _load_options(){
		foreach($this->_options[$this->_options_group]['fields'] as $k => &$v) {
			switch($k){
				default:
					$v['value'] = get_option($this->plugin_name.'_'.$k,$v['value']);
				break;
			}
		}
	}

	/**
	 * Output settings page
	 */
	function settings_page(){

		//pass some class variables to template
		$title = $this->title;
		$plugin_name = $this->plugin_name;
		$option_groups = $this->_options;
		$options_group = $this->_options[$this->_options_group];
		$settings_group = $this->plugin_name.'-settings-group';
		$settings_page = $this->plugin_name.'-settings';
		include(plugin_dir_path( dirname( __FILE__ ) ).'admin/partials/settings-page.php');
	}

	/**
	 * renders wp section, output custom messages here
	 */
	function render_section(){
		do_action($this->plugin_name.'/display_messages');

		if('' != $this->_options[$this->_options_group]['description']){
			echo '<p>'.$this->_options[$this->_options_group]['description'].'</p>';
		}
	}

	/**
	 * routes basic show/save inputs through display functions based on passed arguments
	 * @param  array $args arguments from add_settings_field
	 * @return string       html
	 */
	function _default_input($args){
		$fxn = isset($args['output_function'])? $args['output_function']:'_text_input';
		echo $this->$fxn($args['key'],$args);
	}

	/**
	 * general text input type for settings
	 * @param  string $key     options key
	 * @param  array  $params  parameters to affect output
	 * @param  array  $attribs additional html attributes to avoid having to overwrite most used ones
	 * @return string          html input field
	 */
	function _text_input($key,$params = array(),$attribs = array()){
		$params += array(
			'attributes'=>array(
				'id'=>$this->plugin_name.'_'.$key,
				'name'=>$this->plugin_name.'_'.$key,
				'value'=>$this->_options[$this->_options_group]['fields'][$key]['value'],
				'type'=>'text',
				'size'=>60,
			),
		);
		extract($params);
		$attribs += $attributes;

		if(isset($description) && '' != $description){
			$description = '<p>'.$description.'</p>';
		}

		return '<input '.$this->_html_attribs($attribs).'>'.$description;
	}

	/**
	 * general textarea input type for settings
	 * @param  string $key     options key
	 * @param  array  $params  parameters to affect output
	 * @param  array  $attribs additional html attributes to avoid having to overwrite most used ones
	 * @return string          html textarea input field
	 */
	function _textarea_input($key,$params = array(),$attribs = array()){
		$params += array(
			'attributes'=>array(
				'id'=>$this->plugin_name.'_'.$key,
				'name'=>$this->plugin_name.'_'.$key,
				'cols'=>60,
				'rows'=>4,
			),
			'value'=>$this->_options[$this->_options_group]['fields'][$key]['value'],
		);
		extract($params);
		$attribs += $attributes;

		if(isset($description) && '' != $description){
			$description = '<p>'.$description.'</p>';
		}

		return '<textarea '.$this->_html_attribs($attribs).'>'.esc_html($value).'</textarea>';
	}

	/**
	 * generate html for standard html select input
	 * @param  string $key     options key
	 * @param  array  $params  parameters to affect output
	 * @param  array  $attribs additional html attributes to avoid having to overwrite most used ones
	 * @return string         html select input field
	 */
	function _select($key,$params = array(),$attribs = array()){
		$params += array(
			'attributes'=>array(
				'id'=>$this->plugin_name.'_'.$key,
				'name'=>$this->plugin_name.'_'.$key,
			),
			'options'=>array(),
			'value'=>$this->_options[$this->_options_group]['fields'][$key]['value'],
		);
		extract($params);
		$attribs += $attributes;

		$ret = array(
			'<select '.$this->_html_attribs($attribs).'>',
		);
		foreach($options as $option){
			$sel = ($option['value'] == $value)? ' SELECTED':'';
			$ret[] = '<option value="'.$option['value'].'"'.$sel.'>'.$option['label'].'</option>';
		}

		$ret[] = '</select>';

		if(isset($description) && '' != $description){
			$ret[] = '<p>'.$description.'</p>';
		}

		return implode("\n",$ret);
	}

	/**
	 * generate html for a single post selection input
	 * @param  array  $args input parameters passed from wp register_setting
	 * @return string       html dropdown input field
	 */
	function _post_select($args = array()){
		$get_post_type_args = $args['get_post_type_args'] += array(
			'post_type'=>'post',
			'posts_per_page'=>-1,
			'orderby'=>'title',
			'order'=>'ASC',
		);

		$args['options'][] = array(
			'value'=>'',
			'label'=>'-- Select --',
		);

		//load field groups
		$posts = get_posts($get_post_type_args);

		foreach($posts as $post){
			$args['options'][] = array('value'=>$post->ID,'label'=>$post->post_title);
		}

		$args['value'] = get_option($this->plugin_name.'_'.$args['key']);

		echo $this->_select($args['key'],$args);
	}

	/**
	 * generate html for advanced custom fields selection input
	 * @param  array  $args input parameters passed from wp register_setting
	 * @return string       html dropdown input field
	 */
	function _acf_select($args = array()){
		if(!isset($args['get_post_type_args'])){
			$args['get_post_type_args'] = array();
		}

		$args['get_post_type_args'] += array(
			'post_type'=>'acf-field-group',
		);

		$this->_post_select($args);
	}

	/**
	 * generate html for a gravity forms selection input
	 * @param  array  $args input parameters passed from wp register_setting
	 * @return string       html dropdown input field
	 */
	function _gravity_form_select($args = array()){
		$args['options'][] = array(
			'value'=>'',
			'label'=>'-- Select --',
		);

		$forms = RGFormsModel::get_forms( null, 'title' );
		foreach( $forms as $form ){
			$args['options'][] = array('label'=>$form->title,'value'=>$form->id);
		}

		echo $this->_select($args['key'],$args);
	}

	/**
	 * use this function to output an admin wide nag message
	 */
	function nag_message(){
		//update-nag updated error
		//echo '<div class="update-nag"><p><b>'.$this->title.'</b>:  Some message.</p></div>';
	}

	/**
	 * Register the stylesheets for the Dashboard.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_styles() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in CPT_Boilerplate_Admin_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The CPT_Boilerplate_Admin_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		$current_page = isset($_GET['page'])? $_GET['page']:'';
		if(false === strpos($current_page, $this->plugin_name)){
			return;
		}

		//wp_enqueue_style( $this->plugin_name.'-font-awesome', '//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css', array(), '4.3.0' );
		//wp_enqueue_style( $this->plugin_name.'-bootstrap', '//maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css', array(), '3.3.5' );
		//wp_enqueue_style( $this->plugin_name.'-admin-ui-css','http://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/themes/redmond/jquery-ui.css',false,PLUGIN_VERSION,false);
		wp_enqueue_style( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'css/cpt-boilerplate-admin.css', array(), $this->version, 'all' );

	}

	/**
	 * Register the JavaScript for the dashboard.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_scripts() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in CPT_Boilerplate_Admin_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The CPT_Boilerplate_Admin_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		$current_page = isset($_GET['page'])? $_GET['page']:'';
		if(false === strpos($current_page, $this->plugin_name)){
			return;
		}

		//https://codex.wordpress.org/Function_Reference/wp_enqueue_script - other available scripts that are registered by wordpress
		//wp_enqueue_script('jquery');
		//wp_enqueue_script('jquery-ui-core');
		//wp_enqueue_script('jquery-ui-tabs');
		//wp_enqueue_script('jquery-ui-accordion');
		//wp_enquque_script('jquery-ui-sortable');
		//wp_enquque_script('jquery-ui-draggable');
		//wp_enquque_script('jquery-ui-droppable');
		//wp_enquque_script('jquery-ui-datepicker');
		//wp_enquque_script('jquery-ui-dialog');

		wp_enqueue_script( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'js/cpt-boilerplate-admin.js', array( 'jquery' ), $this->version, false );

	}

	/**
	 * admin redirect for plugin
	 * @param  array  $args query string arguments to build url with
	 */
	function _redirect($args = array()){
		$args += array(
			'page'=>$this->plugin_name,
		);
		wp_redirect(admin_url('admin.php?'.http_build_query($args)));
		exit;
	}

	/**
	 * retrieve a variable from post or get
	 * @param  string $name variable name
	 * @return mixed       variable value
	 */
	function _request_var($name){
		if(isset($_POST[$name])){
			return $_POST[$name];
		}
		if(isset($_GET[$name])){
			return $_GET[$name];
		}
		return false;
	}

	/**
	 * retrieve a variable from cookie, set if in request
	 * @param  string $name variable name
	 * @return mixed       variable value
	 */
	function _persistent_var($name, $default = false){
		$ret = $default;
		$cookie_name = $this->plugin_name.$name;

		$existing_value = $this->_request_var($name);
		if(false !== $existing_value){
			setcookie($cookie_name,$existing_value);
			return $existing_value;
		}

		if(isset($_COOKIE[$cookie_name])){
			$ret = $_COOKIE[$cookie_name];
		}

		return $ret;
	}
}