<?php

/**
 * Fired during plugin activation
 *
 * @link       http://www.redclayinteractive.com
 * @since      1.0.0
 *
 * @package    CPT_Boilerplate
 * @subpackage CPT_Boilerplate/includes
 */

/**
 * Fired during plugin activation.
 *
 * This class defines all code necessary to run during the plugin's activation.
 *
 * @since      1.0.0
 * @package    CPT_Boilerplate
 * @subpackage CPT_Boilerplate/includes
 * @author     Yancey Vickers <yvickers@redclayinteractive.com>
 */
class CPT_Boilerplate_Activator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function activate() {
		/**
		 * It might be helpful to force some settings for this site on installation
		 * Especially helpful to future developers who may be working on this site
		 */
		//update_option('some_wp_setting','some_value');

		/**
		 * Do you need to create a front page and use that?
		 */
		/*
		update_option('show_on_front','page');
		$home_page = get_page_by_path('home-page');
		if(is_null($home_page)){
			$home_page_id = wp_insert_post(array(
				'post_title'=>'Home Page',
				'post_name'=>'home-page',
				//'post_content'=>'Optionally setup some content',
				'post_status'=>'publish',
				'post_type'=>'page',
			));
		}else{
			$home_page_id = $home_page->ID;
		}*/

		/**
		 * Do you need to create a posts page?
		 */
		/*
		update_option('page_on_front',$home_page_id);
		$updates_page = get_page_by_path('news');
		if(is_null($updates_page)){
			$updates_page_id = wp_insert_post(array(
				'post_title'=>'News',
				'post_name'=>'news',
				'post_status'=>'publish',
				'post_type'=>'page',
			));
		}else{
			$updates_page_id = $updates_page->ID;
		}
		update_option('page_for_posts',$updates_page_id);
		*/

		/**
		 * Are there any pages that are required?
		 * Do they have a particular template?
		 */
		/*
		$required_page = get_page_by_path('page-slug');
		if(is_null($required_page)){
			$required_page_id = wp_insert_post(array(
				'post_title'=>'Page Title',
				'post_name'=>'page-slug',
				'post_status'=>'publish',
				'post_type'=>'page',
				'meta_input'=>array(
					'_wp_page_template'=>'templates/some-template.php',
				),
			));
		}else{
			$required_page_id = $required_page->ID;
		}*/

	}

}
