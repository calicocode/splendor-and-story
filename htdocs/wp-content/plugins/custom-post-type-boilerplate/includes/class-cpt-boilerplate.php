<?php

/**
 * The file that defines the core plugin class
 *
 * A class definition that includes attributes and functions used across both the
 * public-facing side of the site and the dashboard.
 *
 * @link       http://www.redclayinteractive.com
 * @since      1.0.0
 *
 * @package    CPT_Boilerplate
 * @subpackage CPT_Boilerplate/includes
 */

/**
 * The core plugin class.
 *
 * This is used to define internationalization, dashboard-specific hooks, and
 * public-facing site hooks.
 *
 * Also maintains the unique identifier of this plugin as well as the current
 * version of the plugin.
 *
 * @since      1.0.0
 * @package    CPT_Boilerplate
 * @subpackage CPT_Boilerplate/includes
 * @author     Yancey Vickers <yvickers@redclayinteractive.com>
 */
class CPT_Boilerplate {

	/**
	 * The loader that's responsible for maintaining and registering all hooks that power
	 * the plugin.
	 *
	 * @since    1.0.0
	 * @access   protected
	 * @var      CPT_Boilerplate_Loader    $loader    Maintains and registers all hooks for the plugin.
	 */
	protected $loader;

	/**
	 * The unique identifier of this plugin.
	 *
	 * @since    1.0.0
	 * @access   protected
	 * @var      string    $plugin_name    The string used to uniquely identify this plugin.
	 */
	protected $plugin_name;

	/**
	 * The current version of the plugin.
	 *
	 * @since    1.0.0
	 * @access   protected
	 * @var      string    $version    The current version of the plugin.
	 */
	protected $version;

	/**
	 * Define the core functionality of the plugin.
	 *
	 * Set the plugin name and the plugin version that can be used throughout the plugin.
	 * Load the dependencies, define the locale, and set the hooks for the Dashboard and
	 * the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function __construct() {

		$this->plugin_name = CPT_BOILERPLATE_STEM;
		$this->version = CPT_BOILERPLATE_VERSION;

		$this->load_dependencies();
		$this->set_locale();
		//$this->define_admin_hooks();
		//$this->define_message_hooks();
		$this->define_public_hooks();

	}

	/**
	 * Load the required dependencies for this plugin.
	 *
	 * Include the following files that make up the plugin:
	 *
	 * - CPT_Boilerplate_Loader. Orchestrates the hooks of the plugin.
	 * - CPT_Boilerplate_i18n. Defines internationalization functionality.
	 * - CPT_Boilerplate_Admin. Defines all hooks for the dashboard.
	 * - CPT_Boilerplate_Public. Defines all hooks for the public side of the site.
	 *
	 * Create an instance of the loader which will be used to register the hooks
	 * with WordPress.
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function load_dependencies() {
		//check for composer libraries
		$vendor_file = plugin_dir_path( dirname( __FILE__ ) ) . 'vendor/autoload.php';
		if(file_exists($vendor_file)){
			require_once $vendor_file;
		}

		/**
		 * The class responsible for orchestrating the actions and filters of the
		 * core plugin.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/class-cpt-boilerplate-loader.php';

		/**
		 * The class responsible for defining internationalization functionality
		 * of the plugin.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/class-cpt-boilerplate-i18n.php';

		/**
		 * The class responsible for defining all actions that occur in the Dashboard.
		 */
		//require_once plugin_dir_path( dirname( __FILE__ ) ) . 'admin/class-cpt-boilerplate-admin.php';

		/**
		 * This class contains admin messages and displays them using the cpt-boilerplate/display_messages action
		 */
		//require_once plugin_dir_path( dirname( __FILE__ ) ) . 'admin/class-cpt-boilerplate-messages.php';

		/**
		 * The class responsible for defining all actions that occur in the public-facing
		 * side of the site.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'public/class-cpt-boilerplate-public.php';

		$this->loader = new CPT_Boilerplate_Loader();

	}

	/**
	 * Define the locale for this plugin for internationalization.
	 *
	 * Uses the CPT_Boilerplate_i18n class in order to set the domain and to register the hook
	 * with WordPress.
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function set_locale() {

		$plugin_i18n = new CPT_Boilerplate_i18n();
		$plugin_i18n->set_domain( $this->get_cpt_boilerplate() );

		$this->loader->add_action( 'plugins_loaded', $plugin_i18n, 'load_plugin_textdomain' );

	}

	/**
	 * Register all of the hooks related to the dashboard functionality
	 * of the plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function define_admin_hooks() {

		$plugin_admin = new CPT_Boilerplate_Admin( $this->get_cpt_boilerplate(), $this->get_version() );

		$this->loader->add_action( 'admin_enqueue_scripts', $plugin_admin, 'enqueue_styles' );
		$this->loader->add_action( 'admin_enqueue_scripts', $plugin_admin, 'enqueue_scripts' );

		$this->loader->add_action('admin_menu',$plugin_admin,'add_pages');
		$this->loader->add_action('admin_init',$plugin_admin,'init_page');

		$this->loader->add_action('admin_notices',$plugin_admin,'nag_message');
	}

	/**
	 * register hooks for the messaging class
	 *
	 * @since  1.0.0
	 * @access 	private
	 */
	private function define_message_hooks(){

		$plugin_messages = new CPT_Boilerplate_Messages( $this->get_cpt_boilerplate(), $this->get_version() );

		$this->loader->add_action($this->plugin_name.'/display_messages',$plugin_messages,'display');
	}

	/**
	 * Register all of the hooks related to the public-facing functionality
	 * of the plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function define_public_hooks() {

		$plugin_public = new CPT_Boilerplate_Public( $this->get_cpt_boilerplate(), $this->get_version() );

		$this->loader->add_action('init',$plugin_public,'init');
		$this->loader->add_action('wp_loaded',$plugin_public,'auto_actions');

		/**
		 * You may want to add acf fields or remove meta boxes for the built in WP types
		 */
		//$this->loader->add_action('admin_menu',$plugin_public,'remove_meta_boxes');
		//$this->loader->add_action('acf/init',$plugin_public,'posts_acf');

		$this->loader->add_action($this->plugin_name.'/theme_template',$plugin_public,'theme_template',10,2);

		$this->loader->add_action( 'wp_enqueue_scripts', $plugin_public, 'enqueue_styles' );
		$this->loader->add_action( 'wp_enqueue_scripts', $plugin_public, 'enqueue_scripts' );

	}

	/**
	 * Run the loader to execute all of the hooks with WordPress.
	 *
	 * @since    1.0.0
	 */
	public function run() {
		$this->loader->run();
	}

	/**
	 * The name of the plugin used to uniquely identify it within the context of
	 * WordPress and to define internationalization functionality.
	 *
	 * @since     1.0.0
	 * @return    string    The name of the plugin.
	 */
	public function get_cpt_boilerplate() {
		return $this->plugin_name;
	}

	/**
	 * The reference to the class that orchestrates the hooks with the plugin.
	 *
	 * @since     1.0.0
	 * @return    CPT_Boilerplate_Loader    Orchestrates the hooks of the plugin.
	 */
	public function get_loader() {
		return $this->loader;
	}

	/**
	 * Retrieve the version number of the plugin.
	 *
	 * @since     1.0.0
	 * @return    string    The version number of the plugin.
	 */
	public function get_version() {
		return $this->version;
	}

}
