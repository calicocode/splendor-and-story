<?php

/**
 * Fired during plugin deactivation
 *
 * @link       http://www.redclayinteractive.com
 * @since      1.0.0
 *
 * @package    CPT_Boilerplate
 * @subpackage CPT_Boilerplate/includes
 */

/**
 * Fired during plugin deactivation.
 *
 * This class defines all code necessary to run during the plugin's deactivation.
 *
 * @since      1.0.0
 * @package    CPT_Boilerplate
 * @subpackage CPT_Boilerplate/includes
 * @author     Yancey Vickers <yvickers@redclayinteractive.com>
 */
class CPT_Boilerplate_Deactivator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function deactivate() {

	}

}
