# Custom Post Type Plugin Boilerplate

A starting point for generating the custom post types for a Wordpress Site

## Features

* The Boilerplate is based on the [Plugin API](http://codex.wordpress.org/Plugin_API), [Coding Standards](http://codex.wordpress.org/WordPress_Coding_Standards), and [Documentation Standards](http://make.wordpress.org/core/handbook/inline-documentation-standards/php-documentation-standards/).
* Built on top of Wordpress Plugin Boilerplate https://github.com/devinvinson/WordPress-Plugin-Boilerplate/
* All classes, functions, and variables are documented so that you know what you need to be changed.
* The Boilerplate uses a strict file organization scheme that correspond both to the WordPress Plugin Repository structure, and that make it easy to organize the files that compose the plugin.
* The project includes a `.pot` file as a starting point for internationalization.

## Installation

1. Copy the folder to your wp-content/plugins directory.  Please rename the folder
1. Change the plugin definition variables within cpt-boilerplate.php.  At least Plugin Name and Description
1. Replace the generic strings within files with your project specific ones
    1. CPT_Boilerplate => Your_Plugin_Name  This renames class names
    1. cpt-boilerplate => your-plugin-name  This changes file includes and plugin slugs
    1. _cpt_boilerplate => _your_plugin_name  This changes function names and makes them unique
    1. CPT_BOILERPLATE => YOUR_PLUGIN_NAME  This changes any constants used
1. Rename your files.  cpt-boilerplate => your-plugin-name
1. Activate the plugin

## Usage
* Copy the example_post_type.php into a new file.
* Change the name, key, registration function settings, and any acf fields to generate your new post type
* Include your new file within the appropriate section within the public class.
* Optionally, use the activation hook within the includes folder to apply some initial settings

## Recommended Tools

### i18n Tools

The WordPress Plugin Boilerplate uses a variable to store the text domain used when internationalizing strings throughout the Boilerplate. To take advantage of this method, there are tools that are recommended for providing correct, translatable files:

* [Poedit](http://www.poedit.net/)
* [makepot](http://i18n.svn.wordpress.org/tools/trunk/)
* [i18n](https://github.com/grappler/i18n)

Any of the above tools should provide you with the proper tooling to internationalize the plugin.

## License

This plugin and its basis WordPress Plugin Boilerplate is licensed under the GPL v2 or later.

> This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License, version 2, as published by the Free Software Foundation.

> This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

> You should have received a copy of the GNU General Public License along with this program; if not, write to the Free Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA

A copy of the license is included in the root of the plugin’s directory. The file is named `LICENSE`.

## Important Notes

### Licensing

This plugin and its basis WordPress Plugin Boilerplate is licensed under the GPL v2 or later; however, if you opt to use third-party code that is not compatible with v2, then you may need to switch to using code that is GPL v3 compatible.

For reference, [here's a discussion](http://make.wordpress.org/themes/2013/03/04/licensing-note-apache-and-gpl/) that covers the Apache 2.0 License used by [Bootstrap](http://twitter.github.io/bootstrap/).

### Includes

Note that if you include your own classes, or third-party libraries, there are three locations in which said files may go:

* `cpt-boilerplate/includes` is where functionality shared between the dashboard and the public-facing parts of the side reside
* `cpt-boilerplate/admin` is for all dashboard-specific functionality
* `cpt-boilerplate/public` is for all public-facing functionality

Note that previous versions of the Boilerplate did not include `CPT_Boilerplate_Loader` but this class is used to register all filters and actions with WordPress.

# Credits

The WordPress Plugin Boilerplate was started in 2011 by [Tom McFarlin](http://twitter.com/tommcfarlin/) and his since included a number of great contributions.

The current version of the Boilerplate was developed in conjunction with [Josh Eaton](https://twitter.com/jjeaton), [Ulrich Pogson](https://twitter.com/grapplerulrich), and [Brad Vincent](https://twitter.com/themergency).