<?php

/**
 * The public-facing functionality of the plugin.
 *
 * @link       http://www.redclayinteractive.com
 * @since      1.0.0
 *
 * @package    CPT_Boilerplate
 * @subpackage CPT_Boilerplate/public
 */

/**
 * The public-facing functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the dashboard-specific stylesheet and JavaScript.
 *
 * @package    CPT_Boilerplate
 * @subpackage CPT_Boilerplate/public
 * @author     Yancey Vickers <yvickers@redclayinteractive.com>
 */
class CPT_Boilerplate_Public {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $plugin_name    The ID of this plugin.
	 */
	private $plugin_name;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @var      string    $plugin_name       The name of the plugin.
	 * @var      string    $version    The version of this plugin.
	 */
	public function __construct( $plugin_name, $version ) {

		$this->plugin_name = $plugin_name;
		$this->version = $version;

		$this->_load_custom_post_types();
	}

	/**
	 * include any custom post types
	 */
	function _load_custom_post_types(){
		include(plugin_dir_path( dirname( __FILE__ ) ).'public/post_types/base.php');
		include(plugin_dir_path( dirname( __FILE__ ) ).'public/post_types/project.php');

		/**
		 * include custom post types here
		 *
		 * Don't include the example, but you may want to copy it as a basis for your new post types
		 * include(plugin_dir_path( dirname( __FILE__ ) ).'public/post_types/example_post_type.php');
		 */
	}

	/**
	 * initialize plugin for front end
	 */
	function init(){

		/**
		 * define any shortcodes here
		 */

	}

	/**
	 * remove particular taxonomy term boxes from posts
	 */
	function remove_meta_boxes(){
		//remove_meta_box( "categorydiv", 'post', 'side' );
		//remove_meta_box( "tagsdiv_post_tag", 'post', 'side' );
	}

	/**
	 * Define any custom fields here - it is assumed you are using Advanced Custom Fields
	 * Please see the exampe post type for acf examples
	 *
	 * https://www.advancedcustomfields.com/resources/register-fields-via-php/
	 */
	function posts_acf(){
		$stem = 'post';
		acf_add_local_field_group(array(
			'key' => $stem,
			'title' => 'Additional Information',
			'fields' => array (
			),
			'location' => array (
				array (
					array (
						'param' => 'post_type',
						'operator' => '==',
						'value' => $stem,
					),
				),
			),
			'menu_order' => 0,
			'position' => 'normal',
			'style' => 'default',
			'label_placement' => 'top',
			'instruction_placement' => 'label',
			'hide_on_screen' => '',
			'active' => 1,
			'description' => '',
		));
	}

	/**
	 * listener to automatically run actions based on request variables
	 * typically run at the wp_loaded action so that all other plugins have had a chance to load
	 */
	function auto_actions(){
		//do any passed action for get or post
		if('' != ($action = $this->_request_var($this->plugin_name.'-action'))){
			do_action($this->plugin_name.'/'.$action);
		}
	}

	/**
	 * function to include a particular template file, from theme if present, plugin if not
	 *
	 * Usage:
	 * $variables = array();
	 * ob_start();
	 * $this->theme_template('template.php',$variables);
	 * $output = ob_get_contents();
	 * ob_end_clean();
	 * return or echo $output
	 * @param  mixed $template  string or array of templates with file with extension
	 * @param  array  $variables variables to pass to template
	 */
	function theme_template($templates,$variables = array()){
		//if passed a string, convert to array
		if(!is_array($templates)){
			$templates = array($templates);
		}

		//allow overriding of templates within theme files with sub folder
		$theme_templates = array();
		foreach($templates as $template){
			$theme_templates[] = $this->plugin_name.'/'.$template;
		}

		$variables += apply_filters($this->plugin_name.'/global_variables',$this->_get_globals());
		extract($variables);

		if ( $overridden_template = locate_template( $theme_templates ) ) {
			include( $overridden_template );
		} else {
			foreach($templates as $template){
				if(!is_file(plugin_dir_path( dirname( __FILE__ ) ).'public/partials/'.$template)){
					continue;
				}

				include( plugin_dir_path( dirname( __FILE__ ) ).'public/partials/'.$template );
				break;
			}
		}
	}

	/**
	 * pass standard wp global variables to templates
	 * @return array global variables as array to extract
	 */
	function _get_globals(){
		global $posts, $post, $wp_did_header, $wp_query, $wp_rewrite, $wpdb, $wp_version, $wp, $id, $comment, $user_ID;

		return array(
			'posts' => $posts,
			'post' => $post,
			'wp_did_header' => $wp_did_header,
			'wp_query' => $wp_query,
			'wp_rewrite' => $wp_rewrite,
			'wpdb' => $wpdb,
			'wp_version' => $wp_version,
			'wp' => $wp,
			'id' => $id,
			'comment' => $comment,
			'user_ID' => $user_ID,
		);
	}

	/**
	 * Register the stylesheets for the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_styles() {

		//wp_enqueue_style( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'css/cpt-boilerplate-public.css', array(), $this->version, 'all' );

	}

	/**
	 * Register the scripts for the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_scripts() {

		//wp_enqueue_script( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'js/cpt-boilerplate-public.js', array( 'jquery' ), $this->version, false );

	}

	/**
	 * check get and post arrays for a variable
	 * @param  string $var variable name
	 * @return string      variable value if found
	 */
	function _request_var($var){
		if(isset($_GET[$var])){
			return trim($_GET[$var]);
		}

		if(isset($_POST[$var])){
			return trim($_POST[$var]);
		}

		return '';
	}
}
