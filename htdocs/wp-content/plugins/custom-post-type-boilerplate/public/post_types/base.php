<?php

/**
 * All other post types should extend this base post type
 * There are additional functions that can be added here from example_post_type depending on functionality needed
 */
class CPT_Boilerplate_Base_Post_Type{

	/**
	 * unique key for post type
	 * @var string
	 */
	protected $key = '__base__';

	/**
	 * holds any warnings about improper setup of post type
	 * @var array
	 */
	protected $warnings = array();

	/**
	 * holds taxonomy slugs that each post should be synced as term for
	 * @var array
	 */
	protected $sync_taxonomies = array();

	function __construct(){
		if('__base__' == $this->key){
			$this->warnings[] = '<b>Missing Key</b>: Need a unique key to register with wp.';
			return;
		}

		add_action('init',array($this,'define_cpt'));
		add_action('acf/init',array($this,'acf_fields'));
		add_action('admin_notices',array($this,'warnings'));
	}

	/**
	 * change title field placeholder text
	 *
	 *	add_filter('enter_title_here',array($this,'change_title_text'));
	 *
	 * @param  string $title title placeholder text
	 * @return string        adjusted text
	 */
	function change_title_text( $title ){
		$screen = get_current_screen();
		if($this->key !== $screen->post_type){
			return $title;
		}

		return __('Enter custom title here', CPT_BOILERPLATE_STEM);
	}

	/**
	 * Rename the featured image box with a custom string
	 *
	 *	add_action('do_meta_boxes', array($this,'rename_featured_image_box'));
	 */
	function rename_featured_image_box(){
		$custom_label = __('Header Image.', CPT_BOILERPLATE_STEM);

		remove_meta_box( 'postimagediv', $this->key, 'side' );
		add_meta_box('postimagediv', $custom_label, 'post_thumbnail_meta_box', $this->key, 'side', 'low');
	}

	/**
	 * prevent particular taxonomy boxes even if someone has access to the UI
	 * This has been used to give admin level users access to the taxonomy term lists for a post type
	 * but not want them to be able to add them within the context of the detail page (perhaps using an ACF field instead)
	 *
	 *	add_action('admin_menu',array($this,'remove_meta_boxes'));
	 */
	function remove_meta_boxes(){
		remove_meta_box( "tagsdiv-taxonomy1", $this->key, 'side' );
		remove_meta_box( "tagsdiv-taxonomy2", $this->key, 'side' );
		remove_meta_box( "tagsdiv-taxonomy3", $this->key, 'side' );
	}

	/**
	 * inserts a new synced term into various taxonomies
	 * see delete_synced_term method and define $sync_taxonomies for class
	 *
	 *	add_action('save_post',array($this,'insert_synced_term'),10,3);
	 *
	 * @param  int $post_id wp post id
	 * @param  object $post    wp post object
	 * @param  boolean $update  true if update
	 */
	function insert_synced_term($post_id,$post,$update){
		if($this->key != $post->post_type){
			return;
		}

		if( wp_is_post_revision( $post_id ) ){
			return;
		}

		if($post->post_status == 'auto-draft'){
			return;
		}

		foreach($this->sync_taxonomies as $tax){
			$term_id = get_post_meta($post_id, $tax.'_term_id', true);
			if('' == $term_id){
				$term = wp_insert_term($post->post_title,$tax,array('slug'=>$post->post_name));
				if(!is_wp_error($term)){
					add_term_meta($term['term_id'], $this->key, $post_id, true);
					add_post_meta($post_id, $tax.'_term_id',$term['term_id']);
				}
			}else{
				wp_update_term( $term_id, $tax, array('name'=>$post->post_title,'slug'=>$post->post_name) );
			}
		}
	}

	/**
	 * delete a synced term when a post of this type is removed
	 * see insert_synced_term method and define $sync_taxonomies for class
	 *
	 *	add_action('before_delete_post',array($this,'delete_synced_term'));
	 *
	 * @param  int $post_id wp post id
	 */
	function delete_synced_term($post_id){
		$post = get_post($post_id);
		if ( $post->post_type != $this->key ){
			return;
		}

		foreach($this->sync_taxonomies as $tax){
			$term_id = get_post_meta($post_id, $tax.'_term_id', true);
			wp_delete_term($term_id,$tax);
		}
	}

	/**
	 * Registers post type with wordpress
	 * you should also define any taxonomies for this custom post type here
	 *
	 * Please see the example post type for what should be in this function
	 * Base plugin just registers warning for not being set up correctly
	 *
	 * More Information about options here:
	 * https://codex.wordpress.org/Function_Reference/register_post_type
	 * https://developer.wordpress.org/reference/functions/register_post_type/
	 *
	 * Creation wizard:
	 * https://generatewp.com/post-type/
	 *
	 */
	function define_cpt(){
		$this->warnings[] = '<b>Missing Method</b>: Need a define_cpt method to register with wp.';
	}

	/**
	 * Define any custom fields here - it is assumed you are using Advanced Custom Fields
	 * Please see the exampe post type for acf examples
	 *
	 * https://www.advancedcustomfields.com/resources/register-fields-via-php/
	 */
	function acf_fields(){

	}

	/**
	 * This is here if you have failed to provide a key for your custom post type
	 */
	function warnings(){
		if(count($this->warnings) == 0){
			return;
		}

		echo '<div class="error"><h2>'.get_class($this).'</h2><p>'.implode($this->warnings,'</p><p>').'</p></div>';
	}
}