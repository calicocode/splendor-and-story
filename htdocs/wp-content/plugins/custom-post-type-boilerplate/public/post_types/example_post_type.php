<?php

class CPT_Boilerplate_Example_Post_Type extends CPT_Boilerplate_Base_Post_Type{

	protected $key = 'example';

	/**
	 * Registers post type with wordpress
	 * you should also define any taxonomies for this custom post type here
	 *
	 * More Information about options here:
	 * https://codex.wordpress.org/Function_Reference/register_post_type
	 * https://developer.wordpress.org/reference/functions/register_post_type/
	 *
	 * Creation wizard:
	 * https://generatewp.com/post-type/
	 *
	 */
	function define_cpt(){

		$single_label = 'Item';
		$plural_label = 'Items';
		$lowercase = strtolower($single_label);

		$labels = array(
			'name'                  => _x( $single_label, 'Post Type General Name', CPT_BOILERPLATE_STEM ),
			'singular_name'         => _x( $single_label, 'Post Type Singular Name', CPT_BOILERPLATE_STEM ),
			'menu_name'             => __( $plural_label, CPT_BOILERPLATE_STEM ),
			'name_admin_bar'        => __( $plural_label, CPT_BOILERPLATE_STEM ),
			'archives'              => __( $single_label.' Archives', CPT_BOILERPLATE_STEM ),
			'parent_item_colon'     => __( 'Parent '.$single_label.':', CPT_BOILERPLATE_STEM ),
			'all_items'             => __( 'All '.$plural_label, CPT_BOILERPLATE_STEM ),
			'add_new_item'          => __( 'Add New '.$single_label, CPT_BOILERPLATE_STEM ),
			'add_new'               => __( 'Add New', CPT_BOILERPLATE_STEM ),
			'new_item'              => __( 'New '.$single_label, CPT_BOILERPLATE_STEM ),
			'edit_item'             => __( 'Edit '.$single_label, CPT_BOILERPLATE_STEM ),
			'update_item'           => __( 'Update '.$single_label, CPT_BOILERPLATE_STEM ),
			'view_item'             => __( 'View '.$single_label, CPT_BOILERPLATE_STEM ),
			'search_items'          => __( 'Search '.$single_label, CPT_BOILERPLATE_STEM ),
			'not_found'             => __( 'Not found', CPT_BOILERPLATE_STEM ),
			'not_found_in_trash'    => __( 'Not found in Trash', CPT_BOILERPLATE_STEM ),
			'featured_image'        => __( 'Featured Image', CPT_BOILERPLATE_STEM ),
			'set_featured_image'    => __( 'Set featured image', CPT_BOILERPLATE_STEM ),
			'remove_featured_image' => __( 'Remove featured image', CPT_BOILERPLATE_STEM ),
			'use_featured_image'    => __( 'Use as featured image', CPT_BOILERPLATE_STEM ),
			'insert_into_item'      => __( 'Insert into '.$single_label, CPT_BOILERPLATE_STEM ),
			'uploaded_to_this_item' => __( 'Uploaded to this '.$lowercase, CPT_BOILERPLATE_STEM ),
			'items_list'            => __( $single_label.' list', CPT_BOILERPLATE_STEM ),
			'items_list_navigation' => __( $single_label.' list navigation', CPT_BOILERPLATE_STEM ),
			'filter_items_list'     => __( 'Filter '.$lowercase.' list', CPT_BOILERPLATE_STEM ),
		);

		$args = array(
			'label'                 => __( $single_label, CPT_BOILERPLATE_STEM ),

			'description'           => __( '', CPT_BOILERPLATE_STEM ),

			'labels'                => $labels,

			//An alias for calling add_post_type_support() directly. As of 3.5, boolean false can be passed as value instead of an array to prevent default (title and editor) behavior.
			'supports'              => array(
				'title',
				'editor',
				'author',
				'thumbnail',
				'excerpt',
				'trackbacks',
				'custom-fields',
				'comments',
				'revisions',
				'page-attributes',
				'post-formats',
			),

			//Controls how the type is visible to authors (show_in_nav_menus, show_ui) and readers (exclude_from_search, publicly_queryable).
			//If no value is specified for exclude_from_search, publicly_queryable, show_in_nav_menus, or show_ui, they inherit their values from public.
			'public'                => true,

			//Whether to generate a default UI for managing this post type in the admin.
			'show_ui'               => true,

			//Where to show the post type in the admin menu. show_ui must be true.
			//Boolean or if an existing top level page such as 'tools.php' or 'edit.php?post_type=page', the post type will be placed as a sub menu of that.
			'show_in_menu'          => true,

			//Whether post_type is available for selection in navigation menus.
			'show_in_nav_menus'     => true,

			//Whether to make this post type available in the WordPress admin bar.
			'show_in_admin_bar'     => true,

			//The position in the menu order the post type should appear. show_in_menu must be true.
			//5 - below Posts, 10 - below Media, 15 - below Links, 20 - below Pages, 25 - below comments, 60 - below first separator, 65 - below Plugins, 70 - below Users, 75 - below Tools, 80 - below Settings, 100 - below second separator
			'menu_position'         => 20,

			//The url to the icon to be used for this menu or the name of the icon from the iconfont https://developer.wordpress.org/resource/dashicons/#welcome-view-site
			'menu_icon'             => 'dashicons-admin-post',

			//Whether to exclude posts with this post type from front end search results.
			'exclude_from_search'   => true,

			//Whether queries can be performed on the front end as part of parse_request().
			'publicly_queryable'    => false,

			//Whether the post type is hierarchical (e.g. page). Allows Parent to be specified. The 'supports' parameter should contain 'page-attributes' to show the parent select box on the editor page.
			'hierarchical'          => false,

			//Can this post_type be exported.
			'can_export'            => true,

			//Enables post type archives. Will use $post_type as archive slug by default.
			'has_archive'           => true,

			//Triggers the handling of rewrites for this post type. To prevent rewrites, set to false.
			/**
			 * $args array
			 * 'slug' => string Customize the permalink structure slug. Defaults to the $post_type value. Should be translatable.
			 * 'with_front' => bool Should the permalink structure be prepended with the front base. (example: if your permalink structure is /blog/, then your links will be: false->/news/, true->/blog/news/). Defaults to true
			 * 'feeds' => bool Should a feed permalink structure be built for this post type. Defaults to has_archive value.
			 * 'pages' => bool Should the permalink structure provide for pagination. Defaults to true
			 */
			'rewrite'               => array( 'slug' => $this->key ),

			/**
			 * read more about more complex capabilities within codex
			 * 99/100 you will just need post or page here
			 */
			'capability_type'       => 'post',
		);

		register_post_type( $this->key, $args );
	}

	/**
	 * Define any custom fields here - it is assumed you are using Advanced Custom Fields
	 * Each field type is present here, fields have common settings and special settings per field
	 *
	 * https://www.advancedcustomfields.com/resources/register-fields-via-php/
	 */
	function acf_fields(){
		$stem = strtolower(get_class($this));
		acf_add_local_field_group(array (
			'key' => $stem,
			'title' => 'Example Post Type Custom Fields',
			'fields' => array (

				/**
				 * Text field
				 *
				 * If copy/pasting, there is a simpler text configuration below this one
				 */
				array (
					/**
					 * Common field settings
					 */

					/* (string) Unique identifier for the field. Must begin with 'field_' */
					'key' => $stem.'_text',

					/* (string) Visible when editing the field value */
					'label' => 'Text',

					/* (string) Used to save and load data. Single word, no spaces. Underscores and dashes allowed */
					'name' => 'text',

					/* (string) Type of field (text, textarea, image, etc) */
					'type' => 'text',

					/* (string) Instructions for authors. Shown when submitting data */
					'instructions' => '',

					/* (int) Whether or not the field value is required. Defaults to 0 */
					'required' => 0,

					/* (mixed) Conditionally hide or show this field based on other field's values. Best to use the ACF UI and export to understand the array structure. Defaults to 0 */
					'conditional_logic' => 0,
					/* Sample conditional Logic
					'conditional_logic' => array(
						array(
							array(
								'field'=>'field_key_here',
								'operator'=>'==',
								'value'=>'some_value',
							),

							// "And" condition
							array(
								'field'=>'some_other_key_here',
								'operator'=>'==',
								'value'=>'some_value',
							),
						),
						// "Or" condition
						array(
							array(
								'field'=>'field_key_here',
								'operator'=>'==',
								'value'=>'some_other_value',
							),
						),
					),
					 */

					/* (array) An array of attributes given to the field element */
					'wrapper' => array (
						'width' => '',
						'class' => '',
						'id' => '',
					),

					/* (mixed) A default value used by ACF if no value has yet been saved */
					'default_value' => '',

					/**
					 * Text Field specific settings
					 */

					/* (string) Appears within the input. Defaults to '' */
					'placeholder' => '',

					/* (string) Appears before the input. Defaults to '' */
					'prepend' => '',

					/* (string) Appears after the input. Defaults to '' */
					'append' => '',

					/* (string) Restricts the character limit. Defaults to '' */
					'maxlength' => '',

					/* (bool) Makes the input readonly. Defaults to 0 */
					'readonly' => 0,

					/* (bool) Makes the input disabled. Defaults to 0 */
					'disabled' => 0,
				),

				/**
				 * Text field for copying
				 */
				array (
					'key' => $stem.'_clean_text',
					'label' => 'Text for copying',
					'name' => 'clean_text',
					'type' => 'text',
					'instructions' => '',
					'required' => 0,
					'conditional_logic' => 0,
					'wrapper' => array (
						'width' => '',
						'class' => '',
						'id' => '',
					),
					'default_value' => '',
					'placeholder' => '',
					'prepend' => '',
					'append' => '',
					'maxlength' => '',
					'readonly' => 0,
					'disabled' => 0,
				),

				/**
				 * Textarea field
				 */
				array (
					'key' => $stem.'_textarea',
					'label' => 'Textarea',
					'name' => 'textarea',
					'type' => 'textarea',
					'instructions' => '',
					'required' => 0,
					'conditional_logic' => 0,
					'wrapper' => array (
						'width' => '',
						'class' => '',
						'id' => '',
					),
					'default_value' => '',

					/* (string) Appears within the input. Defaults to '' */
					'placeholder' => '',

					/* (string) Restricts the character limit. Defaults to '' */
					'maxlength' => '',

					/* (int) Restricts the number of rows and height. Defaults to '' */
					'rows' => '',

					/* (new_lines) Decides how to render new lines. Detauls to 'wpautop'. Choices of 'wpautop' (Automatically add paragraphs), 'br' (Automatically add <br>) or '' (No Formatting) */
					'new_lines' => '',

					/* (bool) Makes the input readonly. Defaults to 0 */
					'readonly' => 0,

					/* (bool) Makes the input disabled. Defaults to 0 */
					'disabled' => 0,
				),

				/**
				 * Number field
				 */
				array (
					'key' => $stem.'_number',
					'label' => 'Number',
					'name' => 'number',
					'type' => 'number',
					'instructions' => '',
					'required' => 0,
					'conditional_logic' => 0,
					'wrapper' => array (
						'width' => '',
						'class' => '',
						'id' => '',
					),
					'default_value' => '',

					/* (string) Appears within the input. Defaults to '' */
					'placeholder' => '',

					/* (string) Appears before the input. Defaults to '' */
					'prepend' => '',

					/* (string) Appears after the input. Defaults to '' */
					'append' => '',

					/* (int) Minimum number value. Defaults to '' */
					'min' => '',

					/* (int) Maximum number value. Defaults to '' */
					'max' => '',

					/* (int) Step size increments. Defaults to '' */
					'step' => '',
				),

				/**
				 * Email field
				 */
				array (
					'key' => $stem.'_email',
					'label' => 'Email',
					'name' => 'email',
					'type' => 'email',
					'instructions' => '',
					'required' => 0,
					'conditional_logic' => 0,
					'wrapper' => array (
						'width' => '',
						'class' => '',
						'id' => '',
					),
					'default_value' => '',

					/* (string) Appears within the input. Defaults to '' */
					'placeholder' => '',

					/* (string) Appears before the input. Defaults to '' */
					'prepend' => '',

					/* (string) Appears after the input. Defaults to '' */
					'append' => '',
				),

				/**
				 * URL field
				 */
				array (
					'key' => $stem.'_url',
					'label' => 'URL',
					'name' => 'url',
					'type' => 'url',
					'instructions' => '',
					'required' => 0,
					'conditional_logic' => 0,
					'wrapper' => array (
						'width' => '',
						'class' => '',
						'id' => '',
					),
					'default_value' => '',

					/* (string) Appears within the input. Defaults to '' */
					'placeholder' => '',
				),

				/**
				 * Password field
				 */
				array (
					'key' => $stem.'_password',
					'label' => 'Password',
					'name' => 'password',
					'type' => 'password',
					'instructions' => '',
					'required' => 0,
					'conditional_logic' => 0,
					'wrapper' => array (
						'width' => '',
						'class' => '',
						'id' => '',
					),
					'default_value' => '',

					/* (string) Appears within the input. Defaults to '' */
					'placeholder' => '',

					/* (string) Appears before the input. Defaults to '' */
					'prepend' => '',

					/* (string) Appears after the input. Defaults to '' */
					'append' => '',
				),

				/**
				 * Wysiwyg field
				 */
				array (
					'key' => $stem.'_wysiwyg',
					'label' => 'Wysiwyg',
					'name' => 'wysiwyg',
					'type' => 'wysiwyg',
					'instructions' => '',
					'required' => 0,
					'conditional_logic' => 0,
					'wrapper' => array (
						'width' => '',
						'class' => '',
						'id' => '',
					),
					'default_value' => '',

					/* (string) Specify which tabs are available. Defaults to 'all'. Choices of 'all' (Visual & Text), 'visual' (Visual Only) or text (Text Only) */
					'tabs' => 'all',

					/* (string) Specify the editor's toolbar. Defaults to 'full'. Choices of 'full' (Full), 'basic' (Basic) or a custom toolbar (https://www.advancedcustomfields.com/resources/customize-the-wysiwyg-toolbars/) */
					'toolbar' => 'full',

					/* (bool) Show the media upload button. Defaults to 1 */
					'media_upload' => 1,

					/* (bool) delays load of tinymce until user clicks into field */
					'delay' => 0,
				),


				/**
				 * Object Embed field
				 */
				array (
					'key' => $stem.'_oembed',
					'label' => 'Object Embed',
					'name' => 'oembed',
					'type' => 'oembed',
					'instructions' => '',
					'required' => 0,
					'conditional_logic' => 0,
					'wrapper' => array (
						'width' => '',
						'class' => '',
						'id' => '',
					),
					'default_value' => '',

					/* (int) Specify the width of the oEmbed element. Can be overridden by CSS */
					'width' => '',

					/* (int) Specify the height of the oEmbed element. Can be overridden by CSS */
					'height' => '',
				),

				/**
				 * Image field
				 */
				array (
					'key' => $stem.'_image',
					'label' => 'Image',
					'name' => 'image',
					'type' => 'image',
					'instructions' => '',
					'required' => 0,
					'conditional_logic' => 0,
					'wrapper' => array (
						'width' => '',
						'class' => '',
						'id' => '',
					),
					'default_value' => '',

					/* (string) Specify the type of value returned by get_field(). Defaults to 'array'. Choices of 'array' (Image Array), 'url' (Image URL) or 'id' (Image ID) */
					'return_format' => 'array',

					/* (string) Specify the image size shown when editing. Defaults to 'thumbnail'. */
					'preview_size' => 'thumbnail',

					/* (string) Restrict the image library. Defaults to 'all'. Choices of 'all' (All Images) or 'uploadedTo' (Uploaded to post) */
					'library' => 'all',

					/* (int) Specify the minimum width in px required when uploading. Defaults to 0 */
					'min_width' => 0,

					/* (int) Specify the minimum height in px required when uploading. Defaults to 0 */
					'min_height' => 0,

					/* (int) Specify the minimum filesize in MB required when uploading. Defaults to 0 
					The unit may also be included. eg. '256KB' */
					'min_size' => 0,

					/* (int) Specify the maximum width in px allowed when uploading. Defaults to 0 */
					'max_width' => 0,

					/* (int) Specify the maximum height in px allowed when uploading. Defaults to 0 */
					'max_height' => 0,

					/* (int) Specify the maximum filesize in MB in px allowed when uploading. Defaults to 0 The unit may also be included. eg. '256KB' */
					'max_size' => 0,

					/* (string) Comma separated list of file type extensions allowed when uploading. Defaults to '' */
					'mime_types' => '',
				),

				/**
				 * File field
				 */
				array (
					'key' => $stem.'_file',
					'label' => 'File',
					'name' => 'file',
					'type' => 'file',
					'instructions' => '',
					'required' => 0,
					'conditional_logic' => 0,
					'wrapper' => array (
						'width' => '',
						'class' => '',
						'id' => '',
					),
					'default_value' => '',

					/* (string) Specify the type of value returned by get_field(). Defaults to 'array'. Choices of 'array' (File Array), 'url' (File URL) or 'id' (File ID) */
					'return_format' => 'array',

					/* (string) Specify the file size shown when editing. Defaults to 'thumbnail'. */
					'preview_size' => 'thumbnail',

					/* (string) Restrict the file library. Defaults to 'all'. Choices of 'all' (All Files) or 'uploadedTo' (Uploaded to post) */
					'library' => 'all',

					/* (int) Specify the minimum filesize in MB required when uploading. Defaults to 0 The unit may also be included. eg. '256KB' */
					'min_size' => 0,

					/* (int) Specify the maximum filesize in MB in px allowed when uploading. Defaults to 0 The unit may also be included. eg. '256KB' */
					'max_size' => 0,

					/* (string) Comma separated list of file type extensions allowed when uploading. Defaults to '' */
					'mime_types' => '',
				),

				/**
				 * Gallery field
				 */
				array (
					'key' => $stem.'_gallery',
					'label' => 'Gallery',
					'name' => 'gallery',
					'type' => 'gallery',
					'instructions' => '',
					'required' => 0,
					'conditional_logic' => 0,
					'wrapper' => array (
						'width' => '',
						'class' => '',
						'id' => '',
					),
					'default_value' => '',

					/* (int) Specify the minimum attachments required to be selected. Defaults to 0 */
					'min' => 0,

					/* (int) Specify the maximum attachments allowed to be selected. Defaults to 0 */
					'max' => 0,

					/* (string) Specify the image size shown when editing. Defaults to 'thumbnail'. */
					'preview_size' => 'thumbnail',

					/* (string) Restrict the image library. Defaults to 'all'. Choices of 'all' (All Images) or 'uploadedTo' (Uploaded to post) */
					'library' => 'all',

					/* (int) Specify the minimum width in px required when uploading. Defaults to 0 */
					'min_width' => 0,

					/* (int) Specify the minimum height in px required when uploading. Defaults to 0 */
					'min_height' => 0,

					/* (int) Specify the minimum filesize in MB required when uploading. Defaults to 0 The unit may also be included. eg. '256KB' */
					'min_size' => 0,

					/* (int) Specify the maximum width in px allowed when uploading. Defaults to 0 */
					'max_width' => 0,

					/* (int) Specify the maximum height in px allowed when uploading. Defaults to 0 */
					'max_height' => 0,

					/* (int) Specify the maximum filesize in MB in px allowed when uploading. Defaults to 0 The unit may also be included. eg. '256KB' */
					'max_size' => 0,

					/* (string) Comma separated list of file type extensions allowed when uploading. Defaults to '' */
					'mime_types' => '',
				),

				/**
				 * Select field
				 */
				array (
					'key' => $stem.'_select',
					'label' => 'Select',
					'name' => 'select',
					'type' => 'select',
					'instructions' => '',
					'required' => 0,
					'conditional_logic' => 0,
					'wrapper' => array (
						'width' => '',
						'class' => '',
						'id' => '',
					),
					'default_value' => '',

					/* (array) Array of choices where the key ('red') is used as value and the value ('Red') is used as label */
					'choices' => array(
						'red'	=> 'Red'
					),

					/* (bool) Allow a null (blank) value to be selected. Defaults to 0 */
					'allow_null' => 0,

					/* (bool) Allow mulitple choices to be selected. Defaults to 0 */
					'multiple' => 0,

					/* (bool) Use the select2 interface. Defaults to 0 */
					'ui' => 0,

					/* (bool) Load choices via AJAX. The ui setting must also be true for this to work. Defaults to 0 */
					'ajax' => 0,

					/* (string) Appears within the select2 input. Defaults to '' */
					'placeholder' => '',
				),

				/**
				 * Checkbox field
				 */
				array (
					'key' => $stem.'_checkbox',
					'label' => 'Checkbox',
					'name' => 'checkbox',
					'type' => 'checkbox',
					'instructions' => '',
					'required' => 0,
					'conditional_logic' => 0,
					'wrapper' => array (
						'width' => '',
						'class' => '',
						'id' => '',
					),
					'default_value' => '',

					/* (array) Array of choices where the key ('red') is used as value and the value ('Red') is used as label */
					'choices' => array(
						'red'	=> 'Red'
					),

					/* (string) Specify the layout of the checkbox inputs. Defaults to 'vertical'. Choices of 'vertical' or 'horizontal' */
					'layout' => 0,
				),

				/**
				 * Radio field
				 */
				array (
					'key' => $stem.'_radio',
					'label' => 'Radio',
					'name' => 'radio',
					'type' => 'radio',
					'instructions' => '',
					'required' => 0,
					'conditional_logic' => 0,
					'wrapper' => array (
						'width' => '',
						'class' => '',
						'id' => '',
					),
					'default_value' => '',

					/* (array) Array of choices where the key ('red') is used as value and the value ('Red') is used as label */
					'choices' => array(
						'red'	=> 'Red'
					),

					/* (bool) Allow a custom choice to be entered via a text input */
					'other_choice' => 0,

					/* (bool) Allow the custom value to be added to this field's choices. Defaults to 0. Will not work with PHP registered fields, only DB fields */
					'save_other_choice' => 0,

					/* (string) Specify the layout of the checkbox inputs. Defaults to 'vertical'. Choices of 'vertical' or 'horizontal' */
					'layout' => 0,
				),

				/**
				 * True/False field
				 */
				array (
					'key' => $stem.'_true_false',
					'label' => 'True/False',
					'name' => 'true_false',
					'type' => 'true_false',
					'instructions' => '',
					'required' => 0,
					'conditional_logic' => 0,
					'wrapper' => array (
						'width' => '',
						'class' => '',
						'id' => '',
					),
					'default_value' => '',

					/* (string) Text shown along side the checkbox */
					'message' => '',
				),

				/**
				 * Post Object field
				 */
				array (
					'key' => $stem.'_post_object',
					'label' => 'Post Object',
					'name' => 'post_object',
					'type' => 'post_object',
					'instructions' => '',
					'required' => 0,
					'conditional_logic' => 0,
					'wrapper' => array (
						'width' => '',
						'class' => '',
						'id' => '',
					),
					'default_value' => '',

					/* (mixed) Specify an array of post types to filter the available choices. Defaults to '' */
					'post_type' => '',

					/* (mixed) Specify an array of taxonomies to filter the available choices. Defaults to '' */
					'taxonomy' => '',

					/* (bool) Allow a null (blank) value to be selected. Defaults to 0 */
					'allow_null' => 0,

					/* (bool) Allow mulitple choices to be selected. Defaults to 0 */
					'multiple' => 0,

					/* (string) Specify the type of value returned by get_field(). Defaults to 'object'. Choices of 'object' (Post object) or 'id' (Post ID) */
					'return_format' => 'object',
				),

				/**
				 * Page link field
				 */
				array (
					'key' => $stem.'_page_link',
					'label' => 'Page link',
					'name' => 'page_link',
					'type' => 'page_link',
					'instructions' => '',
					'required' => 0,
					'conditional_logic' => 0,
					'wrapper' => array (
						'width' => '',
						'class' => '',
						'id' => '',
					),
					'default_value' => '',

					/* (mixed) Specify an array of post types to filter the available choices. Defaults to '' */
					'post_type' => '',

					/* (mixed) Specify an array of taxonomies to filter the available choices. Defaults to '' */
					'taxonomy' => '',

					/* (bool) Allow a null (blank) value to be selected. Defaults to 0 */
					'allow_null' => 0,

					/* (bool) Allow mulitple choices to be selected. Defaults to 0 */
					'multiple' => 0,
				),

				/**
				 * Relationship field
				 */
				array (
					'key' => $stem.'_relationship',
					'label' => 'Relationship',
					'name' => 'relationship',
					'type' => 'relationship',
					'instructions' => '',
					'required' => 0,
					'conditional_logic' => 0,
					'wrapper' => array (
						'width' => '',
						'class' => '',
						'id' => '',
					),
					'default_value' => '',

					/* (mixed) Specify an array of post types to filter the available choices. Defaults to '' */
					'post_type' => '',

					/* (mixed) Specify an array of taxonomies to filter the available choices. Defaults to '' */
					'taxonomy' => '',

					/* (array) Specify the available filters used to search for posts. Choices of 'search' (Search input), 'post_type' (Post type select) and 'taxonomy' (Taxonomy select) */
					'filters' => array('search', 'post_type', 'taxonomy'),

					/* (array) Specify the visual elements for each post. Choices of 'featured_image' (Featured image icon) */
					'elements' => array(),

					/* (int) Specify the minimum posts required to be selected. Defaults to 0 */
					'min' => 0,

					/* (int) Specify the maximum posts allowed to be selected. Defaults to 0 */
					'max' => 0,

					/* (string) Specify the type of value returned by get_field(). Defaults to 'object'. Choices of 'object' (Post object) or 'id' (Post ID) */
					'return_format' => 'object',
				),

				/**
				 * Taxonomy field
				 */
				array (
					'key' => $stem.'_taxonomy',
					'label' => 'Taxonomy',
					'name' => 'taxonomy',
					'type' => 'taxonomy',
					'instructions' => '',
					'required' => 0,
					'conditional_logic' => 0,
					'wrapper' => array (
						'width' => '',
						'class' => '',
						'id' => '',
					),
					'default_value' => '',

					/* (string) Specify the taxonomy to select terms from. Defaults to 'category' */
					'taxonomy' => '',

					/* (array) Specify the appearance of the taxonomy field. Defaults to 'checkbox' Choices of 'checkbox' (Checkbox inputs), 'multi_select' (Select field - multiple), 'radio' (Radio inputs) or 'select' (Select field) */
					'field_type' => 'checkbox',

					/* (bool) Allow a null (blank) value to be selected. Defaults to 0 */
					'allow_null' => 0,

					/* (bool) Allow selected terms to be saved as relatinoships to the post */
					'load_save_terms' 	=> 0,

					/* (string) Specify the type of value returned by get_field(). Defaults to 'id'. Choices of 'object' (Term object) or 'id' (Term ID) */
					'return_format'		=> 'id',

					/* (bool) Allow new terms to be added via a popup window */
					'add_term'			=> 1
				),

				/**
				 * User field
				 */
				array (
					'key' => $stem.'_user',
					'label' => 'User',
					'name' => 'user',
					'type' => 'user',
					'instructions' => '',
					'required' => 0,
					'conditional_logic' => 0,
					'wrapper' => array (
						'width' => '',
						'class' => '',
						'id' => '',
					),
					'default_value' => '',

					/* (array) Array of roles to limit the users available for selection */
					'role' => array(),

					/* (bool) Allow a null (blank) value to be selected. Defaults to 0 */
					'allow_null' => 0,

					/* (bool) Allow mulitple choices to be selected. Defaults to 0 */
					'multiple' => 0,
				),

				/**
				 * Google Map field
				 *
				 * https://developers.google.com/maps/documentation/static-maps/intro
				 */
				array (
					'key' => $stem.'_google_map',
					'label' => 'Google Map',
					'name' => 'google_map',
					'type' => 'google_map',
					'instructions' => '',
					'required' => 0,
					'conditional_logic' => 0,
					'wrapper' => array (
						'width' => '',
						'class' => '',
						'id' => '',
					),
					'default_value' => '',

					/* (int) Specify the maximum map height. Defaults to 400 */
					'height' => '',

					/* (float) Specify the latitude -37.81411 */
					'center_lat' => '',

					/* (float) Specify the latitude 144.96328 */
					'center_lng' => '',

					/* (int) Zoom level. Approximate detail - 1: World, 5: Landmass/continent, 10: City, 15: Streets, 20: Buildings */
					'zoom' => '',
				),

				/**
				 * Date Picker field
				 *
				 * http://php.net/manual/en/function.date.php
				 */
				array (
					'key' => $stem.'_date_picker',
					'label' => 'Date Picker',
					'name' => 'date_picker',
					'type' => 'date_picker',
					'instructions' => '',
					'required' => 0,
					'conditional_logic' => 0,
					'wrapper' => array (
						'width' => '',
						'class' => '',
						'id' => '',
					),
					'default_value' => '',

					/* (string) valid php datetime format */
					'display_format' => 'd/m/Y',

					/* (string) valid php datetime format */
					'return_format' => 'd/m/Y',

					/* (int) day to use as beginning of calendar.  1 (for Monday) through 7 (for Sunday) */
					'first_day' => 1,
				),

				/**
				 * Date Time Picker field
				 *
				 * http://php.net/manual/en/function.date.php
				 */
				array (
					'key' => $stem.'_date_time_picker',
					'label' => 'Date Time Picker',
					'name' => 'date_time_picker',
					'type' => 'date_time_picker',
					'instructions' => '',
					'required' => 0,
					'conditional_logic' => 0,
					'wrapper' => array (
						'width' => '',
						'class' => '',
						'id' => '',
					),
					'default_value' => '',

					/* (string) valid php datetime format */
					'display_format' => 'd/m/Y g:i a',

					/* (string) valid php datetime format */
					'return_format' => 'd/m/Y g:i a',

					/* (int) day to use as beginning of calendar.  1 (for Monday) through 7 (for Sunday) */
					'first_day' => 1,
				),

				/**
				 * Time Picker field
				 *
				 * http://php.net/manual/en/function.date.php
				 */
				array (
					'key' => $stem.'_time_picker',
					'label' => 'Time Picker',
					'name' => 'time_picker',
					'type' => 'time_picker',
					'instructions' => '',
					'required' => 0,
					'conditional_logic' => 0,
					'wrapper' => array (
						'width' => '',
						'class' => '',
						'id' => '',
					),
					'default_value' => '',

					/* (string) valid php datetime format */
					'display_format' => 'g:i a',

					/* (string) valid php datetime format */
					'return_format' => 'g:i a',
				),

				/**
				 * Color Picker field
				 */
				array (
					'key' => $stem.'_color_picker',
					'label' => 'Color Picker',
					'name' => 'color_picker',
					'type' => 'color_picker',
					'instructions' => '',
					'required' => 0,
					'conditional_logic' => 0,
					'wrapper' => array (
						'width' => '',
						'class' => '',
						'id' => '',
					),
					'default_value' => '',		//This should be a full hex code #FFFFFF
				),

				/**
				 * Message field - displays information to users
				 */
				array (
					'key' => $stem.'_message',
					'label' => 'Message',
					'name' => '',
					'type' => 'message',
					'instructions' => '',
					'required' => 0,
					'conditional_logic' => 0,
					'wrapper' => array (
						'width' => '',
						'class' => '',
						'id' => '',
					),
					'default_value' => '',

					/* (string) message to diplay to user */
					'message' => '',

					/* (new_lines) Decides how to render new lines. Detauls to 'wpautop'. Choices of 'wpautop' (Automatically add paragraphs), 'br' (Automatically add <br>) or '' (No Formatting) */
					'new_lines' => 'wpautop',

					/* (int) Set to 1 to convert html to entities (disallow html) */
					'esc_html' => 0,
				),

				/**
				 * Tab field
				 */
				array (
					'key' => $stem.'_tab',
					'label' => 'Tab',
					'name' => 'tab',
					'type' => 'tab',
					'instructions' => '',
					'required' => 0,
					'conditional_logic' => 0,
					'wrapper' => array (
						'width' => '',
						'class' => '',
						'id' => '',
					),
					'default_value' => '',

					/* (placement) Determine where tabs display top or left */
					'placement' => 'top',

					/* (bool) Use this field as an end-point and start a new group of tabs */
					'endpoint' => 0,
				),

				/**
				 * Repeater field
				 */
				array (
					'key' => $stem.'_repeater',
					'label' => 'Repeater',
					'name' => 'repeater',
					'type' => 'repeater',
					'instructions' => '',
					'required' => 0,
					'conditional_logic' => 0,
					'wrapper' => array (
						'width' => '',
						'class' => '',
						'id' => '',
					),
					'default_value' => '',

					/* (array) Fields to nest within repeater, uses same settings as a top level field */
					'sub_fields' => array (
						array (
							'key' => $stem.'_repeater_text',	//note that key is still unique, even when nested
							'label' => 'Text',
							'name' => 'text',					//name, however can be repeated when nested
							'type' => 'text',
							'instructions' => '',
							'required' => 0,
							'conditional_logic' => 0,
							'wrapper' => array (
								'width' => '',
								'class' => '',
								'id' => '',
							),
							'default_value' => '',
							'placeholder' => '',
							'prepend' => '',
							'append' => '',
							'maxlength' => '',
							'readonly' => 0,
							'disabled' => 0,
						),
					),

					/* (int) minimum number of blocks */
					'min' => 0,

					/* (int) maximum number of blocks */
					'max' => 0,

					/* (layout) Each group of inputs will be displayed in this way. table - Table, block - Block, row - Row */
					'layout' => 'table',

					/* (string) Test displayed on button to add new group of inputs.  Defaults to Add Row */
					'button_label' => '',

					/* (selection) Select a sub field to show when row is collapsed would be the key of a sub field */
					'collapsed' => '',
				),

				/**
				 * Flexible Content field
				 */
				array (
					'key' => $stem.'_flexible_content',
					'label' => 'Flexible Content',
					'name' => 'flexible_content',
					'type' => 'flexible_content',
					'instructions' => '',
					'required' => 0,
					'conditional_logic' => 0,
					'wrapper' => array (
						'width' => '',
						'class' => '',
						'id' => '',
					),
					'default_value' => '',

					/* (array) of */
					'layouts' => array (
						array (
							'key' => $stem.'_flexible_content_block',
							'name' => 'example_block',
							'label' => 'Example Block',

							/* (display) Each group of inputs will be displayed in this way. table - Table, block - Block, row - Row */
							'display' => 'block',

							/* (array) Fields to nest within repeater, uses same settings as a top level field */
							'sub_fields' => array (
								array (
									'key' => $stem.'_fcb_text',			//note that key is still unique, even when nested
									'label' => 'Text',
									'name' => 'text',					//name, however can be repeated when nested
									'type' => 'text',
									'instructions' => '',
									'required' => 0,
									'conditional_logic' => 0,
									'wrapper' => array (
										'width' => '',
										'class' => '',
										'id' => '',
									),
									'default_value' => '',
									'placeholder' => '',
									'prepend' => '',
									'append' => '',
									'maxlength' => '',
									'readonly' => 0,
									'disabled' => 0,
								),
							),

							/* (int) minimum number of these blocks that can be used */
							'min' => '',

							/* (int) maximum number of these blocks that can be used*/
							'max' => '',
						),
					),

					/* (int) minimum number of blocks */
					'min' => 0,

					/* (int) maximum number of blocks */
					'max' => 0,

					/* (string) Test displayed on button to add new group of inputs.  Defaults to Add Row */
					'button_label' => 'Add Row',
				),
			),

			'location' => array (
				array (
					array (
						'param' => 'post_type',
						'operator' => '==',
						'value' => $this->key,
					),
					/*
					//"and" conditions would go here
					array (
						'param' => 'post_taxonomy',
						'operator' => '==',
						'value' => 'post_tag:sometag',
					),
					 */
				),

				/*
				//"or" conditions
				array(
					array(
						'param' => 'post_type',
						'operator' => '==',
						'value' => 'some_other_key',
					),
				),
				 */
			),

			/* (int) Field groups are shown in order from lowest to highest. Defaults to 0 */
			'menu_order' => 0,

			/* (string) Determines the position on the edit screen. Defaults to normal. Choices of 'acf_after_title', 'normal' or 'side' */
			'position' => 'normal',

			/* (string) Determines the metabox style. Defaults to 'default'. Choices of 'default' or 'seamless' */
			'style' => 'default',

			/* (string) Determines where field labels are places in relation to fields. Defaults to 'top'. Choices of 'top' (Above fields) or 'left' (Beside fields) */
			'label_placement' => 'top',

			/* (string) Determines where field instructions are places in relation to fields. Defaults to 'label'. Choices of 'label' (Below labels) or 'field' (Below fields) */
			'instruction_placement' => 'label',

			/* (array) An array of elements to hide on the screen */
			'hide_on_screen' => '',
			/*
			'hide_on_screen'=> array(
				'permalink', 						//hide Permalink
				'the_content', 						//hide Content Editor
				'excerpt', 							//hide Excerpt
				'custom_fields', 					//hide Custom Fields
				'discussion', 						//hide Discussion
				'comments', 						//hide Comments
				'revisions', 						//hide Revisions
				'slug', 							//hide Slug
				'author', 							//hide Author
				'format', 							//hide Format
				'page_attributes', 					//hide Page Attributes
				'featured_image', 					//hide Featured Image
				'categories', 						//hide Categories
				'tags', 							//hide Tags
				'send-trackbacks', 					//hide Send Trackbacks
			),
			 */

			/* not really pertinent for php defined fields, would turn fields off but leave them defined */
			'active' => 1,

			/* not really pertinent for php defined fields, would display on acf group list page */
			'description' => '',
		));
	}

}

new CPT_Boilerplate_Example_Post_Type();