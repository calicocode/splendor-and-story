<?php

class CPT_Boilerplate_Project extends CPT_Boilerplate_Base_Post_Type{

	protected $key = 'project';

	/**
	 * Registers post type with wordpress
	 * you should also define any taxonomies for this custom post type here
	 *
	 * More Information about options here:
	 * https://codex.wordpress.org/Function_Reference/register_post_type
	 * https://developer.wordpress.org/reference/functions/register_post_type/
	 *
	 * Creation wizard:
	 * https://generatewp.com/post-type/
	 *
	 */
	function define_cpt(){

		$single_label = 'Project';
		$plural_label = 'Projects';
		$lowercase = strtolower($single_label);

		$labels = array(
			'name'                  => _x( $single_label, 'Post Type General Name', CPT_BOILERPLATE_STEM ),
			'singular_name'         => _x( $single_label, 'Post Type Singular Name', CPT_BOILERPLATE_STEM ),
			'menu_name'             => __( $plural_label, CPT_BOILERPLATE_STEM ),
			'name_admin_bar'        => __( $plural_label, CPT_BOILERPLATE_STEM ),
			'archives'              => __( $single_label.' Archives', CPT_BOILERPLATE_STEM ),
			'parent_item_colon'     => __( 'Parent '.$single_label.':', CPT_BOILERPLATE_STEM ),
			'all_items'             => __( 'All '.$plural_label, CPT_BOILERPLATE_STEM ),
			'add_new_item'          => __( 'Add New '.$single_label, CPT_BOILERPLATE_STEM ),
			'add_new'               => __( 'Add New', CPT_BOILERPLATE_STEM ),
			'new_item'              => __( 'New '.$single_label, CPT_BOILERPLATE_STEM ),
			'edit_item'             => __( 'Edit '.$single_label, CPT_BOILERPLATE_STEM ),
			'update_item'           => __( 'Update '.$single_label, CPT_BOILERPLATE_STEM ),
			'view_item'             => __( 'View '.$single_label, CPT_BOILERPLATE_STEM ),
			'search_items'          => __( 'Search '.$single_label, CPT_BOILERPLATE_STEM ),
			'not_found'             => __( 'Not found', CPT_BOILERPLATE_STEM ),
			'not_found_in_trash'    => __( 'Not found in Trash', CPT_BOILERPLATE_STEM ),
			'featured_image'        => __( 'Featured Image', CPT_BOILERPLATE_STEM ),
			'set_featured_image'    => __( 'Set featured image', CPT_BOILERPLATE_STEM ),
			'remove_featured_image' => __( 'Remove featured image', CPT_BOILERPLATE_STEM ),
			'use_featured_image'    => __( 'Use as featured image', CPT_BOILERPLATE_STEM ),
			'insert_into_item'      => __( 'Insert into '.$single_label, CPT_BOILERPLATE_STEM ),
			'uploaded_to_this_item' => __( 'Uploaded to this '.$lowercase, CPT_BOILERPLATE_STEM ),
			'items_list'            => __( $single_label.' list', CPT_BOILERPLATE_STEM ),
			'items_list_navigation' => __( $single_label.' list navigation', CPT_BOILERPLATE_STEM ),
			'filter_items_list'     => __( 'Filter '.$lowercase.' list', CPT_BOILERPLATE_STEM ),
		);

		$args = array(
			'label'                 => __( $single_label, CPT_BOILERPLATE_STEM ),

			'description'           => __( '', CPT_BOILERPLATE_STEM ),

			'labels'                => $labels,

			//An alias for calling add_post_type_support() directly. As of 3.5, boolean false can be passed as value instead of an array to prevent default (title and editor) behavior.
			'supports'              => array(
				'title',
				'editor',
				'author',
				'thumbnail',
				'excerpt',
				'trackbacks',
				'custom-fields',
				'comments',
				'revisions',
				'page-attributes',
				'post-formats',
			),

			//Controls how the type is visible to authors (show_in_nav_menus, show_ui) and readers (exclude_from_search, publicly_queryable).
			//If no value is specified for exclude_from_search, publicly_queryable, show_in_nav_menus, or show_ui, they inherit their values from public.
			'public'                => true,

			//Whether to generate a default UI for managing this post type in the admin.
			'show_ui'               => true,

			//Where to show the post type in the admin menu. show_ui must be true.
			//Boolean or if an existing top level page such as 'tools.php' or 'edit.php?post_type=page', the post type will be placed as a sub menu of that.
			'show_in_menu'          => true,

			//Whether post_type is available for selection in navigation menus.
			'show_in_nav_menus'     => true,

			//Whether to make this post type available in the WordPress admin bar.
			'show_in_admin_bar'     => true,

			//The position in the menu order the post type should appear. show_in_menu must be true.
			//5 - below Posts, 10 - below Media, 15 - below Links, 20 - below Pages, 25 - below comments, 60 - below first separator, 65 - below Plugins, 70 - below Users, 75 - below Tools, 80 - below Settings, 100 - below second separator
			'menu_position'         => 20,

			//The url to the icon to be used for this menu or the name of the icon from the iconfont https://developer.wordpress.org/resource/dashicons/#welcome-view-site
			'menu_icon'             => 'dashicons-admin-post',

			//Whether to exclude posts with this post type from front end search results.
			'exclude_from_search'   => false,

			//Whether queries can be performed on the front end as part of parse_request().
			'publicly_queryable'    => true,

			//Whether the post type is hierarchical (e.g. page). Allows Parent to be specified. The 'supports' parameter should contain 'page-attributes' to show the parent select box on the editor page.
			'hierarchical'          => false,

			//Can this post_type be exported.
			'can_export'            => true,

			//Enables post type archives. Will use $post_type as archive slug by default.
			'has_archive'           => false,

			//Triggers the handling of rewrites for this post type. To prevent rewrites, set to false.
			/**
			 * $args array
			 * 'slug' => string Customize the permalink structure slug. Defaults to the $post_type value. Should be translatable.
			 * 'with_front' => bool Should the permalink structure be prepended with the front base. (example: if your permalink structure is /blog/, then your links will be: false->/news/, true->/blog/news/). Defaults to true
			 * 'feeds' => bool Should a feed permalink structure be built for this post type. Defaults to has_archive value.
			 * 'pages' => bool Should the permalink structure provide for pagination. Defaults to true
			 */
			'rewrite'               => array( 'slug' => $this->key ),

			/**
			 * read more about more complex capabilities within codex
			 * 99/100 you will just need post or page here
			 */
			'capability_type'       => 'post',
		);

		register_post_type( $this->key, $args );

		$labels = array(
			'name'                       => _x( 'Project Categories', 'Taxonomy General Name', CPT_BOILERPLATE_STEM ),
			'singular_name'              => _x( 'Project Category', 'Taxonomy Singular Name', CPT_BOILERPLATE_STEM ),
			'menu_name'                  => __( 'Project Category', CPT_BOILERPLATE_STEM ),
			'all_items'                  => __( 'All Items', CPT_BOILERPLATE_STEM ),
			'parent_item'                => __( 'Parent Item', CPT_BOILERPLATE_STEM ),
			'parent_item_colon'          => __( 'Parent Item:', CPT_BOILERPLATE_STEM ),
			'new_item_name'              => __( 'New Item Name', CPT_BOILERPLATE_STEM ),
			'add_new_item'               => __( 'Add New Item', CPT_BOILERPLATE_STEM ),
			'edit_item'                  => __( 'Edit Item', CPT_BOILERPLATE_STEM ),
			'update_item'                => __( 'Update Item', CPT_BOILERPLATE_STEM ),
			'view_item'                  => __( 'View Item', CPT_BOILERPLATE_STEM ),
			'separate_items_with_commas' => __( 'Separate items with commas', CPT_BOILERPLATE_STEM ),
			'add_or_remove_items'        => __( 'Add or remove items', CPT_BOILERPLATE_STEM ),
			'choose_from_most_used'      => __( 'Choose from the most used', CPT_BOILERPLATE_STEM ),
			'popular_items'              => __( 'Popular Items', CPT_BOILERPLATE_STEM ),
			'search_items'               => __( 'Search Items', CPT_BOILERPLATE_STEM ),
			'not_found'                  => __( 'Not Found', CPT_BOILERPLATE_STEM ),
			'no_terms'                   => __( 'No items', CPT_BOILERPLATE_STEM ),
			'items_list'                 => __( 'Items list', CPT_BOILERPLATE_STEM ),
			'items_list_navigation'      => __( 'Items list navigation', CPT_BOILERPLATE_STEM ),
		);
		$args = array(
			'labels'                     => $labels,
			'hierarchical'               => false,
			'public'                     => true,
			'show_ui'                    => true,
			'show_admin_column'          => false,
			'show_in_nav_menus'          => true,
			'show_tagcloud'              => false,
		    'rewrite'           		 => array( 'slug' => 'project-category', 'with_front' => false ),
		);
		register_taxonomy( 'project_category', array( $this->key ), $args );

		flush_rewrite_rules();
	}

		/**
	 * Define any custom fields here - it is assumed you are using Advanced Custom Fields
	 * Each field type is present here, fields have common settings and special settings per field
	 *
	 * https://www.advancedcustomfields.com/resources/register-fields-via-php/
	 */
	function acf_fields(){
		$stem = strtolower(get_class($this));
		acf_add_local_field_group(array (
			'key' => $stem,
			'title' => 'Project Custom Fields',
			'fields' => array (

				/**
				 * Tab field
				 */
				array (
					'key' => $stem.'_featured_tab',
					'label' => 'Featured Image',
					'name' => 'featured_tab',
					'type' => 'tab',
					'instructions' => '',
					'required' => 0,
					'conditional_logic' => 0,
					'wrapper' => array (
						'width' => '',
						'class' => '',
						'id' => '',
					),
					'default_value' => '',

					/* (placement) Determine where tabs display top or left */
					'placement' => 'left',

					/* (bool) Use this field as an end-point and start a new group of tabs */
					'endpoint' => 0,
				),
				/**
				 * Image field
				 */
				array (
					'key' => $stem.'_slider_image',
					'label' => 'Slider Image',
					'name' => 'slider_image',
					'type' => 'image',
					'instructions' => 'This image is used on the home page slider and must have a square aspect ratio.',
					'required' => 1,
					'conditional_logic' => 0,
					'wrapper' => array (
						'width' => '',
						'class' => '',
						'id' => '',
					),
					'default_value' => '',

					/* (string) Specify the type of value returned by get_field(). Defaults to 'array'. Choices of 'array' (Image Array), 'url' (Image URL) or 'id' (Image ID) */
					'return_format' => 'url',

					/* (string) Specify the image size shown when editing. Defaults to 'thumbnail'. */
					'preview_size' => 'thumbnail',

					/* (string) Restrict the image library. Defaults to 'all'. Choices of 'all' (All Images) or 'uploadedTo' (Uploaded to post) */
					'library' => 'all',

					/* (int) Specify the minimum width in px required when uploading. Defaults to 0 */
					'min_width' => 0,

					/* (int) Specify the minimum height in px required when uploading. Defaults to 0 */
					'min_height' => 0,

					/* (int) Specify the minimum filesize in MB required when uploading. Defaults to 0 
					The unit may also be included. eg. '256KB' */
					'min_size' => 0,

					/* (int) Specify the maximum width in px allowed when uploading. Defaults to 0 */
					'max_width' => 0,

					/* (int) Specify the maximum height in px allowed when uploading. Defaults to 0 */
					'max_height' => 0,

					/* (int) Specify the maximum filesize in MB in px allowed when uploading. Defaults to 0 The unit may also be included. eg. '256KB' */
					'max_size' => 0,

					/* (string) Comma separated list of file type extensions allowed when uploading. Defaults to '' */
					'mime_types' => '',
				),
				/**
				 * Tab field
				 */
				array (
					'key' => $stem.'_detail_slider_tab',
					'label' => 'Detail Images Slider',
					'name' => 'detail_slider_tab',
					'type' => 'tab',
					'instructions' => '',
					'required' => 0,
					'conditional_logic' => 0,
					'wrapper' => array (
						'width' => '',
						'class' => '',
						'id' => '',
					),
					'default_value' => '',

					/* (placement) Determine where tabs display top or left */
					'placement' => 'left',

					/* (bool) Use this field as an end-point and start a new group of tabs */
					'endpoint' => 0,
				),
				/**
				 * Repeater field
				 */
				array (
					'key' => $stem.'_detail_slider',
					'label' => 'Detail Images Slider',
					'name' => 'detail_slider',
					'type' => 'repeater',
					'instructions' => '',
					'required' => 1,
					'conditional_logic' => 0,
					'wrapper' => array (
						'width' => '',
						'class' => '',
						'id' => '',
					),
					'default_value' => '',

					/* (array) Fields to nest within repeater, uses same settings as a top level field */
					'sub_fields' => array (
						/**
						 * Image field
						 */
						array (
							'key' => $stem.'_detail_slider_image',
							'label' => 'Detail Slider Image',
							'name' => 'image',
							'type' => 'image',
							'instructions' => '',
							'required' => 1,
							'conditional_logic' => 0,
							'wrapper' => array (
								'width' => '',
								'class' => '',
								'id' => '',
							),
							'default_value' => '',

							/* (string) Specify the type of value returned by get_field(). Defaults to 'array'. Choices of 'array' (Image Array), 'url' (Image URL) or 'id' (Image ID) */
							'return_format' => 'array',

							/* (string) Specify the image size shown when editing. Defaults to 'thumbnail'. */
							'preview_size' => 'thumbnail',

							/* (string) Restrict the image library. Defaults to 'all'. Choices of 'all' (All Images) or 'uploadedTo' (Uploaded to post) */
							'library' => 'all',

							/* (int) Specify the minimum width in px required when uploading. Defaults to 0 */
							'min_width' => 0,

							/* (int) Specify the minimum height in px required when uploading. Defaults to 0 */
							'min_height' => 0,

							/* (int) Specify the minimum filesize in MB required when uploading. Defaults to 0 
							The unit may also be included. eg. '256KB' */
							'min_size' => 0,

							/* (int) Specify the maximum width in px allowed when uploading. Defaults to 0 */
							'max_width' => 0,

							/* (int) Specify the maximum height in px allowed when uploading. Defaults to 0 */
							'max_height' => 0,

							/* (int) Specify the maximum filesize in MB in px allowed when uploading. Defaults to 0 The unit may also be included. eg. '256KB' */
							'max_size' => 0,

							/* (string) Comma separated list of file type extensions allowed when uploading. Defaults to '' */
							'mime_types' => '',
						),
						/**
						 * Text field
						 *
						 * If copy/pasting, there is a simpler text configuration below this one
						 */
						array (
							/**
							 * Common field settings
							 */

							/* (string) Unique identifier for the field. Must begin with 'field_' */
							'key' => $stem.'_caption',

							/* (string) Visible when editing the field value */
							'label' => 'Caption',

							/* (string) Used to save and load data. Single word, no spaces. Underscores and dashes allowed */
							'name' => 'caption',

							/* (string) Type of field (text, textarea, image, etc) */
							'type' => 'text',

							/* (string) Instructions for authors. Shown when submitting data */
							'instructions' => '',

							/* (int) Whether or not the field value is required. Defaults to 0 */
							'required' => 0,

							/* (mixed) Conditionally hide or show this field based on other field's values. Best to use the ACF UI and export to understand the array structure. Defaults to 0 */
							'conditional_logic' => 0,
							/* Sample conditional Logic
							'conditional_logic' => array(
								array(
									array(
										'field'=>'field_key_here',
										'operator'=>'==',
										'value'=>'some_value',
									),

									// "And" condition
									array(
										'field'=>'some_other_key_here',
										'operator'=>'==',
										'value'=>'some_value',
									),
								),
								// "Or" condition
								array(
									array(
										'field'=>'field_key_here',
										'operator'=>'==',
										'value'=>'some_other_value',
									),
								),
							),
							 */

							/* (array) An array of attributes given to the field element */
							'wrapper' => array (
								'width' => '',
								'class' => '',
								'id' => '',
							),

							/* (mixed) A default value used by ACF if no value has yet been saved */
							'default_value' => '',

							/**
							 * Text Field specific settings
							 */

							/* (string) Appears within the input. Defaults to '' */
							'placeholder' => '',

							/* (string) Appears before the input. Defaults to '' */
							'prepend' => '',

							/* (string) Appears after the input. Defaults to '' */
							'append' => '',

							/* (string) Restricts the character limit. Defaults to '' */
							'maxlength' => '',

							/* (bool) Makes the input readonly. Defaults to 0 */
							'readonly' => 0,

							/* (bool) Makes the input disabled. Defaults to 0 */
							'disabled' => 0,
						),
					),

					/* (int) minimum number of blocks */
					'min' => 1,

					/* (int) maximum number of blocks */
					'max' => 0,

					/* (layout) Each group of inputs will be displayed in this way. table - Table, block - Block, row - Row */
					'layout' => 'table',

					/* (string) Test displayed on button to add new group of inputs.  Defaults to Add Row */
					'button_label' => '',

					/* (selection) Select a sub field to show when row is collapsed would be the key of a sub field */
					'collapsed' => '',
				),
				/**
				 * Tab field
				 */
				array (
					'key' => $stem.'_related_projects_tab',
					'label' => 'Related Projects Block',
					'name' => 'related_projects_tab',
					'type' => 'tab',
					'instructions' => '',
					'required' => 0,
					'conditional_logic' => 0,
					'wrapper' => array (
						'width' => '',
						'class' => '',
						'id' => '',
					),
					'default_value' => '',

					/* (placement) Determine where tabs display top or left */
					'placement' => 'left',

					/* (bool) Use this field as an end-point and start a new group of tabs */
					'endpoint' => 0,
				),
				/**
				 * Relationship field
				 */
				array (
					'key' => $stem.'_related_projects',
					'label' => 'Related Projects',
					'name' => 'related_projects',
					'type' => 'relationship',
					'instructions' => '',
					'required' => 1,
					'conditional_logic' => 0,
					'wrapper' => array (
						'width' => '',
						'class' => '',
						'id' => '',
					),
					'default_value' => '',

					/* (mixed) Specify an array of post types to filter the available choices. Defaults to '' */
					'post_type' => 'project',

					/* (mixed) Specify an array of taxonomies to filter the available choices. Defaults to '' */
					'taxonomy' => '',

					/* (array) Specify the available filters used to search for posts. Choices of 'search' (Search input), 'post_type' (Post type select) and 'taxonomy' (Taxonomy select) */
					'filters' => array('search'),

					/* (array) Specify the visual elements for each post. Choices of 'featured_image' (Featured image icon) */
					'elements' => array('featured_image'),

					/* (int) Specify the minimum posts required to be selected. Defaults to 0 */
					'min' => 1,

					/* (int) Specify the maximum posts allowed to be selected. Defaults to 0 */
					'max' => 4,

					/* (string) Specify the type of value returned by get_field(). Defaults to 'object'. Choices of 'object' (Post object) or 'id' (Post ID) */
					'return_format' => 'object',
				),
			),

			'location' => array (
				array (
					array (
						'param' => 'post_type',
						'operator' => '==',
						'value' => $this->key,
					),
					/*
					//"and" conditions would go here
					array (
						'param' => 'post_taxonomy',
						'operator' => '==',
						'value' => 'post_tag:sometag',
					),
					 */
				),

				/*
				//"or" conditions
				array(
					array(
						'param' => 'post_type',
						'operator' => '==',
						'value' => 'some_other_key',
					),
				),
				 */
			),

			/* (int) Field groups are shown in order from lowest to highest. Defaults to 0 */
			'menu_order' => 0,

			/* (string) Determines the position on the edit screen. Defaults to normal. Choices of 'acf_after_title', 'normal' or 'side' */
			'position' => 'normal',

			/* (string) Determines the metabox style. Defaults to 'default'. Choices of 'default' or 'seamless' */
			'style' => 'default',

			/* (string) Determines where field labels are places in relation to fields. Defaults to 'top'. Choices of 'top' (Above fields) or 'left' (Beside fields) */
			'label_placement' => 'top',

			/* (string) Determines where field instructions are places in relation to fields. Defaults to 'label'. Choices of 'label' (Below labels) or 'field' (Below fields) */
			'instruction_placement' => 'label',

			/* (array) An array of elements to hide on the screen */
			'hide_on_screen'=> array(
				'custom_fields', 					//hide Custom Fields
				'discussion', 						//hide Discussion
				'comments', 						//hide Comments
				'send-trackbacks', 					//hide Send Trackbacks
			),
			/*
			'hide_on_screen'=> array(
				'permalink', 						//hide Permalink
				'the_content', 						//hide Content Editor
				'excerpt', 							//hide Excerpt
				'custom_fields', 					//hide Custom Fields
				'discussion', 						//hide Discussion
				'comments', 						//hide Comments
				'revisions', 						//hide Revisions
				'slug', 							//hide Slug
				'author', 							//hide Author
				'format', 							//hide Format
				'page_attributes', 					//hide Page Attributes
				'featured_image', 					//hide Featured Image
				'categories', 						//hide Categories
				'tags', 							//hide Tags
				'send-trackbacks', 					//hide Send Trackbacks
			),
			 */

			/* not really pertinent for php defined fields, would turn fields off but leave them defined */
			'active' => 1,

			/* not really pertinent for php defined fields, would display on acf group list page */
			'description' => '',
		));
	}
}

new CPT_Boilerplate_Project();