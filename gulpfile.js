'use strict';
var gulp = require('gulp');
var $ = require('gulp-load-plugins')();
var sourcemaps = require("gulp-sourcemaps");
var babel = require("gulp-babel");
var concat = require("gulp-concat");
var uglify = require('gulp-uglify');
var rename = require('gulp-rename');
//var browser = require("browser-sync");

var sassPaths = [];

var jsPaths = [
    "htdocs/wp-content/themes/cleanslate/assets/js/plugins.js",
    "htdocs/wp-content/themes/cleanslate/assets/js/main.js"
];

gulp.task('sass', function() {
    return gulp.src('htdocs/wp-content/themes/cleanslate/assets/scss/style.scss')
        .pipe($.sass({
                includePaths: sassPaths,
                outputStyle: 'compressed' // if css compressed **file size**
            })
            .on('error', $.sass.logError))
        .pipe($.autoprefixer({
            browsers: ['last 2 versions', 'ie >= 9']
        }))
        .pipe(gulp.dest('htdocs/wp-content/themes/cleanslate/assets/css/'));
});

/*gulp.task('print', function() {
    return gulp.src('htdocs/wp-content/themes/cleanslate/assets/scss/print.scss')
        .pipe($.sass({
                includePaths: sassPaths,
                outputStyle: 'compressed' // if css compressed **file size**
            })
            .on('error', $.sass.logError))
        .pipe($.autoprefixer({
            browsers: ['last 2 versions', 'ie >= 9']
        }))
        .pipe(gulp.dest('htdocs/wp-content/themes/cleanslate/assets/css/'));
});*/

gulp.task('ie8', function() {
    return gulp.src('htdocs/wp-content/themes/cleanslate/assets/scss/ie8.scss')
        .pipe($.sass({
                includePaths: sassPaths,
                outputStyle: 'compressed' // if css compressed **file size**
            })
            .on('error', $.sass.logError))
        .pipe($.autoprefixer({
            browsers: ['last 2 versions', 'ie >= 9']
        }))
        .pipe(gulp.dest('htdocs/wp-content/themes/cleanslate/assets/css/'));
});

gulp.task("compress", function() {
    return gulp.src('htdocs/wp-content/themes/cleanslate/assets/js/{main,plugins}.js')
        .pipe(sourcemaps.init())
        .pipe(babel({
            presets: ['es2015']
        }))
        .pipe(uglify({
            preserveComments: 'license'
        }))
        .pipe(rename({
            suffix: '.min'
        }))
        .pipe(sourcemaps.write("."))
        .pipe(gulp.dest("htdocs/wp-content/themes/cleanslate/assets/js/min"));
});

// Start a server with BrowserSync to preview the site in
gulp.task("server", function() {
    browser.init({
        server: './',
        port: 8888
    });
});

// Reload the browser with BrowserSync
function reload(done) {
    browser.reload();
    //done();
}

gulp.task('default', ['sass', 'compress'], function() {
    gulp.watch(['htdocs/wp-content/themes/cleanslate/assets/scss/**/*.scss'], ['sass']);
    gulp.watch(['htdocs/wp-content/themes/cleanslate/assets/js/*.js'], ['compress']);
    gulp.watch(['htdocs/wp-content/themes/cleanslate/assets/scss/print.scss'], ['print']);
    gulp.watch(['htdocs/wp-content/themes/cleanslate/assets/scss/ie8.scss'], ['ie8']);
});